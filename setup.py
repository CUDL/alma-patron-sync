from setuptools import setup, find_packages


def get_version(filename):
    '''
    Parse the value of the __version__ var from a Python source file
    without running/importing the file.
    '''
    import re
    version_pattern = r'^ *__version__ *= *[\'"](\d+\.\d+\.\d+)[\'"] *$'
    match = re.search(version_pattern, open(filename).read(), re.MULTILINE)

    assert match, ('No version found in file: {!r} matching pattern: {!r}'
                   .format(filename, version_pattern))

    return match.group(1)


setup(
    name='alma-user-sync',
    version=get_version('src/almausersync/_version.py'),
    packages=find_packages('src'),
    package_dir={'': 'src'},
    author='Hal Blackburn',
    install_requires=[
        'aiofiles',
        'aiohttp',
        # Ideally the stuff in here we use should be broken out into a separate
        # package. Perhaps alma-user-utils.
        'alma-load > 0.1.0, < 1',
        'attrs',
        'docopt',
        'lxml',
        'transitions',
        'yarl'
    ],
    dependency_links=[
        'git+https://bitbucket.org/CUDL/alma-load-public.git@master#egg=alma-load'
    ],
    entry_points={
        'console_scripts': [
            'curl2har=curl2har:main',
            'response2har=response2har:main',
            'almausersync=almausersync.cmdline:sync_main',
            'failed-id-migrations=failed_id_migrations:main',
            'extract-pids=extract_pids:main',
            'extract-key=failed_id_migrations:extract_object_value_main',
            'apply-failed-id-migration-temporary-ids=failed_id_migrations:apply_id_migration_temporary_ids_main',
            'conflictingusers=conflictingusers:main'
        ]
    }
)
