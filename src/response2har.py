"""
Usage: response2har [options] [--header=<value>...] <response-head> [<response-body>] [<request-body>]

Options:
    -m=<method>,--method=<method>
    -u=<url>,--url=<url>
    -o=<file>,--output=<file>
    -h=<value>..., --header=<value>...
        Request header(s) to include
"""
import asyncio
import base64
import cgi
from datetime import datetime
from http.client import HTTPResponse
from io import BytesIO
import json
import math
import os
import sys
import warnings

import docopt
import aiofiles


def parse_response(data):
    resp = HTTPResponse(FakeSocket(data))
    resp.begin()

    result = {
        'status': resp.status,
        'statusText': resp.reason,
        'httpVersion': get_http_version_repr(resp.version),
        'headers': [
            {'name': name, 'value': value}
            for name, value in resp.getheaders()
        ]
    }

    content = parse_content_metadata(resp)
    if content:
        result['content'] = content

    return result


def parse_content_metadata(resp):
    meta = {}
    length = resp.getheader('Content-Length')
    if length is not None:
        meta['size'] = int(length)

    type = resp.getheader('Content-Type')
    if type is not None:
        meta['mimeType'] = type

    return meta


def get_http_version_repr(number):
    if number == 10:
        return 'HTTP/1.0'
    elif number == 11:
        return 'HTTP/1.1'
    raise ValueError(f'Unsupported version: {number}')


class FakeSocket:
    def __init__(self, data):
        self._data = data

    def makefile(self, mode=None, **kwargs):
        assert mode == 'rb'
        return BytesIO(self._data)


async def load_headers(path):
    async with aiofiles.open(path, 'rb') as f:
        return parse_response(await f.read())


async def load_body(path):
    if path is None:
        return

    async with aiofiles.open(path, 'rb') as f:
        return await f.read()


def populate_body(response, body):
    content = response.get('content')

    if content is None:
        if body:
            raise ValueError('response has no content to insert body into')
        # No body expected or received
        return

    size = content.get('size')
    if size is not None and size != len(body):
        warnings.warn(f'response content size mismatch; expected: {size}, '
                      f'actual: {len(body)}', UserWarning)

    charset = cgi.parse_header(content.get('mimeType', ''))[1].get('charset')
    if charset:
        try:
            text = body.decode(charset)
        except Exception as e:
            raise ValueError(
                'Unable to decode body with charset: {charset!r}') from e

        content['text'] = text
    else:
        content['text'] = base64.b64encode(body).decode('ascii')
        content['encoding'] = 'base64'


async def load_response(headers_path, *, body_path=None):
    response, body = await asyncio.gather(
        load_headers(headers_path),
        load_body(body_path))

    if body is not None:
        populate_body(response, body)

    return response


async def create_request(args, excluded_headers=()):

    headers = parse_headers(args, exclude=excluded_headers)
    redact_headers(headers)
    post_data, body_size = await load_post_data(args['<request-body>'], headers)

    request = {
        'method': args['--method'],
        'url': args['--url'],
        'httpVersion': 'HTTP/1.1',
        'headers': headers or None,
        'headersSize': -1,
        'bodySize': -1 if body_size is None else body_size,
        'postData': post_data
    }

    return strip_none(request)


async def load_post_data(path, headers):
    data = await load_body(path)

    if data is None:
        return None, None

    type = next((h['value'] for h in headers
                 if h['name'].lower() == 'content-type'), None)

    if type is None:
        raise ValueError('Can\'t embed <request-body> unless a Content-Type '
                         'header is specified')

    _, params = cgi.parse_header(type)
    charset = params.get('charset', 'utf-8')

    # HAR clearly isn't designed to represent request bodies accurately -
    # can't use the same content representation as response bodies. This works
    # for our needs though (not sending any binary data)...
    return {
        'text': data.decode(charset),
        'mimeType': type
    }, len(data)


def parse_headers(args, exclude=()):
    parsed = (parse_header(value) for value in args['--header'])
    return [h for h in parsed if h['name'].lower() not in exclude]


def parse_header(value):
    # This is simplistic, but we don't need to handle unusual stuff like
    # multi-line values.
    parts = [v.strip() for v in value.split(':', maxsplit=1)]

    if len(parts) == 1:
        raise ValueError(
            f'Invalid header value - contains no colon: {value!r}')

    return {'name': parts[0], 'value': parts[1]}


def if_named(name):
    def match_name(header):
        return header['name'].lower() == name
    return match_name


def redact_api_key():
    def api_key_redaction(header):
        val = header['value']
        if not val.lower().startswith('apikey'):
            return False

        key = val[len('apikey'):].strip()
        redact_frag = 'notarealkey'
        redact_len = max(len(key), len(redact_frag))
        redacted = ('notarealkey' *
                    math.ceil(len(key) / len(redact_frag)))[:redact_len]

        header['value'] = val[:len('apikey')] + ' ' + redacted
        return True
    return api_key_redaction


def redact_generically(text='**REDACTED**'):
    def generic_redaction(header):
        header['value'] = text
        return True
    return generic_redaction


header_redactors = [
    (if_named('authorization'), redact_api_key()),
    (if_named('authorization'), redact_generically())
]


def redact_header(header, redactors):
    for matcher, transformer in redactors:
        if matcher(header) and transformer(header):
            return True
    return False


def redact_headers(headers, redactors=header_redactors):
    for header in headers:
        redact_header(header, redactors)


def strip_none(dictionary):
    return {k: v for k, v in dictionary.items() if v is not None}


def create_har(request, response):
    return {
        'log': {
            'version': '1.2',
            'creator': {
                'name': 'response2har.py',
                'version': '0.0.0'
            }
        },
        'entries': [
            {
                'startedDateTime': f'{datetime.utcnow().isoformat()}Z',
                'time': 0,
                'request': request,
                'response': response
            }
        ]
    }


async def render_har(har, out_path=None):
    if out_path in [None, '-']:
        out_path = sys.stdout.fileno()

    rendered = (json.dumps(har, indent=2) + '\n').encode('utf-8')
    async with aiofiles.open(out_path, 'wb') as f:
        await f.write(rendered)


def get_excluded_headers():
    return set(h.strip().lower() for h in
               os.environ.get('HAR_EXCLUDE_HEADERS', '').split(':'))


async def async_main(args):
    response = await load_response(
        args['<response-head>'],
        **strip_none({'body_path': args['<response-body>']}))
    request = await create_request(args, excluded_headers=get_excluded_headers())

    har = create_har(request, response)

    await render_har(har, out_path=args['--output'])


def main():
    args = docopt.docopt(__doc__)

    asyncio.get_event_loop().run_until_complete(async_main(args))


if __name__ == '__main__':
    main()
