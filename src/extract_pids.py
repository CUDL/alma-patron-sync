"""
Print the primary_ids of users in a JSON stream.

Usage: extract-pids [options] (old|new) (from-user-wrappers|from-jobs)

Stream is read on stdin and ids written to stdout.
"""
import sys
from functools import partial

from almauserutils.jsonstreams import load
from almaload.coroutines import pump, apply
import docopt


def main():
    args = docopt.docopt(__doc__)

    if args['old']:
        extract_id = extract_user_wrapper_orig_pid
    else:
        extract_id = extract_user_wrapper_new_pid

    if args['from-user-wrappers']:
        extractor = extract_id
    else:
        extractor = partial(extract_job_user_id, extract_id)

    pump(load(sys.stdin.buffer),
         apply(extractor, apply(print_id)))


def extract_user_wrapper_orig_pid(user_wrapper):
    id = next(id.get('value') for id in user_wrapper['original_ids']
              if id.get('type') == 'primary')
    return id


def extract_user_wrapper_new_pid(user_wrapper):
    id = user_wrapper['alma_user'].get('primary_id')

    if id is None:
        index = user_wrapper.get('stream_index')
        print(f'Warning: user has no primary_id; stream_index: {index}',
              file=sys.stderr)
        return

    return id


def extract_job_user_id(id_extractor, job):
    input_data = job['job']['input_data']

    if isinstance(input_data, dict):
        user_wrapper = input_data
    else:
        # Crashed job output is a bit screwed ATM
        assert isinstance(input_data, list)
        user_wrapper = input_data[1]

    return id_extractor(user_wrapper)


def print_id(id):
    if id is not None:
        print(id)
