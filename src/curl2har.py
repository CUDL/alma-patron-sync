"""
Usage: curl2har [options] <har-file> <url> [-- <curl-opts>...]
"""
import os
import subprocess
import tempfile

import docopt


def main():
    args = docopt.docopt(__doc__)

    curl_opts = args['<curl-opts>']
    url = args['<url>']
    method = get_opt(curl_opts, names=['-X', '--request'], default='GET')

    har_file = args['<har-file>']
    head_file = tempfile.NamedTemporaryFile('rb')
    body_file = tempfile.NamedTemporaryFile('rb')

    curl_cmd = (['curl'] + ['-o', body_file.name, '-D', head_file.name] +
                curl_opts + [url])
    subprocess.check_call(curl_cmd)

    # Infer significant request headers from curl arguments
    headers = [token for val in get_opts(curl_opts, names=('-H', '--header'))
               for token in ('-h', val)]

    req_body_path, req_body_file = get_request_body(curl_opts)
    req_body = [] if req_body_path is None else [req_body_path]

    subprocess.check_call(
        ['response2har'] + headers +
        ['-m', method, '-u', url, '-o', har_file, head_file.name,
         body_file.name] +
        req_body)


def get_request_body(curl_opts):
    data = get_opt(curl_opts, name='--data-binary', default=None)

    if data is None:
        return None, None

    if data.startswith('@'):
        return data[1:], None

    tf = tempfile.NamedTemporaryFile('wb')
    # Use a @file is careful control of encoding is required...
    tf.file.write(data.encode())
    tf.file.flush()
    os.fsync(tf.file.fileno())

    return tf.name, tf


sentinel = object()


def get_opt(args, *, name=None, names=None, default=sentinel):
    try:
        return next(get_opts(args, name=name, names=names, default=default))
    except StopIteration:
        pass
    if default is not sentinel:
        return default
    raise ValueError('Not in args: {}'.format(', '.join(names))) from None


def get_opts(args, *, name=None, names=None, default=sentinel):
    if sum(x is None for x in [name, names]) != 1:
        raise ValueError('One of name or names must be provided')

    if names is None:
        names = [name]

    for i, arg in enumerate(args):
        if arg in names:
            try:
                yield args[i + 1]
            except (ValueError, IndexError):
                pass


if __name__ == '__main__':
    main()
