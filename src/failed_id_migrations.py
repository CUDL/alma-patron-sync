"""
Extract details of users who failed ID migration from a sync WAL file.

Usage: failed-id-migrations [options] [<input> [<output>]]

Reads WAL on stdin, writes JSON stream of info objects on stdout.
"""
from functools import partial

import attr
import docopt
import sys
from enum import Enum, auto

from almaload.coroutines import pump, apply
from almauserutils.jsonstreams import load, dump_incremental


class IdMigrationState(Enum):
    NEW = auto()
    STARTED_PHASE_1 = auto()
    FINISHED_PHASE_1 = auto()
    FAILED_PHASE_1 = auto()
    STARTED_PHASE_2 = auto()
    FAILED_PHASE_2 = auto()
    FINISHED_PHASE_2 = auto()


@attr.s(slots=True)
class IdMigrationJobState:
    current_primary_id = attr.ib(default=None)
    final_primary_id = attr.ib(default=None)
    temporary_id = attr.ib(default=None)

    state = attr.ib(default=None)

    def is_finished(self):
        return self.state == IdMigrationState.FINISHED_PHASE_2

    def handle(self, msg):
        barrier = msg.get('barrier')
        src = msg.get('source')
        sub_msg = msg.get('message')
        sub_barrier = (sub_msg or {}).get('barrier')

        if barrier == 'before-id-migration':
            assert self.state is None
            self.state = IdMigrationState.NEW
            self.current_primary_id = msg['current_primary_id']
            self.final_primary_id = msg['final_primary_id']
            self.temporary_id = msg['temporary_id']
        elif (src == 'id_migrate_intermediate' and
              sub_barrier == 'before-attempting'):
            pass
        elif (src == 'id_migrate_intermediate' and
              sub_barrier == 'increment-request-count'):
            assert self.state in [IdMigrationState.NEW,
                                  IdMigrationState.FAILED_PHASE_1]
            self.state = IdMigrationState.STARTED_PHASE_1
        elif (src == 'id_migrate_intermediate' and sub_barrier == 'succeeded'):
            assert self.state == IdMigrationState.STARTED_PHASE_1
            self.state = IdMigrationState.FINISHED_PHASE_1
        elif (src == 'id_migrate_intermediate' and sub_barrier == 'failed'):
            assert self.state == IdMigrationState.STARTED_PHASE_1
            self.state = IdMigrationState.FAILED_PHASE_1

        elif (src == 'id_migrate_final' and
              sub_barrier == 'before-attempting'):
            pass
        elif (src == 'id_migrate_final' and
              sub_barrier == 'increment-request-count'):
            assert self.state in [IdMigrationState.FINISHED_PHASE_1,
                                  IdMigrationState.FAILED_PHASE_2]
            self.state = IdMigrationState.STARTED_PHASE_2
        elif (src == 'id_migrate_final' and sub_barrier == 'succeeded'):
            assert self.state == IdMigrationState.STARTED_PHASE_2
            self.state = IdMigrationState.FINISHED_PHASE_2
        elif (src == 'id_migrate_final' and sub_barrier == 'failed'):
            assert self.state == IdMigrationState.STARTED_PHASE_2
            self.state = IdMigrationState.FAILED_PHASE_2
        else:
            raise ValueError(f'Unexpected message: {msg}')


def get_handler(msg):
    if msg.get('barrier') == 'before-id-migration':
        return IdMigrationJobState()
    return None


DONE = object()


def _handle_messages(jobs, messages):
    for msg in messages:
        job_id = msg['source']
        sub_msg = msg['message']

        if job_id not in jobs:
            jobs[job_id] = get_handler(sub_msg)

        handler = jobs[job_id]

        if handler is DONE:
            raise RuntimeError(
                f'encountered message after handler reported being finished; '
                f'job: {job_id}, msg: {msg}')

        if handler is not None:
            handler.handle(sub_msg)
            if handler.is_finished():
                jobs[job_id] = DONE


def _report_failed_id_migrations(jobs, output):
    out = dump_incremental(output)

    for job_id, job_state in jobs.items():
        if job_state is DONE or job_state is None:
            continue

        out.send({
            'job_id': job_id,
            'state': job_state.state.name,
            'current_primary_id': job_state.current_primary_id,
            'final_primary_id': job_state.final_primary_id,
            'temporary_id': job_state.temporary_id
        })


def main():
    args = docopt.docopt(__doc__)  # just for -h help
    input = (sys.stdin.buffer if args['<input>'] in [None, '-']
             else open(args['<input>'], 'rb'))
    output = (sys.stdout.buffer if args['<output>'] in [None, '-']
              else open(args['<output>'], 'wb'))

    jobs = {}
    messages = load(input)

    _handle_messages(jobs, messages)
    _report_failed_id_migrations(jobs, output)


extract_object_value_main_usage = """
Extract the value of a key from each of the objects in a stream

Usage: extract-key [options] <key>
"""


def extract_object_value_main():
    args = docopt.docopt(extract_object_value_main_usage)
    key = args['<key>']

    pump(load(sys.stdin.buffer),
         apply(partial(extract_key, key), apply(print)))


def extract_key(key, obj):
    return obj[key]


apply_id_migration_temporary_ids_usage = """
Apply the temporary ID migration IDs to a stream of user wrappers. Each
wrapper's primary original_id is set to the temporary migration ID, so that
when synced they'll match the temporary ID their account currently has on Alma.

Usage: apply-failed-id-migration-temporary-ids [options] <failed-id-migration-states>
"""


# {"job_id":729,"state":"FAILED_PHASE_2","current_primary_id":"VK638","final_primary_id":"0e60732e-029c-433d-b9f8-9fcbea654f82","temporary_id":"5b9c35e4-1874-453c-9b65-fd99d3a70a5c"}


def apply_id_migration_temporary_ids_main():
    args = docopt.docopt(apply_id_migration_temporary_ids_usage)

    with open(args['<failed-id-migration-states>'], 'rb') as f:
        pid_to_tmp_id = load_temporary_id_map(load(f))

    pump(load(sys.stdin.buffer),
         apply(partial(map_orig_id_to_migration_tmp_id, pid_to_tmp_id),
               dump_incremental(sys.stdout.buffer)))


def map_orig_id_to_migration_tmp_id(pid_to_tmp, user_wrapper):
    pid = user_wrapper['alma_user']['primary_id']
    tmp_id = pid_to_tmp[pid]

    orig_pid = next(id for id in user_wrapper['original_ids']
                    if id['type'] == 'primary')

    orig_pid['value'] = tmp_id
    return user_wrapper


def load_temporary_id_map(failed_id_states):
    return {state['final_primary_id']: state['temporary_id']
            for state in failed_id_states}


if __name__ == '__main__':
    main()
