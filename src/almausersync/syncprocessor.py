import asyncio
from collections.abc import Mapping, Iterator, Awaitable

import attr
import itertools
import warnings
from abc import ABC, abstractmethod
from transitions import Machine

from almausersync.exceptions import AlmaUserSyncWarning
from almausersync.states import requirestate, IllegalStateError
from almausersync.wal import wrap_with_source

# TODO: clean up these rambling thoughts/notes
"""
# Architectual Notes

It's necessary to be able to recover from execution of the synchronisation
being aborted mid way through. Primarily because updating users entails
non-atomic operations, and if we lose track of the progression of the
operations we can lose data. Secondarily, being able to resume mid-way through
allows us to avoid re-performing previously completed work.

In order to support re-performing failed synchronisation without the
possibility of data loss the core of the application is divided into three
areas:
    - state objects
    - normal drivers
    - recovery drivers

State objects perform no work by themselves, only maintain the runtime state of
the system during execution.

In the normal course of execution, normal drivers are attached to state objects
and perform synchronisation activities. State transitions across critical
barriers are recorded in the write-ahead log prior to an action being
performed.

After an incomplete, aborted execution, the recovery drivers are attached to
state objects and use the write-ahead log from the previous execution to replay
state transitions, re-creating the state at the point of the last barrier that
was crossed prior to the failed execution's abortion. Normal drivers can then
be attached to this re-created state and executed in order to resume the
process.



The SynchronisationProcessor is responsible for orchistrating the the
individual sync jobs, each of which syncs a single user to Alma.

The processor ensures that state changes are committed to the write-ahead
log such that the state of the processor can be re-created and resumed
following a failure (network, power, crash etc).

The processor limits the number of ongoing sync jobs in order to contain
resource usage, as the total number of jobs to be processed is in the
hundreds of thousands.
"""
"""
We can async processes the WAL, creating sync sending events and sending
them to active sync jobs asynchronously. E.g. each job is a coroutine.

- Use context manager to keep track of async running subtasks
- Use priority queue to manage pending API requests such that we can allow
  pairs of requests to run sequentially.
    - Or allow a sequence of requests to be submitted that must be run
      sequentially
"""
"""
Jobs need:
- type
- data - sent to driver via dispatch
- state - the state instance used by the driver
    - driver only sees state, not job itself
- result (future)


function(type, data) -> state
create the state obj to be used to maintain the job's state

# This is used to create replay or normal driver. For replay a WAL is provided
function(job) -> drivable(run() method?)

function(state, wal_writer) -> drivable (normal)
function(state, wal_reader) -> drivable (replay)

# store the normal driver's future as the result

# How do we create the initial job (or the state on the job?)
function(type, data)


- Has to reference the operations underneith
    - e.g. a job somehow maintains the state of our UserUpdateOperation
- Job has a future representing completion
    - But only in driver, state knows noting of actual execution
"""


def validate_is_nonzero(_, attrib, value):
    if value < 1:
        raise ValueError(f'{attrib.name!r} must be at least 1; got: {value!r}')


class State(ABC):
    @property
    @abstractmethod
    def is_settled(self):
        pass


class JobFactory(ABC):
    @abstractmethod
    def create_state(self, type, data):
        pass

    @abstractmethod
    def create_driver(self, state, wal_writer) -> Awaitable:
        """Returns: An awaitable which runs the driver.
        """
        pass

    @abstractmethod
    def create_replay_driver(self, state, wal_reader) -> Awaitable:
        pass


@attr.s(slots=True)
class Job:
    id = attr.ib()
    type = attr.ib()
    input_data = attr.ib()
    state: State = attr.ib()
    result: asyncio.Future = attr.ib(default=None)

    @property
    def is_finished(self):
        return (self.state is None or
                self.state.is_settled) and self.result.done()


DEFAULT_ACTIVE_JOB_LIMIT = 100
# Job type value attached to Jobs that fail during creation.
JOB_TYPE_CREATION_FAILED = 'JOB_TYPE_CREATION_FAILED'


@attr.s()
class AsyncWalProcessorState(State):

    states = ['receiving_jobs', 'finishing_jobs', 'closing_output', 'finished']

    transitions = [
        ['all_received', 'receiving_jobs', 'finishing_jobs'],
        ['all_sent', 'finishing_jobs', 'closing_output'],
        ['finish', 'finishing_jobs', 'finished']
    ]

    machine: Machine = attr.ib(init=False)
    input_queue: asyncio.Queue = attr.ib()
    output_queue: asyncio.Queue = attr.ib()
    job_factory: JobFactory = attr.ib()
    active_job_limit: int = attr.ib(default=DEFAULT_ACTIVE_JOB_LIMIT,
                                    validator=validate_is_nonzero)
    id_pool: Iterator = attr.ib(default=attr.Factory(itertools.count))
    jobs: Mapping = attr.ib(init=False, default=attr.Factory(dict))

    @machine.default
    def _(self):
        return Machine(model=self, states=self.states,
                       transitions=self.transitions, initial='receiving_jobs',
                       auto_transitions=False)

    def __attrs_post_init__(self):
        pass

    @property
    def is_settled(self):
        return self.state == 'finished'

    @requirestate('receiving_jobs')
    def submit_job(self, job: Job):
        if job.id in self.jobs:
            raise ValueError(f'Job already exists with id: {job.id!r}')

        if len(self.jobs) >= self.active_job_limit:
            raise IllegalStateError('No spare job slots are available')

        self.jobs[job.id] = job

    def finish_job(self, job):
        assert self.jobs[job.id] is job
        del self.jobs[job.id]

    @property
    def has_capacity(self):
        return len(self.jobs) < self.active_job_limit

    @property
    def any_tasks_running(self):
        return any(not job.result.done() for job in self.jobs.values())

    @property
    def any_task_complete(self):
        return any(self._completed_jobs_generator())

    @property
    def completed_jobs(self):
        return list(self._completed_jobs_generator())

    def get_next_completed_job(self):
        return next(self._completed_jobs_generator(), None)

    def _completed_jobs_generator(self):
        return (job for job in self.jobs.values() if job.result.done())

    @property
    def all_jobs_done(self):
        return self.state != 'receiving_jobs' and len(self.jobs) == 0


def cancel_all(futures: asyncio.Future):
    for future in futures:
        future.cancel()


def access_results(futures: asyncio.Future):
    for future in futures:
        future.result()


@attr.s()
class AsyncWalProcessorDriver:
    """
    Responsible for controlling the SyncProcessorState during normal execution.
    """

    _state: AsyncWalProcessorState = attr.ib()
    _wal_writer = attr.ib()
    _unqueued_input = attr.ib(init=False, default=None)

    def _running_tasks(self):
        return [job.result for job in self._state.jobs.values()
                if not job.result.done()]

    async def _next_task_completion(self):
        # This won't normally happen, as we only schedule this when tasks are
        # running, but tasks can finish in between sheduling and running.
        tasks = self._running_tasks()
        if len(tasks) == 0:
            return

        # Don't need to clean up pending tasks from wait as they're all things
        # we're interested in and will be awaited later (and they're already
        # futures, so wait() isn't creating new ones).
        # Need to shield to avoid cancelation cleanup in _next_activity()
        # cancelling our actual tasks!
        await asyncio.shield(asyncio.wait(self._running_tasks(),
                             return_when=asyncio.FIRST_COMPLETED))

    @property
    def is_input_available(self):
        return self._unqueued_input is not None or (
            self._state.state == 'receiving_jobs' and
            not self._state.input_queue.empty())

    @property
    def can_send_output(self):
        return (not self._state.output_queue.full() and
                self._state.any_task_complete)

    @property
    def has_unsendable_output(self):
        return (self._state.output_queue.full() and
                self._state.any_task_complete)

    async def _send_a_completed_task(self):
        """Send at one finished job to the output queue, waiting until it's
        gone. Returns right away if the output queue is not full.
        """
        job = self._state.get_next_completed_job()
        if job is None:
            warnings.warn(
                'Entered _send_a_completed_task without a task available',
                AlmaUserSyncWarning)
            return

        # We expect the queue to be full as this coro only gets scheduled when
        # it is. However by the time we're executed it may have been drained.
        if self._state.output_queue.full():
            await self._state.output_queue.put(job)
            self._state.finish_job(job)

    async def _input_available(self):
        if self._unqueued_input is not None:
            return

        input = await self._state.input_queue.get()

        # sending None on the queue marks end of input
        if input is None:
            self._state.all_received()

        self._unqueued_input = input
        return

    @property
    def want_input(self):
        return self._state.state == 'receiving_jobs'

    async def _next_activity(self):
        waits = []
        if self.want_input and not self.is_input_available:
            waits.append(self._input_available())

        if self._state.any_tasks_running:
            waits.append(self._next_task_completion())

        if self.has_unsendable_output:
            waits.append(self._send_a_completed_task())

        # This shouldn't be the normal path but can happen sometimes as other
        # coros run between this being scheduled and actually executing.
        if not waits:
            return

        # Wait for the first of the available blocks
        done, pending = await asyncio.wait(waits,
                                           return_when=asyncio.FIRST_COMPLETED)
        cancel_all(pending)
        access_results(done)

    def _send_available_output(self):
        for job in self._state.completed_jobs:
            try:
                self._state.output_queue.put_nowait(job)
            except asyncio.QueueFull:
                return
            self._state.finish_job(job)

    def _consume_available_input(self):
        if not self._state.has_capacity:
            return

        if self._unqueued_input is not None:
            self._create_job(self._unqueued_input)
            self._unqueued_input = None

        while self._state.has_capacity:
            try:
                input = self._state.input_queue.get_nowait()
                if input is None:
                    self._state.all_received()
                    return
            except asyncio.QueueEmpty:
                return
            self._create_job(input)

    def _create_job(self, input):
        id = next(self._state.id_pool)
        child_wal = wrap_with_source(self._wal_writer, id)
        type, data, state, driver = None, None, None, None
        try:
            type, data = input
            state = self._state.job_factory.create_state(type, data)
            driver = self._state.job_factory.create_driver(state, child_wal)
        except Exception as e:
            exc_future = asyncio.Future()
            exc_future.set_exception(e)
            job = Job(id=id, type=JOB_TYPE_CREATION_FAILED, input_data=input,
                      state=state, result=exc_future)
        else:
            job = Job(id=id, type=type, input_data=data, state=state,
                      result=asyncio.ensure_future(driver))
        self._state.submit_job(job)

    async def _finish_output(self):
        assert self._state.all_jobs_done
        await self._state.output_queue.put(None)
        self._state.finish()

    async def run(self):
        while True:
            if self._state.state == 'finished':
                return

            self._send_available_output()
            if self.want_input:
                self._consume_available_input()

            if self._state.all_jobs_done:
                await self._finish_output()
                continue

            await self._next_activity()


# TODO: Implement replaying from the WAL
class AsyncWalProcessorReplayDriver:
    def __init__(self):
        raise NotImplementedError()
