import asyncio
import math
import time

_sentinel = object()


async def anext(asic_iter, default=_sentinel):
    if default is not _sentinel:
        try:
            return await asic_iter.__anext__()
        except StopAsyncIteration:
            return default

    return await asic_iter.__anext__()


def default_loop_or(loop=None):
    if loop is None:
        return asyncio.get_event_loop()
    return loop


# Note that the ratelimiter library does not currently rate limit async code
# correctly - it allows too many requests though. Hence the need for these.
# (TODO: Should work with the ratelimiter author(s) to resolve that issue.)
#
# Initially It seemed Alma wanted a hard limit on request concurrency to
# n req/s, but they don't seem at all strict about enforcing rate limits, so
# I suspect a more general throughput limiting approach is fine (hence
# BurstingRateLimiter).


class SlidingWindowRateLimiter:
    """
    An async context manager that throttles the throughput and concurrency of
    flow entering the a block.

    Entries are restricted such that the block can be active no more than a
    set number of times in a sliding window of the defined number of seconds.

    For example, if the time period is 1 second, 10 entries can be made and the
    blocks take 2 seconds each to execute and there are 20 entries to make:
        - Initially 10 blocks will be entered.
        - After 1 second the initial 10 blocks are still active, so no more can
          be entered
        - After 2 seconds the initial 10 blocks will exit, and the next 10 can
          start
    """
    def __init__(self, max_calls, time_period, loop=None):
        if max_calls < 1:
            raise ValueError(f'max_calls must be >= 1, got: {max_calls}')

        if time_period <= 0:
            raise ValueError(f'time_period must be >= 0, got: {time_period}')

        self._loop = default_loop_or(loop)
        self._max_calls = max_calls
        self._time_period = float(time_period)

        self._finished = asyncio.Queue()
        self._available = asyncio.Queue()
        self._recycler = None
        self._waiters = 0

        for id in range(self._max_calls):
            self._available.put_nowait(id)

    def _timestamp(self):
        return time.monotonic()

    def limit(self):
        return AsyncGeneratorContextManager(self._limiter())

    def _schedule_recycler(self):
        # If a recycle is already scheduled there's no point in doing another
        # recycle now, as one will already be performed at the earliest time
        # that could successfully recycle a finished call
        if self._recycler is not None:
            return

        self._recycler = self._loop.create_task(self._recycle_ids())

    async def _recycle_ids(self):
        while not self._finished.empty() or self._waiters > 0:

            id, finish_time = await self._finished.get()
            expires_at = finish_time + self._time_period
            now = self._timestamp()

            while now < expires_at:
                await asyncio.sleep(max(0, expires_at - self._timestamp()))
                now = self._timestamp()

            self._available.put_nowait(id)

        self._recycler = None

    async def _limiter(self):
        self._waiters += 1
        id = await self._available.get()

        try:
            yield
        finally:
            self._waiters -= 1
            assert self._waiters >= 0

            self._finished.put_nowait((id, self._timestamp()))
            self._schedule_recycler()


class BurstingRateLimiter:
    """
    An async context manager that throttles the throughput, but not concurrency
    of flow entering a block.

    The limiter earns permits as time elapses which are consumed each time the
    block is entered. If no permits are available entering blocks until one is
    earnt.

    If permits are not spent as they're earnt, they can build up and be used
    to allow entries to burst, rather than being restricted to precisely to
    the per-second rate.
    """
    def __init__(self, per_second, burst=None, loop=None, start_full=False):
        if per_second <= 0:
            raise ValueError('per_second must be > 0, got: {per_second}')

        if burst is not None and burst < 0:
            raise ValueError('burst cannot be negative, got: {burst}')

        burst = math.ceil(per_second if burst is None else max(1, burst))

        self._per_second = per_second
        self._loop = default_loop_or(loop)
        self._permits = asyncio.Queue(maxsize=burst)

        # The timestamp marking the start of the duration we've been allocating
        # permits during.
        self._base_timestamp = self._timestamp()
        # Number of permits allocated in between _base_time and now
        self._permits_allocated = 0

        self._replenisher = None
        self._waiters = 0

        if start_full:
            for _ in range(burst):
                self._permits.put_nowait(None)

    def _schedule_replinisher(self):
        if self._replenisher is None:
            self._replenisher = self._loop.create_task(
                self._replenish_permits())

    def _replenish_once(self):
        now = self._timestamp()

        # A fixed reference point is maintained rather just waiting
        # 1/_per_second seconds after each sleep. This prevents scheduling
        # slop and floating-point errors accumulating, which would affect the
        # throughput.
        permits_earnt = ((now - self._base_timestamp) * self._per_second) \
            - self._permits_allocated

        burst_capacity = self._permits.maxsize - self._permits.qsize()
        # If we've been idle long enough to over-fill our burst capacity we've
        # implicitly dropped capacity, so we can take this opportunity to reset
        # our baseline timestamp
        if permits_earnt > burst_capacity:
            progress_to_next = 0
            burst_count = burst_capacity
            self._base_timestamp = now
            self._permits_allocated = 0
        else:
            progress_to_next = permits_earnt % 1
            burst_count = math.floor(permits_earnt)
            self._permits_allocated += burst_count

        secs_per_permit = 1 / self._per_second
        secs_to_next_permit = secs_per_permit * (1 - progress_to_next)

        for _ in range(burst_count):
            self._permits.put_nowait(None)

        return secs_to_next_permit

    async def _replenish_permits(self):

        secs_to_next_permit = self._replenish_once()

        while self._waiters > 0:
            try:
                await asyncio.sleep(secs_to_next_permit)
            except asyncio.CancelledError:
                break

            secs_to_next_permit = self._replenish_once()

        self._replenisher = None

    def _timestamp(self):
        return time.monotonic()

    def limit(self):
        return AsyncGeneratorContextManager(self._limiter())

    async def _limiter(self):
        try:
            self._waiters += 1
            self._schedule_replinisher()
            await self._permits.get()
            yield
        finally:
            self._waiters -= 1
            assert self._waiters >= 0
            if self._waiters == 0 and self._replenisher is not None:
                self._replenisher.cancel()


class UnlimitedRateLimiter:
    """A noop RateLimiter which doesn't actually limit at all."""
    def limit(self):
        return AsyncGeneratorContextManager(self._limiter())

    async def _limiter(self):
        yield


class AsyncGeneratorContextManager:
    def __init__(self, agen):
        self._agen = agen

    async def __aenter__(self):
        return await anext(self._agen)

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        if exc_val is not None:
            return await self._agen.athrow(exc_type, exc_val, exc_tb)

        try:
            await anext(self._agen)
            raise RuntimeError(
                'async generator unexpectedly yielded a second time')
        except StopAsyncIteration:
            return
