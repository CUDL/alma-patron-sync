import attr

from almaload.almausers import wrapper_keys
from almausersync.exceptions import TooManyAttemptsError
from almausersync.jobs import DeleteAPIOperation, DeleteAPIOperationDriver
from almausersync.operations.update_user import UpdateUserOperation, \
    UpdateUserOperationDriver
from almausersync.syncprocessor import JobFactory
from almausersync.utils import get_original_ids, get


class NoopJobFactory(JobFactory):
    """Example job factory which sends input through unchanged and fails
    sometimes.
    """
    i = -1

    def create_driver(self, state, wal_writer):
        async def _driver():
            await wal_writer.log('Hi.')
            self.i += 1
            if self.i % 3 == 0:
                raise ValueError('Boom!')
            elif self.i % 4 == 0:
                raise TooManyAttemptsError('Stop trying so hard')

            return state[1]

        return _driver()

    def create_replay_driver(self, state, wal_reader):
        raise NotImplementedError()

    def create_state(self, type, data):
        return (type, data)


@attr.s()
class AlmaSyncJobFactory(JobFactory):
    _user_api = attr.ib()
    _retry_strategy = attr.ib()

    @property
    def _user_api_args(self):
        args = [('userapi', self._user_api),
                ('retry_strategy', self._retry_strategy)]

        return {k: v for k, v in args if v is not None}

    def create_state(self, type, data):
        if type != 'alma-user-wrapper':
            raise ValueError(f'Unsupported input type: {type}')

        if self._is_delete_wrapper(data):
            return DeleteAPIOperation(user_wrapper=data)
        elif self._is_update_wrapper(data):
            return UpdateUserOperation(user_wrapper=data)

        raise ValueError(f'Unrecognised data of type {type}')

    def create_driver(self, state, wal_writer):
        if isinstance(state, UpdateUserOperation):
            instance = UpdateUserOperationDriver(
                state=state, wal_writer=wal_writer, **self._user_api_args)
            return instance.run()  # coro
        elif isinstance(state, DeleteAPIOperation):
            instance = DeleteAPIOperationDriver(
                state=state, wal_writer=wal_writer, **self._user_api_args)
            return instance.run()

        raise ValueError(f'No driver available for state: {state!r}')

    def create_replay_driver(self, state, wal_reader):
        raise NotImplementedError()

    @staticmethod
    def _is_delete_wrapper(wrapper):
        return (~wrapper_keys.alma_user in wrapper and
                wrapper[~wrapper_keys.alma_user] is None and
                len(get_original_ids(wrapper)) > 0)

    @staticmethod
    def _is_update_wrapper(wrapper):
        return (get(wrapper, ~wrapper_keys.alma_user, None, dict)
                is not None and len(get_original_ids(wrapper)) > 0)
