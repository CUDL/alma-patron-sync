import json

from almausersync.async_coroutines import async_prewarmed


class JsonStreamError(ValueError):
    pass


class JsonInvalidJsonStreamError(JsonStreamError):
    pass


class FinalLineTruncatedJsonStreamError(JsonStreamError):
    pass


async def load_async(async_file):
    """
    Asynchronously load the objects in a newline-delimited JSON stream.

    Arguments:
        async_file: An async text-mode file, as returned by aiofiles.open()
    Returns: An async generator which yields each decoded JSON object.
    """
    async for line in async_file:
        if line[-1] != '\n':
            raise FinalLineTruncatedJsonStreamError(
                'line doesn\t end with \\n')
        try:
            yield json.loads(line)
        except json.JSONDecodeError as e:
            raise JsonInvalidJsonStreamError(
                'line doesn\'t contain valid JSON') from e


@async_prewarmed
async def dump_incremental_async(async_file):
    """
    An async generator which writes received values to a newline-delimited JSON
    stream.
    """
    while True:
        obj = yield
        await async_file.write(json.dumps(
            obj, indent=None, separators=(',', ':')))
        await async_file.write('\n')
