import asyncio
from collections import deque
from collections.abc import Callable

import attr
import os
from abc import ABC, abstractmethod

from almausersync import jsonstreams
from almausersync.async_utils import anext, default_loop_or


def _validate_obj_args(obj, objects):
    if obj is None and objects is None:
        raise ValueError('no object(s) provided')

    if obj is not None and objects is not None:
        raise ValueError(
            'either obj or objects can be specified, not both')

    if obj is not None:
        objects = [obj]

    return objects


class WalWriter(ABC):
    @abstractmethod
    async def log(self, obj=None, objects=None):
        pass


class JsonStreamWalWriter(WalWriter):
    """
    A Write Ahead Log writer implemented as file containing a newline-delimited
    JSON stream.
    """
    def __init__(self, file, loop=None):
        self._loop = default_loop_or(loop)
        self._file = file
        self._jsonstream = None
        self._lock = asyncio.Lock(loop=loop)

    @classmethod
    def create(cls, file, loop=None, buffer=True):
        writer = JsonStreamWalWriter(file=file, loop=loop)

        if buffer is True:
            return BufferingWalWriter(wal_writer=writer, loop=loop)
        return writer

    async def _ensure_written(self):
        """
        Ensure writes to our file are flushed to disk.
        """
        await self._file.flush()
        # aiofiles doesn't provide fsync() so we have to call it on a
        # background thread ourselves.
        await self._loop.run_in_executor(None, os.fsync, self._file.fileno())

    async def log(self, obj=None, objects=None):
        """
        Write obj to the log with a guarantee that it's written to storage
        before returning.
        """
        objects = _validate_obj_args(obj, objects)

        # Could enqueue obj here and write all enqueued items once lock is
        # obtained to write potentially > 1 obj per fsync.
        async with self._lock:
            if self._jsonstream is None:
                self._jsonstream = await jsonstreams.dump_incremental_async(
                    self._file)

            for obj in objects:
                await self._jsonstream.asend(obj)
            await self._ensure_written()


@attr.s()
class BufferingWalWriter(WalWriter):
    @attr.s(slots=True, frozen=True)
    class LogBuffer:
        on_written = attr.ib()
        objects = attr.ib(init=False, default=attr.Factory(list))

        @classmethod
        def create(cls, loop):
            return cls(on_written=asyncio.Event(loop=loop))

    _wal_writer = attr.ib()
    _loop = attr.ib(default=attr.Factory(asyncio.get_event_loop))
    _writing: LogBuffer = attr.ib(init=False)
    _pending: LogBuffer = attr.ib(init=False)
    _spare = attr.ib(init=False)

    def __attrs_post_init__(self):
        self._writing = None
        self._pending = None
        self._spare = deque(BufferingWalWriter.LogBuffer.create(self._loop)
                            for _ in range(2))

    async def _do_write(self):
        assert self._writing is not None

        await self._wal_writer.log(objects=self._writing.objects)

        # wake up anyone waiting on this write finishing
        self._writing.on_written.set()
        self._writing.on_written.clear()
        self._writing.objects.clear()

        # Return the write buffer to the pool
        self._spare.append(self._writing)
        self._writing = None

    async def log(self, obj=None, objects=None):
        objects = _validate_obj_args(obj, objects)

        # We double-buffer writes:
        # - if no writes are ongoing then the write goes through right away
        # - if a write is active, the requested write is buffered and performed
        #   as soon as the current write is done.
        # This should allow more writes per flush + fsync.

        if self._writing is None and self._pending is None:
            # Nothing queued so just log our message(s) right away
            assert len(self._spare) == 2

            self._writing = self._spare.popleft()
            self._writing.objects.extend(objects)

            await self._do_write()
        elif self._pending is None:
            # The first blocked writer is responsible for performing the actual
            # buffered write once the current write is done

            assert len(self._spare) == 1

            self._pending = self._spare.popleft()
            self._pending.objects.extend(objects)

            await self._writing.on_written.wait()

            assert self._writing is None
            assert self._pending is not None
            assert len(self._spare) == 1

            self._writing, self._pending = self._pending, None
            await self._do_write()
        else:
            assert self._pending is not None
            assert len(self._spare) in [0, 1]

            # The pending queue is already established - we can add our logs
            # there and wait for it to be cleared. The writing buffer may be
            # none if it's just been cleared and the first pending writer has
            # not yet awoken to clear the buffer.
            self._pending.objects.extend(objects)
            await self._pending.on_written.wait()


def wrap_with_source(wal_writer, source_name):
    """Create a sub WAL writer whos messages are wrapped in an object identifying
    them as being from the given source name.
    """
    return NestedJsonStreamWalWriter(wal_writer=wal_writer,
                                     wrapper=_source_wrapper(source_name))


def _source_wrapper(source_name):
    def wrap(obj):
        return {
            'source': source_name,
            'message': obj
        }
    return wrap


@attr.s()
class NestedJsonStreamWalWriter:
    _wal_writer = attr.ib()
    _wrapper = attr.ib(validator=attr.validators.instance_of(Callable))

    async def log(self, obj):
        wrapped = self._wrapper(obj)
        await self._wal_writer.log(wrapped)


class JsonStreamWalReader:
    def __init__(self, file, loop=None):
        self._loop = default_loop_or(loop)
        self._file = file
        self._jsonstream = jsonstreams.load_async(file)
        self._at_end = False

    def __aiter__(self):
        return self

    async def __anext__(self):
        if self._at_end:
            raise StopAsyncIteration()
        try:
            return await anext(self._jsonstream)
        # The final line may well be incomplete if the process is aborted mid
        # way through writing an entry. We can handle this as if the final
        # entry was never written.
        except jsonstreams.FinalLineTruncatedJsonStreamError:
            self._at_end = True
            raise StopAsyncIteration()
