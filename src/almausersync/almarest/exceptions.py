__all__ = [
    'AlmaApiWarning', 'BaseAlmaApiError', 'BaseHttpResponseAlmaApiError',
    'ErrorResponseAlmaApiError', 'HardAlmaApiError', 'InvalidInputApiError',
    'UnexpectedResponseAlmaApiError', 'TemporaryAlmaApiError']

from .constants import (ErrorCode, ERR_THRESHOLD_DAILY, ERR_THRESHOLD_QPS,
                        Status)


class AlmaApiWarning(UserWarning):
    pass


class BaseAlmaApiError(Exception):
    """Base class for all other Alma API-related exceptions."""

    def __init__(self, *args, is_transitory=False):
        super().__init__(*args)
        self.transitory = is_transitory


class InvalidInputApiError(BaseAlmaApiError):
    """An error raised when an invalid input (e.g. user object) was provided to
    the API. No error code, etc is available as the failure occured prior to a
    request being made."""


class BaseHttpResponseAlmaApiError(BaseAlmaApiError):
    def __init__(self, url, method, http_status, **kwargs):
        super().__init__(**kwargs)

        self.url = url
        self.method = method
        self.http_status = http_status

    @classmethod
    def from_response(cls, response, *args, **kwargs):
        return cls(response.url, response.method, response.status,
                   *args, **kwargs)

    @property
    def is_client_error(self):
        return self.http_status in range(400, 500)

    @property
    def is_server_error(self):
        return self.http_status in range(500, 600)

    def get_details_message(self):
        return None

    def __str__(self):
        msg = f'API {self.method} request to {self.url} received HTTP {self.http_status}'
        details = self.get_details_message()

        if details is not None:
            return f'{msg} - {details}'
        return msg


class ErrorResponseAlmaApiError(BaseHttpResponseAlmaApiError):
    def __init__(self, url, method, http_status, error_code, error_message,
                 tracking_code, **kwargs):
        super().__init__(url, method, http_status, **kwargs)

        self.error_code = error_code
        self.error_message = error_message
        self.tracking_code = tracking_code

    @classmethod
    def from_response(cls, response, error_code, error_message, tracking_code):
        if ((error_code in (ERR_THRESHOLD_DAILY.code, ERR_THRESHOLD_QPS.code) and
                response.status == Status.TOO_MANY_REQUESTS.value) or
                response.status == Status.SERVICE_UNAVAILABLE.value):
            target_cls = TemporaryAlmaApiError
        else:
            target_cls = HardAlmaApiError

        return target_cls(response.url, response.method, response.status,
                          error_code, error_message, tracking_code)

    def get_details_message(self):
        return self._format_error_details()

    def _format_error_details(self):
        if self.error_code:
            return f'{self.error_code}: {self.error_message}'
        return '[response contained no specific error information]'

    def has_code(self, code):
        if isinstance(code, ErrorCode):
            return self.error_code == code.code
        return self.error_code == code


class HardAlmaApiError(ErrorResponseAlmaApiError):
    """An error that's not known to be safe to retry, and is likely to occur
    again if the operation is retried without modification.
    """


class TemporaryAlmaApiError(ErrorResponseAlmaApiError):
    """An API error which indicates an operation was not performed, and it's
    safe to retry the operation without modification.
    """
    def __init__(self, *args):
        super().__init__(*args, is_transitory=True)


class UnexpectedResponseAlmaApiError(BaseHttpResponseAlmaApiError):
    """An error raised when a response is received from the Alma API which
    doesn't conform to the documented API behaviour.
    """
    def __init__(self, url, method, http_status, msg):
        super().__init__(url, method, http_status)

        self.msg = msg

    def get_details_message(self):
        return self.msg
