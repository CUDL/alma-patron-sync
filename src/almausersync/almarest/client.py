import aiohttp
import cgi
import http
import json
import re
import sys
import urllib.parse
import warnings
from lxml import etree
from yarl import URL

from almaload.almausers import keys
from almausersync.async_utils import (BurstingRateLimiter,
                                      UnlimitedRateLimiter)
from . import constants as const
from .exceptions import *

__all__ = ['AlmaApiConfig', 'Session', 'UserApiClient', 'AlmaApiAuth',
           'ApiResponseErrorHandler']


class AlmaApiConfig(object):
    def __init__(self, base_url, apikey):
        self.base_url = self._parse_base_url(base_url)
        self.apikey = apikey

    def _parse_base_url(self, url):
        parsed = URL(url)

        if not parsed.is_absolute():
            raise ValueError('base_url was not absolute: {}'.format(url))

        return parsed

    @staticmethod
    def from_params(apikey=None, region=None, base_url=None):
        if not apikey:
            raise ValueError('no apikey provided')

        if region and base_url:
            raise ValueError('both region and base_url were specified')

        if region:
            canonical_region_name = const.region_aliases.get(region)

            if canonical_region_name is None:
                raise ValueError('Unknown region: {}'.format(region))

            base_url = const.default_regions[canonical_region_name]

        return AlmaApiConfig(base_url, apikey)


class AlmaApiAuth:
    '''
    Attaches parameters to a Request required to authenticate Alma REST API
    requests.
    '''
    header_name = 'Authorization'
    header_value_template = 'apikey {}'
    # https://tools.ietf.org/html/rfc7235#appendix-C (token68 rule)
    apikey_pattern = re.compile('^[a-zA-Z0-9-._~+/]+=*$')

    def __init__(self, apikey):
        self._validate_apikey(apikey)
        self.apikey = apikey

    def _validate_apikey(self, apikey):
        if not self.apikey_pattern.match(apikey):
            raise ValueError('API key doesn\'t match {!r} apikey: {!r}'
                             .format(self.apikey_pattern, apikey))

    def get_header_value(self):
        return self.header_value_template.format(self.apikey)

    def __call__(self, request):
        request.headers[self.header_name] = self.get_header_value()


class ApiResponseErrorHandler:
    codes_xpath = etree.XPath(
        '/xlib:web_service_result/xlib:errorList/xlib:error',
        namespaces={'xlib': const.EXLIB_NS})

    def _get_content_type(self, resp):
        type, _ = cgi.parse_header(resp.headers.get('content-type', ''))
        return type

    def _parse_error_response_xml(self, xml_bytes):
        # Parse bytes rather than str as lxml refuses to parse a str if the XML
        # contains an encoding declaration (which Alma responses do).
        root = etree.fromstring(xml_bytes)

        errors = self.codes_xpath.evaluate(root)

        if len(errors) == 0:
            return None, None, None
        elif len(errors) > 1:
            self._warn_multiple_errors()

        error = errors[0]
        error

        return (error.findtext(etree.QName(const.EXLIB_NS, 'errorCode')),
                error.findtext(etree.QName(const.EXLIB_NS, 'errorMessage')),
                error.findtext(etree.QName(const.EXLIB_NS, 'trackingId')))

    def _warn_multiple_errors(self):
        warnings.warn('API error response contained multiple error '
                      'codes, ignoring all but first', AlmaApiWarning)

    def _parse_error_response_json(self, json_text):
        try:
            root = json.loads(json_text)
            errors = root.get('errorList', {}).get('error', [])

            if len(errors) == 0:
                return None, None, None
            elif len(errors) > 1:
                self._warn_multiple_errors()

            error = errors[0]

            return (error.get('errorCode'),
                    error.get('errorMessage'),
                    error.get('trackingId'))
        except Exception as e:
            raise UnexpectedResponseAlmaApiError(
                'Unable to parse JSON error response') from e

    async def _build_exception(self, resp):
        content_type = self._get_content_type(resp)

        # Some API responses contain error XML but with type text/plain...
        if content_type in (const.CONTENT_TYPE_XML, const.CONTENT_TYPE_TXT):
            try:
                code, msg, track_id = self._parse_error_response_xml(
                    await resp.read())

                return ErrorResponseAlmaApiError.from_response(
                    resp, code, msg, track_id)
            except etree.LxmlError:
                # We only speculatively try to parse the plain text response as
                # XML, so if that fails we can't really complain...
                if content_type == const.CONTENT_TYPE_XML:
                    return UnexpectedResponseAlmaApiError.from_response(
                        resp, 'API error response contained invalid XML, '
                              'unable to parse error code')
                # ... ignore failure for plain text and use the generic error
        elif content_type == const.CONTENT_TYPE_JSON:
            code, msg, track_id = self._parse_error_response_json(
                await resp.text())
            return ErrorResponseAlmaApiError.from_response(
                resp, code, msg, track_id)

        return UnexpectedResponseAlmaApiError.from_response(
            resp, 'API response was not in a documented format')

    async def __call__(self, resp):
        try:
            resp.raise_for_status()
        except aiohttp.ClientResponseError as e:
            raise await self._build_exception(resp) from e


class Session:
    def __init__(self, api_config, http_session, rate_limiter=None,
                 product=None):

        if rate_limiter is None:
            rate_limiter = UnlimitedRateLimiter()

        self.api_config = api_config
        self.rate_limiter = rate_limiter
        self.http_session = http_session

    @classmethod
    def _create_user_agent(cls, product):
        ua = f'aiohttp/{aiohttp.__version__} Python/{sys.version_info.major}'

        return f'{product} {ua}' if product is not None else ua

    @classmethod
    def _create_http_session(cls, product=None, loop=None):
        return aiohttp.ClientSession(
            loop=loop, json_serialize=cls._json_dumps_compact,
            headers={'User-Agent': cls._create_user_agent(product)})

    @staticmethod
    def _json_dumps_compact(obj):
        return json.dumps(obj, indent=None, separators=(',', ':'))

    @classmethod
    def create(cls, apikey, region, loop=None, qps=const.DEFAULT_QPS,
               product=None):
        http_session = cls._create_http_session(product=product, loop=loop)

        api_config = AlmaApiConfig.from_params(apikey, region=region)

        rate_limiter = BurstingRateLimiter(qps, loop=loop)

        return Session(api_config=api_config, http_session=http_session,
                       rate_limiter=rate_limiter)

    def users(self):
        return UserApiClient(self)

    @property
    def authorisation(self):
        return AlmaApiAuth(self.api_config.apikey)


class BaseApiClient(object):
    _error_handler = ApiResponseErrorHandler()

    def __init__(self, session):
        self.session = session

    async def _request(self, handler, method, path, params=None, headers=None,
                       **kwargs):

        url = self.session.api_config.base_url.with_path(path)
        headers = self._get_headers(headers)

        try:
            async with self.session.rate_limiter.limit():
                async with self.session.http_session.request(
                        method, url,
                        params=params, headers=headers, **kwargs) as resp:

                    await self._error_handler(resp)
                    return await handler(resp)
        except aiohttp.ClientError as e:
            raise UnexpectedResponseAlmaApiError(
                url, method, None, 'Alma API HTTP request failed') from e

    def _get_headers(self, headers):
        sources = [self._get_auth_headers(),
                   self._get_accept_headers(),
                   headers or {}]
        return {k: v for headers in sources for k, v in headers.items()}

    def _get_auth_headers(self):
        return {
            'Authorization': self.session.authorisation.get_header_value()
        }

    def _get_accept_headers(self):
        return {
            'Accept': 'application/json'
        }

    async def _get_response_as_json(self, resp):
        try:
            return await resp.json()
        except json.JSONDecodeError as e:
            raise UnexpectedResponseAlmaApiError.from_response(
                resp, 'API response was not valid JSON') from e
        except aiohttp.ClientError as e:
            raise UnexpectedResponseAlmaApiError.from_response(
                resp, 'Unable to load JSON response') from e

    async def _expect_no_content(self, resp):
        if resp.status != http.HTTPStatus.NO_CONTENT:
            raise UnexpectedResponseAlmaApiError(
                'Expected a {} {} but got: {}'.format(
                    http.HTTPStatus.NO_CONTENT.name,
                    http.HTTPStatus.NO_CONTENT.phrase, resp.status))


def _uriencode(s):
    return urllib.parse.quote(s, safe='')


class UserApiClient(BaseApiClient):
    _users_resource_path = 'almaws/v1/users'
    _user_resource_path = 'almaws/v1/users/{userid}'
    _fees_resource_path = 'almaws/v1/users/{userid}/fees'
    _loans_resource_path = 'almaws/v1/users/{userid}/loans'

    limit_range = range(1, 100)

    async def get_user(self, id, id_type=None):

        path = self._user_resource_path.format(userid=_uriencode(id))

        params = {}
        if id_type is not None:
            params['user_id_type'] = id_type

        return await self._request(self._get_response_as_json,
                                   'GET', path, params=params)

    async def get_fees(self, userid, id_type=None):
        path = self._fees_resource_path.format(userid=_uriencode(userid))

        params = {}
        if id_type is not None:
            params['user_id_type'] = id_type

        return await self._request(self._get_response_as_json,
                                   'GET', path, params=params)

    async def get_loans(self, userid, id_type=None):
        path = self._loans_resource_path.format(userid=_uriencode(userid))

        params = {'limit': 100}
        if id_type is not None:
            params['user_id_type'] = id_type

        return await self._request(self._get_response_as_json,
                                   'GET', path, params=params)

    async def create_user(self, user):
        return await self._request(
            self._get_response_as_json, 'POST', self._users_resource_path,
            json=user)

    def _format_overrides(self, override):
        if not all(isinstance(o, const.UserOverride) for o in override):
            raise ValueError(
                f'override must be a sequence of UserOverride values')

        return ','.join(o.value for o in sorted(set(override),
                                                key=lambda o: o.value)) or None

    async def update_user(self, user, override=const.UserOverride.all,
                          previous_id=None, prev_id_type=None):
        """Update a user with the specified user representation.

        Args:
            user: An Alma JSON user object
            override: A sequence of UserOverride values. Default: all
            previous_id: The value of the identifier previously used for the
                         user. Needed when an ID value in the user object has
                         changed.
            prev_id_type: The ID type of the previous_id value. None
                              results in any ID type with the value matching.
        Returns:
            The new representation of the user stored on Alma.
        """
        if previous_id is None:
            if prev_id_type is not None:
                raise ValueError(
                    'prev_id_type cannot be set without previous_id')

            primary_id = user.get(~keys.primary_id)

            if primary_id is None:
                raise InvalidInputApiError(
                    f'user has no {~keys.primary_id} value')
            # There doesn't seem to be a way to say "only primary ID"
            # FIXME: However, we can verify that the returned user has the
            # given id as their primary ID.
            id_type = None
        else:
            primary_id = previous_id
            id_type = prev_id_type

        override = self._format_overrides(override or ())

        path = self._user_resource_path.format(userid=_uriencode(primary_id))
        params = {
            'override': override,
            'user_id_type': id_type
        }

        return await self._request(self._get_response_as_json, 'PUT', path,
                                   params=_strip_none(params), json=user)

    async def delete_user(self, id, id_type=None):
        path = self._user_resource_path.format(userid=_uriencode(id))

        params = {'user_id_type': id_type}

        return await self._request(self._expect_no_content, 'DELETE', path,
                                   params=_strip_none(params))


def _strip_none(mapping):
    return [(k, v) for k, v in mapping.items() if v is not None]


default_clients = {
    'users': UserApiClient
}
