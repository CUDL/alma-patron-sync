import attr
import enum
from yarl import URL

DEFAULT_QPS = 25


region_aliases = {
    'america': 'na',
    'na': 'na',
    'europe': 'eu',
    'eu': 'eu',
    'asia pacific': 'ap',
    'ap': 'ap',
    'canada': 'ca',
    'ca': 'ca',
    'china': 'cn',
    'cn': 'cn'
}

default_regions = {
    'na': URL('https://api-na.hosted.exlibrisgroup.com/'),
    'eu': URL('https://api-eu.hosted.exlibrisgroup.com/'),
    'ap': URL('https://api-ap.hosted.exlibrisgroup.com/'),
    'ca': URL('https://api-ca.hosted.exlibrisgroup.com/'),
    'cn': URL('https://api-cn.hosted.exlibrisgroup.com/')
}


class Status(enum.Enum):
    OK = 200
    NO_CONTENT = 200
    UNAUTHORIZED = 401
    BAD_REQUEST = 400
    TOO_MANY_REQUESTS = 429
    SERVER_ERROR = 500
    SERVICE_UNAVAILABLE = 503


# XML namespace used in API error response documents
EXLIB_NS = 'http://com/exlibris/urm/general/xmlbeans'

CONTENT_TYPE_XML = 'application/xml'
CONTENT_TYPE_JSON = 'application/json'
CONTENT_TYPE_TXT = 'text/plain'


error_codes = {}

# These were extracted from the Alma User API WADL definition
error_codes.update({
    '401129': 'No items can fulfill the submitted request.',
    '401136': 'Failed to save the request: Patron has active request for selected item.',
    '401153': 'Item cannot be loaned from this circulation desk.',
    '401604': 'Warning: The institutional inventory has services for the requested title.',
    '401607': 'Resource sharing library (owner) is missing',
    '401608': 'The given resource sharing library (owner) is not defined in the patron record',
    '401651': 'Item is not loanable.',
    '401652': 'General Error: An error has occurred while processing the request.',
    '401658': 'General Error - Failed to create new user',
    '401664': 'Mandatory field is missing: X',
    '40166411': 'Parameter value is invalid.',
    '40166412': 'Failed to perform operation.',
    '40166422': 'Value of parameter is invalid given other parameter.',
    '40166425': 'Shipping cost cannot be lower than 0.',
    '40166426': 'Parameter value is invalid (with error message).',
    '40166450': 'No result found for given parameters.',
    '401665': 'Fine not found.',
    '401666': 'X parameter is not valid.',
    '401672': 'PID field is empty.',
    '401676': 'No valid XML was given.',
    '401681': 'Due date cannot be in the past.',
    '401694': 'Request Identifier not found.',
    '401768': 'Patron is not affiliated with a resource sharing library',
    '401822': 'Cannot renew loan: Loan ID XXX.',
    '401823': 'Loan ID XXX does not exist.',
    '401824': 'Due date is not in loan object.',
    '401850': 'Failed to delete user with identifier X of type Y.',
    '401851': 'User with identifier  of type Y already exists.',
    '401852': 'Given user group is not legal.',
    '401853': 'External Id must be empty for internal user.',
    '401854': 'External Id must be given for external user.',
    '401855': "The account type 'Internal with external authentication' is currently not supported.",
    '401857': 'The given user account type is illegal (must be INTERNAL/EXTERNAL).',
    '401858': 'The external id in DB does not fit the given value in xml - external id cannot be updated.',
    '401859': 'Action currently not supported.',
    '401860': 'Failed to update user.',
    '401861': 'User with identifier X was not found.',
    '401863': 'Given X type (Y) is not supported for given user record type (Z).',
    '401864': 'Given X type (Y) is invalid.',
    '401866': 'User authentication failed',
    '401890': 'User with identifier X of type Y was not found.',
    '401907': 'Failed to find a request for the given request ID.',
    '401932': 'Request X is not a Digitization request',
    '401933': 'Cannot move forward in workflow. Request ID: X, Step: Y',
    '401934': 'Move digitization request to next step in workflow has failed. Request ID: X',
    '4019990': 'User with source identifier X of institution Y was not found.',
    '4019991': 'User fine/fee type is required.',
    '4019992': 'User fine/fee type invalid entry.',
    '4019993': 'User item barcode invalid entry.',
    '4019994': 'User fine/fee original amount is required.',
    '4019996': 'User fine/fee owner is required.',
    '4019997': 'User fine/fee owner must be a library and not institution.',
    '4019998': 'User with linking ID not found.',
    '402039': 'Could not create request, default item location is not defined for the resource sharing library',
    '402119': 'General error.',
    '402205': 'Input parameter X (Y) is not numeric.',
    '402238': 'Failed to get deposit.',
    '402362': 'Failed to save the request: Patron has duplicate request',
    '60101': 'General Error: An error has occurred while processing the request.'
})

error_codes.update({
    'DAILY_THRESHOLD':
        'Daily API Request Threshold has been reached. For details see: '
        'https://developers.exlibrisgroup.com/alma/apis#threshold',
    'PER_SECOND_THRESHOLD':
        'HTTP requests are more than allowed per second. '
        'See https://goo.gl/2x1c1M ',  # space is in original
    'GENERAL_ERROR':
        'A Gateway error has occurred - Make sure you add an appropriate '
        'apikey as a parameter or a header.'
})


class ErrorCodes(enum.Enum):
    USER_ID_DOESNT_EXIST_WITH_TYPE = '401890'
    USER_ID_DOESNT_EXIST = '401861'
    ID_TYPE_DOESNT_EXIST = '401862'
    MISSING_REQUIRED_FIELD = '401664'
    MISSING_EXTERNAL_USER_FIELD = '401854'
    USER_ID_ALREADY_EXISTS = '401851'
    INTERNAL_SERVER_ERROR = 'INTERNAL_SERVER_ERROR'
    ID_DUPLICATED_IN_REQUEST = '4018994'
    ROUTING_ERROR = 'ROUTING_ERROR'
    ERR_THRESHOLD_DAILY = 'DAILY_THRESHOLD'
    ERR_THRESHOLD_QPS = 'PER_SECOND_THRESHOLD'


@attr.s(slots=True, frozen=True)
class ErrorCode:
    code = attr.ib()
    msg = attr.ib(cmp=False)

    @msg.default
    def lookup_msg(self):
        if self.code in error_codes:
            return error_codes[self.code]
        raise ValueError(f'Code {self.code!r} is not known, msg must be specified')


ERR_THRESHOLD_DAILY = ErrorCode('DAILY_THRESHOLD')
ERR_THRESHOLD_QPS = ErrorCode('PER_SECOND_THRESHOLD')


class UserOverride(enum.Enum):
    CAMPUS_CODE = 'campus_code'
    JOB_CATEGORY = 'job_category'
    LIBRARY_NOTICES = 'library_notices'
    PIN_NUMBER = 'pin_number'
    PREFERRED_LANGUAGE = 'preferred_language'
    RS_LIBRARIES = 'rs_libraries'
    USER_GROUP = 'user_group'
    USER_TITLE = 'user_title'


UserOverride.all = frozenset(UserOverride)
