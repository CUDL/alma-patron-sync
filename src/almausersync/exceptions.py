class AlmaUserSyncWarning(UserWarning):
    pass


class InvalidInputError(ValueError):
    """Raised when user-specified incoming data is invalid."""
    pass


class InvalidUserWrapper(InvalidInputError):
    """Raised when a user wrapper object is invalid."""
    pass


class InvalidUserError(InvalidInputError):
    """Raised when a user object is invalid."""
    pass


class ApiOperationFailedError(RuntimeError):
    pass


class UserUpdateFailedError(ApiOperationFailedError):
    pass


class IdMigrationFailedError(UserUpdateFailedError):
    pass


class TooManyAttemptsError(RuntimeError):
    """Raised when an operation has been aborted due to being (re)attempted too
     many times."""
    pass
