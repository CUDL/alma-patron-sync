from asyncio import Queue
from functools import wraps
from collections.abc import Collection, AsyncIterable

from almaload.coroutines import ApplyError, PartitionError
from almaload.exceptions import attach_cause


def async_prewarmed(async_generator):
    """
    Wrap an async generator such that its asend() method is immediately called
    with None so that clients can asend() freely without worrying about whether
    it's been done already.
    """
    # Could remove the need to await on creating the generator by hooking
    # asend() to send None on the first call prior to sending the real value.
    # The trouble with that is that you can't replace the asend method, so
    # you'd need to wrap the whole generator object, intercepting every asend,
    # not just the first.
    @wraps(async_generator)
    async def async_prewarmer(*args, **kwargs):
        gen = async_generator(*args, **kwargs)
        await gen.asend(None)
        return gen
    return async_prewarmer


async def pump(src, target):
    if isinstance(src, AsyncIterable):
        async for thing in src:
            await target.asend(thing)
        await target.aclose()
    elif isinstance(src, Collection):
        for thing in src:
            await target.asend(thing)
        await target.aclose()
    else:
        raise ValueError(f'Can\'t pump a {type(src)}; value: {src!r}')


@async_prewarmed
async def apply(func, target, errors=None):
    while True:
        value = yield
        try:
            result = func(value)
        except Exception as e:
            if errors is None:
                raise ApplyError(value, func) from e
            await errors.asend(attach_cause(ApplyError(value, func), e))
            continue
        await target.asend(result)


@async_prewarmed
async def into_queue(queue: Queue):
    while True:
        try:
            value = yield
        except GeneratorExit:
            await queue.put(None)
            raise
        else:
            await queue.put(value)


async def from_queue(queue: Queue, target):
    while True:
        value = await queue.get()
        if value is None:
            await target.aclose()
            return
        else:
            await target.asend(value)


@async_prewarmed
async def partition(key, targets, errors=None):
    while True:
        value = yield
        try:
            key_val = key(value)
        except Exception as e:
            if errors is None:
                raise PartitionError(value, key) from e
            await errors.athrow(attach_cause(PartitionError(value, key), e))
            continue
        try:
            target = targets[key_val]
        except KeyError:
            raise ValueError(f'No target to dispatch to for key value: '
                             f'{key!r}; targets: {set(targets.keys())}')
        await target.asend(value)
