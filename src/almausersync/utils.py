import uuid
from collections import Counter
from datetime import datetime

from almaload.almausers import keys, wrapper_keys
from almausersync.exceptions import InvalidUserWrapper, InvalidUserError

_sentinel = object()


def get(mapping, key, default=_sentinel, type=None):
    value = mapping.get(key, _sentinel)

    if value is _sentinel:
        return None if default is _sentinel else default

    if type is not None and not isinstance(value, type):
        return None if default is _sentinel else default

    return value


def create_id():
    return str(uuid.uuid4())


def iso_timestamp_now_utc():
    return datetime.utcnow().isoformat() + 'Z'


def get_user(user_or_wrapper):
    if ~wrapper_keys.alma_user in user_or_wrapper:
        return user_or_wrapper[~wrapper_keys.alma_user]
    return user_or_wrapper


def get_original_ids(user_wrapper):
    ids = user_wrapper.get(~wrapper_keys.original_ids, [])

    return [
        (id[~wrapper_keys.original_ids.type],
         id[~wrapper_keys.original_ids.value],
         True if id[~wrapper_keys.original_ids.type] == 'primary' else
            id[~wrapper_keys.original_ids.is_active])
        for id in ids
    ]


def filter_ids(ids, active=True):
    active = bool(active)
    return [(t, v) for (t, v, a) in ids if a is active]


def get_new_ids(user_or_wrapper):
    user = get_user(user_or_wrapper)

    ids = []
    primary_id_val = user.get('primary_id')
    if primary_id_val:
        ids.append(('primary', primary_id_val, True))

    uid = keys.user_identifier
    for obj in get(user, ~uid, [], list):
        type = get(get(obj, ~uid.id_type, {}, dict),
                   ~uid.id_type.value, type=str)
        value = get(obj, ~uid.value, type=str)
        is_active = get(obj, ~uid.status, type=str) == 'ACTIVE'

        ids.append((type, value, is_active))

    return ids


class IdSelectionStrategy:

    @staticmethod
    def _id_priority(id):
        id_type, id_val = id
        return (0, id_val) if id_type == 'primary' else (1, id_val)

    def _get_candidate_original_ids(self, user_wrapper):
        return ((t, v) for t, v, a in get_original_ids(user_wrapper)
                if a is True)

    def get_id_for_delete(self, user_wrapper):
        original_ids = sorted(self._get_candidate_original_ids(user_wrapper),
                              key=self._id_priority)

        if len(original_ids) == 0:
            raise InvalidUserWrapper(
                'user_wrapper has no active {~wrapper_keys.original_ids!r}')

        return original_ids[0]

    def get_id_for_update(self, user_wrapper):
        """Get the best ID to select a user with when performing an API update.
        """
        # - any original ID will work
        # - can't use a current ID if it's new
        # Simplest thing is to just use one of the specified original IDs. As
        # long as they're all populated then we'll use the primary ID if there
        # was one.

        # PUTing a user with no IDs can be done with the API - they'll be given
        # a newly-generated primary ID. However it's not safe as if we create
        # the user and then fail before we can log the new ID we'll have no way
        # to recover the user. This is another case for the ID migration to
        # handle.
        if not any(a for t, v, a in get_new_ids(user_wrapper)):
            raise InvalidUserError('New user state has no IDs. '
                                   'This is unsafe, refusing to continue.')

        original_ids = sorted(self._get_candidate_original_ids(user_wrapper),
                              key=self._id_priority)

        if len(original_ids) == 0:
            raise InvalidUserWrapper(
                'user_wrapper has no active {~wrapper_keys.original_ids!r}')

        return original_ids[0]

    def get_ids_for_update_clean_up(self, user_wrapper):
        """
        Get IDs that can be used to recover a user's state after a failed
        request or after replaying from the WAL.

        Either one or two IDs are returned. If any ID values are present on
        the user before and after the update then one ID (and one request) can
        be used to discover the present user state. Otherwise if no IDs are
        common then two IDs will be returned and up to two requests are
        required.

        If two values are returned, the first is from the original IDs and the
        second from the new IDs.

        Args:
            user_wrapper: The user being updated

        Returns: A list of one or two (type, value) pairs.

        """
        active_new_ids = {v: t
                          for (t, v, a) in get_new_ids(user_wrapper) if a}
        active_old_ids = {v: t
                          for (t, v, a) in get_original_ids(user_wrapper) if a}

        if not active_new_ids:
            raise ValueError('New user state has no active IDs')
        if not active_old_ids:
            raise ValueError('Old user state has no active IDs')

        common_ids = active_new_ids.keys() & active_old_ids.keys()

        if common_ids:
            # it's desirable to use an ID with the same type on both
            commonly_typed = {(active_new_ids[v], v) for v in common_ids
                              if active_new_ids[v] == active_old_ids[v]}

            if commonly_typed:
                return [sorted(commonly_typed, key=self._id_priority)[0]]

            # No common types, but we can select with one ID by omitting the
            # type. Just use the first alphabetically to ensure determinism.
            return [(None, sorted(common_ids)[0])]

        # No IDs match, so we'll have to do potentially two queries to resolve
        # the user.
        return [
            sorted(((t, v) for (v, t) in active_old_ids.items()),
                   key=self._id_priority)[0],
            sorted(((t, v) for (v, t) in active_new_ids.items()),
                   key=self._id_priority)[0],
        ]


# FIXME: Pull these dynamically from the code table API
HARDCODED_ID_TYPES = set('00 02 03 04 05 06 BARCODE CRSID FACEBOOK GOOGLE '
                         'INST_ID LTI_SELF_REGISTERED TWITTER'.split())
MAX_ID_LEN = 255
MAX_NOTE_LEN = 2000


def _validate_primary_id(user):
    primary_id = user.get(~keys.primary_id)

    try:
        _validate_id_val(primary_id)
    except ValueError as e:
        raise InvalidUserError(
            f'User\'s {~keys.primary_id} is invalid: {e}') from None


def _validate_id_val(id):
    _validate_is_string(id)

    if not id:
        raise ValueError(f'id has no value: {id!r}')

    if len(id) > MAX_ID_LEN:
        raise ValueError(f'id was longer than the limit of {MAX_ID_LEN} '
                         f'characters: {len(id)}')


def validate_ids(user_or_wrapper):
    user = get_user(user_or_wrapper)

    _validate_primary_id(user)
    _validate_additional_ids(user)

    id_counts = Counter(v for _, v, a in get_new_ids(user))
    dupes = [k for k, count in id_counts.items() if count > 1]

    if dupes:
        raise InvalidUserError(f'User has multiple occurrences of IDs: {dupes}')

    # primary_id is case insensitive - users can't have additional IDs which
    # match the primary ID case insensitively
    lower_additional = {v.lower() for t, v, a in get_new_ids(user)
                        if t != 'primary'}
    lower_primary = user['primary_id'].lower()
    if lower_primary in lower_additional:
        raise InvalidUserError(
            f'User has case-insensitive collision between primary and '
            f'additional ID values: {lower_primary!r}')


def _validate_additional_ids(user):
    ids = get(user, ~keys.user_identifier)

    if ids is None:
        return

    if not isinstance(ids, list):
        raise InvalidUserError(
            f'Invalid {~keys.user_identifier}: value is not a list')

    try:
        for i, user_identifier in enumerate(ids):
            _validate_additional_id(user_identifier)
    except ValueError as e:
        raise InvalidUserError(
            f'Invalid additional identifier at index {i}') from e


ID_OBJ_KEYS = {~keys.user_identifier.segment_type,
               ~keys.user_identifier.value,
               ~keys.user_identifier.note,
               ~keys.user_identifier.status,
               ~keys.user_identifier.id_type}


ID_TYPE_OBJ_KEYS = {~keys.user_identifier.id_type.desc,
                    ~keys.user_identifier.id_type.value}


def _validate_additional_id(id_obj):
    if not isinstance(id_obj, dict):
        raise ValueError(f'{type(id_obj)} id not an object')

    if id_obj.keys() - ID_OBJ_KEYS:
        raise ValueError(f'object contains unexpected keys: '
                         f'{id_obj.keys() - ID_OBJ_KEYS!r}')

    value = get(id_obj, ~keys.user_identifier.value)
    try:
        _validate_id_val(value)
    except ValueError as e:
        raise ValueError(f'Invalid value: {e}')

    note = get(id_obj, ~keys.user_identifier.note)
    if note is not None:
        try:
            _validate_is_string(note)
            if note is not None and len(note) > 2000:
                raise ValueError(f'Value longer than 2000: {len(note)}')
        except ValueError as e:
            raise ValueError(f'Invalid note: {e}') from None

    status = id_obj.get(~keys.user_identifier.status)
    if status is not None:
        if status not in ['ACTIVE', 'INACTIVE']:
            raise ValueError(f'Unexpected status: {status!r}')

    segment_type = id_obj.get(~keys.user_identifier.segment_type)
    if segment_type is not None:
        if segment_type not in ['External', 'Internal']:
            raise ValueError(
                f'Unexpected {~keys.user_identifier.segment_type}: '
                f'{segment_type!r}')

    type = id_obj.get(~keys.user_identifier.id_type)
    if type is None:
        raise ValueError(f'{~keys.user_identifier.id_type} is not an object')

    if type.keys() - ID_TYPE_OBJ_KEYS:
        raise ValueError(f'{~keys.user_identifier.id_type} contains '
                         f'unexpected keys: {type.keys() - ID_TYPE_OBJ_KEYS}')

    try:
        desc = type.get(~keys.user_identifier.id_type.desc)
        if desc is not None:
            _validate_is_string(desc)
            if len(desc) > 4000:
                raise ValueError(f'desc was longer than 4000: {len(desc)}')

        value = type.get(~keys.user_identifier.id_type.value)
        _validate_is_string(value)
        # FIXME: should be a dynamic list, obtained from and up-to-date with
        # the API.
        if value not in HARDCODED_ID_TYPES:
            raise ValueError(f'Unexpected type code: {value!r}')
    except ValueError as e:
        raise ValueError(
            f'Invalid {~keys.user_identifier.id_type}: {e}') from None


def _validate_is_string(value):
    if not isinstance(value, str):
        raise ValueError(f'{type(value).__name__} is not str: {value!r}')
