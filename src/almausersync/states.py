import functools


class IllegalStateError(RuntimeError):
    pass


def requirestate(*states):
    def decorate(method_descriptor):
        @functools.wraps(method_descriptor)
        def checkstate(self, *args, **kwargs):
            if self.state not in states:
                raise IllegalStateError(f'cannot call {method_descriptor!r} '
                                        f'from state: {self.state}')
            return method_descriptor(self, *args, **kwargs)
        return checkstate
    return decorate
