import asyncio
import signal
import traceback
from functools import partial, lru_cache

import aiofiles
import attr
import math
import os
import sys
import tqdm
from docopt import docopt

from almaload.cmdline import CmdlineError, parse_input_count
from almaload.exceptions import attach_cause
from almausersync._version import __version__
from almausersync.almarest import AlmaApiConfig, Session, UserApiClient, \
    AlmaApiAuth
from almausersync.almarest.constants import region_aliases
from almausersync.async_coroutines import async_prewarmed, pump, apply, \
    into_queue, from_queue, partition
from almausersync.exceptions import ApiOperationFailedError, \
    TooManyAttemptsError
from almausersync.jobfactory import AlmaSyncJobFactory
from almausersync.jobs import DefaultRetryStrategy
from almausersync.jsonstreams import dump_incremental_async, load_async
from almausersync.syncprocessor import (
    AsyncWalProcessorState, DEFAULT_ACTIVE_JOB_LIMIT, AsyncWalProcessorDriver,
    Job)
from almausersync.wal import JsonStreamWalWriter

DEBUG = bool(os.environ.get('ALMAUSERSYNC_DEBUG'))


__doc__ = f"""
Synchronise a list of Alma user data with an Alma instance using the REST API.

Usage: sync-users [options] <write-ahead-log> <input> <output>

Arguments:
    <write-ahead-log>
        A path to a not-yet-existing file which will be used as a write-ahead
        log during the sync process. This is required to safely perform
        non-atomic data manipulations with the Alma API without risking data
        loss if the program is interrupted.

    <input>
        A JSON stream of Alma wrapper user objects

    <output>
        A file path, to which a JSON stream of sync job output will be written.
        Unless --failed-sync-jobs is also provided this file will contain both
        succesful and unsuccessful sync jobs.

Options:
    -f=<file>,--failed-sync-jobs=<file>
        Write a JSON stream containing sync jobs which didn't successfully
        complete. The errors are related to the Alma API - anything from
        network issues, users with invalid data to misuse of the API.

    -c=<file>,--crashed-jobs=<file>
        Write a JSON stream containing jobs/input data which triggered errors
        while being processed. These are either caused by bad input data or
        bugs in the tool itself.

    --input-count=<n>
        The expected number of records in the input stream, for the purpose of
        displaying progress. Can be used to display progress when input is not
        a seekable file, or to avoid scanning input file to determine the
        number of records.

    --api-region=<region>
        The API region which defines the base URL to communicate with.
        Values: {', '.join(region_aliases.keys())}; Default: europe

    --api-qps=<n>
        The number of queries per-second to rate-limit API usage to. The normal
        value is 25. Anecdotally I've been able to sustain > 100 without being
        rate limited. Default: 25

    --api-retry-limit=<n>
        The max number of queries to expend on a given user when requests fail
        in a way which implies a retry might help. E.g. rate limiting
        responses, network errors, etc. Default: 10

    --max-concurrent-jobs=<n>
        The maximum number of concurrent jobs (users) to process at once.
        Default: 100

    --job-processor-io-queue-size=<n>
        Size limit for the job processor's input and output queues.
        Default: 100:100

    --quiet
        Don't render progress bars.

    --traceback
        Write a stack trace on fatal errors.

    -h,--help
        Show this info.

    -v,--version
        Print the version and exit.

Alma API Key:
Specify your API key using the envar: ALMA_API_KEY

Input Data:
The user records are Alma user JSON, except wrapped in an additional JSON
object to hold metadata on the user record. The actual user record is stored
on the 'alma_user' key. For example:

    {{
        'alma_user': {{
            'foo': 'abc'
        }}
    }}

JSON Streams:
    This program reads and writes JSON streams of Alma users. These JSON
    streams are simply serialised JSON objects without any whitespace,
    separated by newlines. (JSON strings can't contain literal newlines, so
    excluding whitespace from the serialisation guarantees documents won't
    contain newlines.)
"""

PRODUCT = f'cam-alma-user-sync/{__version__}'


class ProgressReporter:
    """
    Maintain a set of 4 progress bars to visualise execution progress. One
    shows overall progress of all records. The other three show the number of
    records that have been directed to the 3 output streams: included
    (successful), excluded and failed.
    """

    # A bit of hacking is required to get tqdm to reliably update the total for
    # each bar. It doesn't seem to expect the total to change dynamically, so
    # changing the total when not changing n won't trigger an update. You could
    # just call refresh() on each change, but that'd be slow.
    #
    # To work around, we re-draw all 4 bars when any one bar is redrawn. tqdm
    # doesn't provide a way to hook into its decisions on when to draw, so we
    # hook its render method to detect draw calls and keep track of which bars
    # have been drawn, then render the others if any were drawn after each
    # update.

    def __init__(self):
        opts = dict(unit=' records', unit_scale=True)
        self.overall_progress = tqdm.tqdm(desc='   Overall: ', **opts)
        self.included_progress = tqdm.tqdm(desc='Successful: ', **opts)
        self.excluded_progress = tqdm.tqdm(desc='   Crashed: ', **opts)
        self.failed_progress = tqdm.tqdm(desc='    Failed: ', **opts)

        self._tqs = [self.overall_progress, self.included_progress,
                     self.excluded_progress, self.failed_progress]

        for i, tq in enumerate(self._tqs):
            self._hook_render_calls(i, tq)

        self._renders_performed = [False] * len(self._tqs)

    def close(self):
        for tq in self._tqs:
            tq.close()

    def _hook_render_calls(self, index, tq):
        """
        Hook the sp() method of the progress bar (which renders the bar) in
        order to detect when a progress bar is rendered.
        """
        orig_sp = tq.sp

        def sp(s):
            self._renders_performed[index] = True
            return orig_sp(s)

        tq.sp = sp

    def maybe_refresh(self):
        """
        refresh() any of our progress bars which weren't rendered if any of
        them were.
        """
        if any(self._renders_performed):
            for i, tq in enumerate(self._tqs):
                if not self._renders_performed[i]:
                    tq.refresh()
                self._renders_performed[i] = False

    def update_progress(self, progress):
        total = progress.total
        seen = progress.total_seen

        self.overall_progress.n = seen
        self.included_progress.n = progress.included_count
        self.excluded_progress.n = progress.excluded_count
        self.failed_progress.n = progress.failed_count

        self.overall_progress.total = total
        self.included_progress.total = seen
        self.excluded_progress.total = seen
        self.failed_progress.total = seen

        # update() calls may trigger a render of the bar
        self.overall_progress.update(0)
        self.overall_progress.update(0)
        self.included_progress.update(0)
        self.excluded_progress.update(0)
        self.failed_progress.update(0)

        # Ensure all bars are rendered if any one bar is - see comments at top
        # of class.
        self.maybe_refresh()


class ProgressState:
    def __init__(self, total):
        self._total = total

        self._included_count = 0
        self._excluded_count = 0
        self._failed_count = 0

        self.listeners = []

    def add_listener(self, listener):
        self.listeners.append(listener)

    def notify_listeners(self):
        for listener in self.listeners:
            listener(self)

    @property
    def total(self):
        if self._total is None:
            return None
        return max(self._total, self.total_seen)

    @total.setter
    def total(self, value):
        self._total = value
        self.notify_listeners()

    @property
    def total_seen(self):
        return self.included_count + self.excluded_count + self.failed_count

    @property
    def included_count(self):
        return self._included_count

    @included_count.setter
    def included_count(self, value):
        self._included_count = value
        self.notify_listeners()

    @property
    def excluded_count(self):
        return self._excluded_count

    @excluded_count.setter
    def excluded_count(self, value):
        self._excluded_count = value
        self.notify_listeners()

    @property
    def failed_count(self):
        return self._failed_count

    @failed_count.setter
    def failed_count(self, value):
        self._failed_count = value
        self.notify_listeners()


@async_prewarmed
async def increment_observer(obj, name, target):
    """
    A coroutine which increments a named property on obj when it receives a
    value. Values are sent on to target.
    """
    while True:
        val = yield

        setattr(obj, name, getattr(obj, name) + 1)

        await target.asend(val)


async def estimate_record_count(file):
    count = 0
    async for _ in file:  # noqa: F841
        count += 1
    await file.seek(0)
    return count


async def open_input_file(path):
    if path == '-':
        path = sys.stdin.buffer.fileno()
    try:
        return await aiofiles.open(path, 'r')
    except OSError as e:
        raise CmdlineError(f'Unable to open file for reading: {path!r}') from e


async def open_output_file(path):
    if path == '-':
        path = sys.stdout.buffer.fileno()
    try:
        return await aiofiles.open(path, 'w')
    except OSError as e:
        raise CmdlineError(f'Unable to open file for writing: {path!r}') from e


async def get_input(path, count_estimate=None):
    file = await open_input_file(path)

    if count_estimate is None and await file.seekable():
        count_estimate = await estimate_record_count(file)

    return load_async(file), count_estimate


async def get_json_output_target(dest):
    if dest is None:
        return None

    out_file = await open_output_file(dest)

    return await dump_incremental_async(out_file)


def cachedproperty(f):
    @property
    @lru_cache(1)
    def wrapper(self):
        return f(self)
    return wrapper


@attr.s(hash=False)
class CmdlineInterface:
    args = attr.ib()
    loop = attr.ib()
    _input_file = attr.ib(init=False, default=None)
    _input_line_count = attr.ib(init=False, default=None)
    _output_streams = attr.ib(init=False, default=attr.Factory(dict))

    @cachedproperty
    def is_progress_enabled(self):
        return (not self.args['--quiet']) and not any(
            output == '-' for output in [self.args['<output>'],
                                         self.args['--failed-sync-jobs'],
                                         self.args['--crashed-jobs']])

    @cachedproperty
    def progress_reporter(self):
        if self.is_progress_enabled:
            return ProgressReporter()
        return None

    @cachedproperty
    async def progress_state(self):
        ps = ProgressState(await self.input_count)
        ps.add_listener(self.progress_reporter.update_progress)
        return ps

    @cachedproperty
    def input_queue(self):
        return asyncio.Queue(self.io_queue_size[0])

    @cachedproperty
    def output_queue(self):
        return asyncio.Queue(self.io_queue_size[1])

    @cachedproperty
    def api_key(self):
        key = os.environ.get('ALMA_API_KEY')

        if key is None:
            raise CmdlineError('The Alma API key must be specified using the'
                               f' environment variable: ALMA_API_KEY')

        try:
            AlmaApiAuth(key)
        except ValueError as e:
            raise CmdlineError(f'Invalid ALMA_API_KEY envar: {key!r}') from e
        return key

    @cachedproperty
    def api_region(self):
        region = self.args['--api-region']
        if region is None:
            region = 'europe'
        canonical = region_aliases.get(region)
        if canonical is None:
            raise CmdlineError('Unrecognised Alma API region: {region}')
        return canonical

    @cachedproperty
    def api_config(self):
        return AlmaApiConfig.from_params(apikey=self.api_key)

    @cachedproperty
    def io_queue_size(self):
        raw = self.args['--job-processor-io-queue-size']
        if raw is None:
            raw = '100'

        try:
            size = tuple(int(x) for x in raw.split(':', 1))
            if len(size) == 1:
                size += size

            if any(s < 1 for s in size):
                raise ValueError('Queue size must be >= 1')
        except Exception as e:
            raise CmdlineError(f'Invalid --job-processor-io-queue-size: {raw}') from e

        return size

    @cachedproperty
    def api_queries_per_second(self):
        raw = self.args['--api-qps']
        if raw is None:
            raw = 25
        try:
            val = float(raw)
            if val <= 0 or not math.isfinite(val):
                raise ValueError('must be a finite number > 0')
            return val
        except Exception as e:
            raise CmdlineError('Invalid --api-qps') from e

    @cachedproperty
    def api_per_user_retry_limit(self):
        raw = self.args['--api-retry-limit']
        if raw is None:
            raw = 10
        try:
            val = int(raw)
            if val < 1:
                raise ValueError(f'must be >= 1')
            return val
        except Exception as e:
            raise CmdlineError('Invalid --api-retry-limit')

    @cachedproperty
    def api_session(self):
        return Session.create(self.api_key, self.api_region, self.loop,
                              self.api_queries_per_second, product=PRODUCT)

    @cachedproperty
    def user_api_client(self):
        return UserApiClient(self.api_session)

    @cachedproperty
    def retry_strategy(self):
        return DefaultRetryStrategy(max_attempts=self.api_per_user_retry_limit)

    @cachedproperty
    def job_factory(self):
        return AlmaSyncJobFactory(user_api=self.user_api_client,
                                  retry_strategy=self.retry_strategy)

    @cachedproperty
    def async_wal_proc_state(self):
        return AsyncWalProcessorState(
            input_queue=self.input_queue, output_queue=self.output_queue,
            job_factory=self.job_factory,
            active_job_limit=self.active_job_limit)

    @cachedproperty
    async def async_wal_proc_driver(self):
        return AsyncWalProcessorDriver(state=self.async_wal_proc_state,
                                       wal_writer=await self.wal_writer)

    @cachedproperty
    def active_job_limit(self):
        raw = self.args['--max-concurrent-jobs']
        if raw is None:
            raw = DEFAULT_ACTIVE_JOB_LIMIT
        try:
            val = int(raw)
            if val < 1:
                raise ValueError('concurrent jobs must be >= 1')
            return val
        except Exception as e:
            raise CmdlineError('Invalid --max-concurrent-jobs') from e

    @cachedproperty
    def wal_path(self):
        return self.args['<write-ahead-log>']

    @cachedproperty
    def in_path(self):
        return self.args['<input>']

    @cachedproperty
    def out_path(self):
        return self.args['<output>']

    @cachedproperty
    def failed_jobs_path(self):
        path = self.args['--failed-sync-jobs']
        return path if path is not None else self.out_path

    @cachedproperty
    def crashed_jobs_path(self):
        path = self.args['--crashed-jobs']
        return path if path is not None else self.out_path

    @cachedproperty
    async def failed_jobs_stream(self):
        return await self._get_or_create_out_stream(self.failed_jobs_path)

    @cachedproperty
    async def crashed_jobs_stream(self):
        return await self._get_or_create_out_stream(self.crashed_jobs_path)

    @cachedproperty
    async def output_jobs_stream(self):
        return await self._get_or_create_out_stream(self.out_path)

    async def _get_or_create_out_stream(self, path):
        if path not in self._output_streams:
            self._output_streams[path] = await get_json_output_target(path)

        return self._output_streams[path]

    @cachedproperty
    async def in_stream(self):
        if self._input_file is None:
            await self._load_input()

        return self._input_file

    @cachedproperty
    def input_count_estimate(self):
        return parse_input_count(self.args['--input-count'])

    @cachedproperty
    async def input_count(self):
        if self._input_file is None:
            await self._load_input()

        return self._input_line_count

    async def _load_input(self):
        self._input_file, self._input_line_count = (
            await get_input(self.in_path,
                            count_estimate=self.input_count_estimate))

    @cachedproperty
    async def wal_writer(self):
        f = await aiofiles.open(self.wal_path, 'x')
        return JsonStreamWalWriter.create(f, loop=self.loop)

    def __hash__(self):
        return object.__hash__(self)


async def produce_input(src_json, dest_queue):
    async for record in src_json:
        await dest_queue.put(record)


def tag_object(name, obj):
    return (name, obj)


@async_prewarmed
async def input_breaker(target, loop=None, shutdown_event=None):
    if loop is None:
        loop = asyncio.get_event_loop()

    if shutdown_event is None:
        shutdown_event = asyncio.Event(loop=loop)

    loop.add_signal_handler(signal.SIGINT, shutdown_event.set)
    count = 0
    try:
        while True:
            val = yield
            await target.asend(val)
            count += 1

            if shutdown_event.is_set():
                print('\n' * 10)
                print('Got SIGINT, shutting down gracefully. No '
                      'further input will enqueued, but already received data '
                      f'will be processed. {count} records have been sent.')
                break
    finally:
        loop.remove_signal_handler(signal.SIGINT)


async def get_input_stage(cmdline, shutdown_event=None):
    return pump(
        await cmdline.in_stream,
        await apply(partial(tag_object, 'alma-user-wrapper'),
                    await into_queue(cmdline.input_queue)))


def is_failed_job(job: Job):
    return job.result.exception() is not None


def classify_output_job(job: Job):
    assert not job.result.cancelled()
    assert job.result.done()

    exc = job.result.exception()

    if exc is None:
        return 'successful-sync'

    if isinstance(exc, (ApiOperationFailedError, TooManyAttemptsError)):
        return 'failed-sync'
    return 'crashed-job'


def debug_log(job: Job):
    import logging
    log = logging.getLogger(__name__)

    if job.result.done() and job.result.exception():
        log.debug(job.result.result())

    return job


async def get_output_stage(cmdline):

    ojs = await cmdline.output_jobs_stream
    cjs = await cmdline.crashed_jobs_stream
    fjs = await cmdline.failed_jobs_stream

    if cmdline.is_progress_enabled:
        progress_state = await cmdline.progress_state
        ojs = await increment_observer(progress_state,
                                       'included_count', ojs)
        cjs = await increment_observer(progress_state,
                                       'excluded_count', cjs)
        fjs = await increment_observer(progress_state,
                                       'failed_count', fjs)

    tail = await partition(classify_output_job, {
        'successful-sync': await apply(create_successful_sync_output_json,
                                       ojs),
        'crashed-job': await apply(create_job_exception_json,
                                   cjs),
        'failed-sync': await apply(create_failed_sync_json,
                                   fjs)})

    if DEBUG:
        tail = await apply(debug_log, tail)

    return from_queue(cmdline.output_queue, tail)


def create_failed_sync_json(job: Job):
    # No need to handle exceptions here as our error handler will handle any
    exc = job.result.exception()

    return {
        'status': 'sync-unsuccessful',
        'job': job_info_json(job),
        'exception': stringify_exception(exc)
    }


def create_successful_sync_output_json(job: Job):
    return {
        'status': 'sync-successful',
        'job': job_info_json(job),
        'result': job.result.result()
    }


def job_info_json(job: Job):
    return {
        'id': job.id,
        'type': job.type,
        'input_data': job.input_data
    }


def create_job_exception_json(job: Job):
    try:
        exc = job.result.exception()
    except BaseException as e:
        exc = RuntimeError('Received job with no exception')
        attach_cause(exc, e)

    return {
        'status': 'failed-job',
        'job': job_info_json(job),
        'exception': stringify_exception(exc)
    }


def stringify_exception(exc):
    """
    Render an exception (and it's linked exceptions) into a string containing
    the traceback and type/message.
    """
    return ''.join(traceback.format_exception(None, exc, exc.__traceback__))


async def main(cmdline):
    shutdown_event = asyncio.Event()

    input_stage = await get_input_stage(cmdline, shutdown_event=shutdown_event)
    output_stage = await get_output_stage(cmdline)

    driver = await cmdline.async_wal_proc_driver

    tasks = [asyncio.ensure_future(f) for f in [input_stage,
                                                driver.run(),
                                                output_stage]]
    try:
        await tasks[0]
    except StopAsyncIteration:
        # The pump gets a stopasync iteration when the breaker stops
        if shutdown_event.is_set():
            pass
        else:
            raise

    # Wait for the others stages, report errors, etc
    await asyncio.gather(*tasks[1:])

    if cmdline.is_progress_enabled:
        cmdline.progress_reporter.close()


def sync_main():
    args = docopt(__doc__, version=__version__, options_first=True)

    loop = asyncio.get_event_loop()
    try:
        cmdline = CmdlineInterface(args, loop)
        loop.run_until_complete(main(cmdline))

        # Required to avoid aiohttp resource warning on exit, see:
        # http://aiohttp.readthedocs.io/en/stable/client.html#graceful-shutdown
        loop.run_until_complete(asyncio.sleep(0.250))
    except CmdlineError as e:
        print(f'Fatal: {e}', file=sys.stderr)

        if args['--traceback']:
            traceback.print_exception(None, e, e.__traceback__, None,
                                      sys.stderr)
        sys.exit(e.status)


if __name__ == '__main__':
    sync_main()
