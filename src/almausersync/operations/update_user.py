import attr
from transitions import Machine

from almausersync.exceptions import ApiOperationFailedError, \
    UserUpdateFailedError, IdMigrationFailedError, \
    InvalidUserError
from almausersync.jobs import (
    validate_is_user_wrapper, UpdateAPIOperation, UpdateAPIOperationDriver,
    DefaultRetryStrategy)
from almausersync.states import requirestate, IllegalStateError
from almausersync.syncprocessor import State
from almausersync.transforms import IdMigrationTransform
from almausersync.utils import get_original_ids, get_new_ids, validate_ids
from almausersync.wal import wrap_with_source


def validate_user_ids(instance, attrib, value):
    try:
        validate_ids(value)
    except InvalidUserError as e:
        raise ValueError(f'{attrib.name!r} has invalid identifiers') from e


@attr.s()
class UpdateUserOperation(State):
    """The state required to track the state required to update a user with a
    new representation.
    """

    states = ['init', 'one_shot', 'id_migrate_intermediate',
              'id_migrate_final', 'failed', 'succeeded']

    machine: Machine = attr.ib(init=False)

    @machine.default
    def _(self):
        return Machine(states=self.states, model=self, auto_transitions=False,
                       initial='init', send_event=True)

    def __attrs_post_init__(self):
        self.machine.add_transition(
            'start', 'init', 'one_shot', unless='requires_id_migration')
        self.machine.add_transition(
            'start', 'init', 'id_migrate_intermediate',
            conditions='requires_id_migration')
        self.machine.add_transition(
            'finish', 'id_migrate_intermediate', 'id_migrate_final')
        self.machine.add_transition(
            'finish', ['one_shot', 'id_migrate_final'], 'succeeded')
        self.machine.add_transition(
            'fail',
            ['one_shot', 'id_migrate_intermediate', 'id_migrate_final'],
            'failed')

        self.machine.on_enter_id_migrate_intermediate(
            self._enter_id_migrate_intermediate)
        self.machine.on_enter_id_migrate_final(self._enter_id_migrate_final)
        self.machine.on_enter_one_shot(self._enter_one_shot)
        self.machine.on_enter_succeeded(self._enter_succeeded)
        self.machine.on_enter_failed(self._enter_failed)

    user_wrapper = attr.ib(validator=attr.validators.and_(
        validate_is_user_wrapper, validate_user_ids))
    tx_temporary_id = attr.ib(default=None)
    api_update_op = attr.ib(init=False, default=None)
    updated_user = attr.ib(init=False, default=None)
    failure_exception = attr.ib(init=False, default=None)
    _id_migration_tx_cls = attr.ib(default=IdMigrationTransform)

    @property
    def is_settled(self):
        return self.state in ['failed', 'succeeded']

    def requires_id_migration(self, event_data=None):
        return self._id_migration_tx_cls.requires_id_migration(
            self.user_wrapper)

    @requirestate('init')
    def init_id_tx_transform(self, temporary_id=attr.NOTHING):
        if self.tx_temporary_id is not None:
            raise ValueError('tx_temporary_id is already set')

        id_tx = self._id_migration_tx_cls(self.user_wrapper,
                                          temporary_id=temporary_id)
        self.tx_temporary_id = id_tx.temporary_id

    def _enter_id_migrate_intermediate(self, event_data):
        if self.tx_temporary_id is None:
            raise IllegalStateError('Must call init_id_tx_transform() before '
                                    'entering id_migrate_intermediate')

        self.api_update_op = UpdateAPIOperation(
            self.get_id_tx().get_intermediate_state())

    def _enter_id_migrate_final(self, event_data):
        user, = event_data.args
        # We have to go through intermediate to get here, which requires this
        # is initialised.
        assert self.tx_temporary_id is not None

        # We can replace the old one as we don't care about the intermediate
        # result other than the fact that it was created and is in place on
        # Alma.
        # xxxx: See comments in the ID TX about needing the intermediate result
        # user obj to handle auto-generated primary_id - can now do that here
        # w/ user param.
        self.api_update_op = UpdateAPIOperation(
            self.get_id_tx().get_final_state())

    def _enter_one_shot(self, event_data):
        self.api_update_op = UpdateAPIOperation(self.user_wrapper)

    def _enter_succeeded(self, event_data):
        user, = event_data.args
        self.updated_user = user

    def _enter_failed(self, event_data):
        cause_exc, = event_data.args
        if event_data.transition.source == 'id_migrate_final':
            exc_cls = IdMigrationFailedError
        else:
            exc_cls = UserUpdateFailedError

        # raise to attach traceback
        try:
            raise exc_cls('Unable to update user') from cause_exc
        except UserUpdateFailedError as exc:
            self.failure_exception = exc

    def get_id_tx(self):
        return self._id_migration_tx_cls(user_wrapper=self.user_wrapper,
                                         temporary_id=self.tx_temporary_id)


@attr.s()
class UpdateUserOperationDriver:
    _state: UpdateUserOperation = attr.ib(
        validator=attr.validators.instance_of(UpdateUserOperation))
    _wal_writer = attr.ib()
    _userapi = attr.ib()
    _retry_strategy = attr.ib(default=attr.Factory(DefaultRetryStrategy))
    _update_api_op_driver_cls = attr.ib(default=UpdateAPIOperationDriver)

    async def _init(self):
        if self._state.requires_id_migration():
            self._state.init_id_tx_transform()

            await self._wal_writer.log({
                'barrier': 'before-id-migration',
                'current_primary_id': next(
                    (v for t, v, a
                     in get_original_ids(self._state.user_wrapper)
                     if t == 'primary'), None),
                'final_primary_id': next(
                    (v for t, v, a
                     in get_new_ids(self._state.user_wrapper)
                     if t == 'primary'), None),
                'temporary_id': self._state.tx_temporary_id})

        self._state.start()

    def get_source_name(self):
        return self._state.state

    async def _perform_api_update(self):
        api_update_state = self._state.api_update_op
        api_update_driver = self._update_api_op_driver_cls(
            state=api_update_state,
            wal_writer=wrap_with_source(self._wal_writer,
                                        self.get_source_name()),
            userapi=self._userapi)

        try:
            user = await api_update_driver.run()
        except ApiOperationFailedError as e:
            assert api_update_state.state == 'failed'
            self._state.fail(e)
            raise self._state.failure_exception

        if api_update_state.state in ['succeeded', 'id_migrate_final']:
            self._state.finish(user)
            return
        else:
            raise IllegalStateError(
                'UpdateAPIOperation finished in unexpected state: '
                f'{api_update_state.state}')

    async def _run_once(self):
        state = self._state.state

        if state == 'init':
            await self._init()
        elif state in ['id_migrate_intermediate', 'id_migrate_final',
                       'one_shot']:
            await self._perform_api_update()
        elif state == 'failed':
            # Should never get here because we re-throw exceptions. If we are
            # re-created we won't start in the failed state because the replay
            # driver wouldn't need to create us if the op already failed.
            raise RuntimeError('This should never be reached')
        elif state == 'succeeded':
            return self._state.updated_user
        else:
            raise ValueError(f'Unknown state: {state}')

    async def run(self):
        while True:
            result = await self._run_once()
            if result:
                return result
