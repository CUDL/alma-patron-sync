ERR_PREFIX = 'Error while replaying write-ahead log:'


def validate_wal_message_dict(message):
    if not isinstance(message, dict):
        raise ValueError(f'{ERR_PREFIX} received non-dict message '
                         f'from WAL: {message!r}')
