import asyncio
import attr
import enum
from traceback import TracebackException
from transitions import Machine, MachineError

from almaload.almausers import wrapper_keys
from almausersync.almarest import BaseAlmaApiError, ErrorResponseAlmaApiError
from almausersync.almarest.constants import UserOverride, ErrorCodes
from almausersync.exceptions import TooManyAttemptsError, \
    ApiOperationFailedError
from almausersync.operations.helpers import validate_wal_message_dict
from almausersync.states import IllegalStateError, requirestate
from almausersync.syncprocessor import State
from almausersync.transforms import UserFingerprinter
from almausersync.utils import create_id, IdSelectionStrategy


@attr.s
class APIOperation(State):
    states = ['init', 'attempting', 'succeeded', 'failed', 'dirty']

    transitions = [
        ['start', 'init', 'attempting'],
        ['finish', ['attempting', 'dirty'], 'succeeded'],
        ['fail', ['attempting', 'dirty'], 'failed'],
        ['recover', ['attempting', 'dirty'], 'dirty'],
        ['retry', 'dirty', 'attempting']
    ]

    machine = attr.ib(init=False)
    failure_message = attr.ib(init=False, default=None)

    @machine.default
    def _(self):
        machine = Machine(states=self.states, initial='init',
                          transitions=self.transitions, model=self,
                          auto_transitions=False)
        machine.on_enter_failed(self._on_failed)
        return machine

    def _on_failed(self, failure_message):
        if failure_message is None:
            raise ValueError('cause_exception was None')

        self.failure_message = failure_message

    @property
    def is_settled(self):
        return self.state in ['succeeded', 'failed']


def validate_is_user_wrapper(instance, attrib, value):
    attr.validators.instance_of(dict)(instance, attrib, value)

    if ~wrapper_keys.alma_user not in value:
        raise ValueError(f'{attrib.name!r} is not a user_wrapper; has no '
                         f'{~wrapper_keys.alma_user!r} key')


@attr.s
class UpdateAPIOperation(APIOperation):
    user_wrapper = attr.ib(validator=validate_is_user_wrapper)
    updated_user = attr.ib(init=False, default=None)
    fingerprint_id = attr.ib(init=False, default=None)
    request_count = attr.ib(init=False, default=0)

    def __attrs_post_init__(self):
        self.machine.on_exit_init(self._exit_init)
        self.machine.on_enter_succeeded(self._enter_succeeded)

    def _exit_init(self):
        if not self.has_fingerprint_id:
            raise IllegalStateError(
                'initialise_fingerprint_id() must be called before leaving init')

    def _enter_succeeded(self, updated_user):
        if updated_user is None:
            raise ValueError('updated_user was None')
        if self.updated_user is not None:
            raise IllegalStateError('updated_user was already set')
        self.updated_user = updated_user

    @requirestate('attempting', 'dirty')
    def bump_request_count(self):
        self.request_count += 1

    @requirestate('init')
    def initialise_fingerprint_id(self, id=None):
        if id is None:
            id = create_id()
        if self.fingerprint_id is not None:
            raise ValueError('A fingerprint_id is already defined')

        self.fingerprint_id = id

    @property
    def has_fingerprint_id(self):
        return self.fingerprint_id is not None


@attr.s
class DeleteAPIOperation(APIOperation):
    user_wrapper = attr.ib(validator=validate_is_user_wrapper)
    request_count = attr.ib(init=False, default=0)

    @requirestate('attempting', 'dirty')
    def bump_request_count(self):
        self.request_count += 1


@attr.s
class DefaultRetryStrategy:
    max_attempts = attr.ib(validator=attr.validators.instance_of(int),
                           default=10)

    @max_attempts.validator
    def _(self, attrib, value):
        if value < 1:
            raise ValueError(f'{attrib.name} must be > 0')

    def can_retry(self, api_op, exception):
        # We handle temporary errors and retry - in the same way as if we'd
        # crashed and been restarted. Other API errors indicate issues with
        # data or API usage, they won't be resolved by retrying, so we halt.
        if not isinstance(exception, BaseAlmaApiError) or not exception.transitory:
            return False, exception

        if api_op.request_count < self.max_attempts:
            return True, None

        try:
            # Raise to attach a traceback
            raise TooManyAttemptsError(f'Reached the retry limit of '
                                       f'{self.max_attempts}') from exception
        except TooManyAttemptsError as e:
            return False, e


class DefaultFailureMessageStrategy:
    def message_from_exception(self, exc):
        return ''.join(TracebackException.from_exception(exc).format())


class UpdateAPIOperationBarriers(str, enum.Enum):
    BEFORE_ATTEMPTING = 'before-attempting'
    INCREMENT_REQUEST_COUNT = 'increment-request-count'
    SUCCEEDED = 'succeeded'
    FAILED = 'failed'


class DeleteAPIOperationBarriers(str, enum.Enum):
    BEFORE_ATTEMPTING = 'before-attempting-delete'
    INCREMENT_REQUEST_COUNT = 'increment-request-count-delete'
    SUCCEEDED = 'succeeded-delete'
    FAILED = 'failed-delete'


@attr.s
class UpdateAPIOperationDriver:
    _state: UpdateAPIOperation = attr.ib(
        validator=attr.validators.instance_of(UpdateAPIOperation))
    _wal_writer = attr.ib()
    _userapi = attr.ib()
    _id_selector = attr.ib(default=attr.Factory(IdSelectionStrategy))
    _fingerprinter_cls = attr.ib(default=UserFingerprinter)
    _retry_strategy = attr.ib(default=attr.Factory(DefaultRetryStrategy))
    _failure_message_strategy = attr.ib(
        default=attr.Factory(DefaultFailureMessageStrategy))

    async def _initialise(self):
        self._state.initialise_fingerprint_id()

        # Store the marker ID in the WAL so that on resuming a failed update we
        # can determine if an update was applied or not.
        await self._wal_writer.log({
            'barrier': UpdateAPIOperationBarriers.BEFORE_ATTEMPTING,
            'fingerprint_id': self._state.fingerprint_id})

        self._state.start()

    async def _record_request_attempt(self):
        self._state.bump_request_count()
        # Record request counts in the WAL so that we maintain retry counts
        # after resuming.
        await self._wal_writer.log({
            'barrier': UpdateAPIOperationBarriers.INCREMENT_REQUEST_COUNT})

    async def _attempt(self):
        fingerprinter = self._fingerprinter_cls(self._state.fingerprint_id)

        user_wrapper = fingerprinter.apply_fingerprint(
            self._state.user_wrapper)

        id_type, id_value = self._id_selector.get_id_for_update(user_wrapper)
        if id_type == 'primary':
            id_type = None
        user = user_wrapper[~wrapper_keys.alma_user]

        # Keep track of the number of retries, otherwise we could infinite loop
        await self._record_request_attempt()

        try:
            result = await self._userapi.update_user(
                user, previous_id=id_value, prev_id_type=id_type,
                override=UserOverride.all)
        except BaseAlmaApiError as e:
            await self._handle_failed_api_call(e)
            return

        assert fingerprinter.matches_fingerprint(result)
        await self._record_success(result)
        return

    async def _handle_failed_api_call(self, exc):
        can_retry, err = self._retry_strategy.can_retry(self._state, exc)

        if can_retry:
            self._state.recover()
        else:
            await self._record_failure(
                self._failure_message_strategy.message_from_exception(err))

    # FIXME: We should probably protect against multiple users being synced
    # at the same time with overlapping IDs.... e.g. IDs swapping from one user
    # to another. FML...

    async def _get_user(self, id_type, id_value):
        if id_type == 'primary':
            id_type = None

        # Keep track of the number of retries, otherwise we could infinite
        # loop
        await self._record_request_attempt()

        # Establish the current state by fetching the current state of our
        # user
        try:
            return (None, await self._userapi.get_user(id_value,
                                                       id_type=id_type))
        except BaseAlmaApiError as e:
            # Handle errors after both requests are done
            return e, None

    async def _clean_up(self):
        # We get here after an update attempt failed, and after our state is
        # restored from the WAL. We don't know if we requested an update and
        # it succeeded, or perhaps it failed. We can use our fingerprint
        # to determine if our update was applied or not. Note that if the
        # old user and the new user have no IDs in common then we have to
        # check both locations to see whether the user was actually moved.

        user_wrapper = self._state.user_wrapper

        ids = self._id_selector.get_ids_for_update_clean_up(user_wrapper)
        assert len(ids) in [1, 2]

        results = await asyncio.gather(*(self._get_user(t, v) for t, v in ids))

        fingerprinter = self._fingerprinter_cls(self._state.fingerprint_id)

        if all(e is not None for e, u in results):
            # Arbitrarily handle the first of the two errors and ignore the
            # other.
            await self._handle_failed_api_call(results[0][0])
            return

        # If any user matches the fingerprint we're done
        for (err, user) in results:
            if user is not None and fingerprinter.matches_fingerprint(user):
                # Update was already applied - we're done
                await self._record_success(user)
                return

        # If we have one (non-fingerprinted) user so the update didn't get
        # applied, retry the update
        self._state.retry()

    async def _record_success(self, user):
        await self._wal_writer.log({
            'barrier': UpdateAPIOperationBarriers.SUCCEEDED,
            'alma_user': user})
        self._state.finish(user)

    async def _record_failure(self, failure_message):
        await self._wal_writer.log({
            'barrier': UpdateAPIOperationBarriers.FAILED,
            'failure_message': failure_message})
        self._state.fail(failure_message)

    async def _run_one(self):
        state = self._state.state
        if state == 'init':
            await self._initialise()
        elif state == 'attempting':
            await self._attempt()
        elif state == 'dirty':
            await self._clean_up()
        elif state == 'succeeded':
            assert self._state.updated_user is not None
            return self._state.updated_user
        elif state == 'failed':
            raise ApiOperationFailedError(
                'Failed to update user', self._state.failure_message)
        else:
            raise ValueError(f'Unknown state: {state}')

    async def run(self):
        while True:
            result = await self._run_one()
            if result:
                return result


@attr.s
class DeleteAPIOperationDriver:
    # TODO: could probably extract a superclass APIOperationDriver
    _state: DeleteAPIOperation = attr.ib(
        validator=attr.validators.instance_of(DeleteAPIOperation))
    _wal_writer = attr.ib()
    _userapi = attr.ib()
    _id_selector = attr.ib(default=attr.Factory(IdSelectionStrategy))
    _retry_strategy = attr.ib(default=attr.Factory(DefaultRetryStrategy))
    _failure_message_strategy = attr.ib(
        default=attr.Factory(DefaultFailureMessageStrategy))

    # If what we want to delete doesn't exist, count that as success
    _sucess_codes = {
        ErrorCodes.USER_ID_DOESNT_EXIST_WITH_TYPE.value,
        ErrorCodes.USER_ID_DOESNT_EXIST.value
    }

    _barriers = DeleteAPIOperationBarriers

    async def _log(self, barrier, **kwargs):
        assert isinstance(barrier, self._barriers)
        kwargs['barrier'] = barrier
        await self._wal_writer.log(kwargs)

    async def _initialise(self):
        await self._log(self._barriers.BEFORE_ATTEMPTING)
        self._state.start()

    async def _record_request_attempt(self):
        self._state.bump_request_count()
        await self._log(self._barriers.INCREMENT_REQUEST_COUNT)

    async def _attempt(self):
        user_wrapper = self._state.user_wrapper

        id_type, id_value = self._id_selector.get_id_for_delete(user_wrapper)
        if id_type == 'primary':
            id_type = None

        await self._record_request_attempt()

        try:
            await self._userapi.delete_user(id_value, id_type)
        except BaseAlmaApiError as e:
            if isinstance(e, ErrorResponseAlmaApiError) \
                    and e.error_code in self._sucess_codes:
                pass
            else:
                await self._handle_failed_api_call(e)
                return

        await self._record_success(id_value)
        return

    async def _handle_failed_api_call(self, exc):
        can_retry, err = self._retry_strategy.can_retry(self._state, exc)

        if can_retry:
            self._state.recover()
        else:
            await self._record_failure(
                self._failure_message_strategy.message_from_exception(err))

    async def _clean_up(self):
        self._state.retry()

    async def _record_success(self, user_id):
        await self._log(self._barriers.SUCCEEDED, deleted_user=user_id)
        self._state.finish()

    async def _record_failure(self, failure_message):
        await self._log(self._barriers.FAILED, failure_message=failure_message)
        self._state.fail(failure_message)

    async def _run_one(self):
        state = self._state.state
        if state == 'init':
            await self._initialise()
        elif state == 'attempting':
            await self._attempt()
        elif state == 'dirty':
            await self._clean_up()
        elif state == 'succeeded':
            return True
        elif state == 'failed':
            raise ApiOperationFailedError(
                'Failed to update user', self._state.failure_message)
        else:
            raise ValueError(f'Unknown state: {state}')

    async def run(self):
        while True:
            result = await self._run_one()
            if result:
                return result


@attr.s
class UpdateAPIOperationReplayDriver:
    _state = attr.ib(validator=attr.validators.instance_of(UpdateAPIOperation))
    _wal_reader = attr.ib()

    _msg = 'Error while replaying write-ahead log:'

    def _handle_before_attempting(self, message):
        fingerprint_id = message.get('fingerprint_id')

        if not isinstance(fingerprint_id, str):
            raise ValueError(
                f'Invalid fingerprint_id value: {fingerprint_id}')

        self._state.initialise_fingerprint_id(fingerprint_id)
        # Immediately go to the recovery state - we'll continue from here
        # unless we turn out to be finished or failed.
        self._state.start()
        self._state.recover()

    def _handle_increment_request_count(self, message):
        self._state.bump_request_count()

    def _handle_succeeded(self, message):
        alma_user = message.get('alma_user')
        if not isinstance(alma_user, dict):
            raise ValueError(f'Invalid alma_user: {alma_user!r}')

        self._state.finish(alma_user)

    def _handle_failed(self, message):
        self._state.fail(message.get('failure_message'))

    # FIXME: Update this to return/throw like the regular driver
    async def run(self):
        async for message in self._wal_reader:
            validate_wal_message_dict(message)

            barrier = message.get('barrier')

            handler = {
                UpdateAPIOperationBarriers.BEFORE_ATTEMPTING:
                    self._handle_before_attempting,
                UpdateAPIOperationBarriers.INCREMENT_REQUEST_COUNT:
                    self._handle_increment_request_count,
                UpdateAPIOperationBarriers.FAILED:
                    self._handle_failed,
                UpdateAPIOperationBarriers.SUCCEEDED:
                    self._handle_succeeded
            }.get(barrier)
            if handler is None:
                raise ValueError(f'{self._msg} Unknown barrier: {barrier}')

            try:
                handler(message)
            except (MachineError, IllegalStateError) as e:
                raise ValueError(
                    f'{self._msg} Received barrier {barrier} while in state '
                    f'{self._state.state}') from e
            except ValueError as e:
                raise ValueError(f'{self._msg} Invalid message data') from e

        # We should either be not started, dirty or successful/failed
        assert self._state.state != 'attempting'
