"""
Generate a list of users with conflicting IDs from a json-stream export of
alma users.

Usage: conflictingusers [options] [<users>]
"""

from collections import defaultdict
import sys
import json

from almauserutils.jsonstreams import load
import docopt

from almausersync.utils import get_new_ids


def main():
    args = docopt.docopt(__doc__)

    input = (open(args['<users>'], 'rb') if args['<users>'] is not None
             else sys.stdin.buffer)

    format = args['--format']
    if args['--format'] not in ['csv', 'json']:
        print(f'Fatal: Unknown --format: {format}')
        raise SystemExit(1)

    users = load(input)
    ids = defaultdict(list)
    ignored = 0

    for u in users:
        _ids = set((t, v, a) for (t, v, a) in get_new_ids(u) if t != 'primary')
        pid = u.get('primary_id')
        if pid is None:
            ignored += 1
        for t, v, active in _ids:
            ids[v].append((pid, t, 'ACTIVE' if active else 'INACTIVE'))

    dupe_ids = {i: x for i, x in ids.items() if len(x) > 1}

    conflicting_users = defaultdict(set)
    for conflict_id, conflicts in dupe_ids.items():
        for pid, type, status in conflicts:
            if status == 'INACTIVE':
                conflicting_users[pid].add((type, conflict_id))

    json.dump(conflicting_users, sys.stdout, indent=2, default=_json_default)
    print()

    if ignored > 0:
        print(f'Ignored {ignored} users with no primary_id', file=sys.stderr)


def _json_default(val):
    if isinstance(val, set):
        return sorted(val)


if __name__ == '__main__':
    main()
