"""
This was used during investigations of AI-68 but doesn't actually
 delete anything. Generating similar reports in future will be much faster
 using the Alma Analytics rather than calling the fees and loans APIs for
 each user individually.
"""

import abc
import asyncio
import csv
import fileinput
import sys
from itertools import chain

from almausersync.almarest import *


class BaseCSV(abc.ABC):
    headers = ['primary_id', 'full_name', 'contact']

    def __init__(self, file):
        self._csv = csv.DictWriter(file, self.headers, lineterminator='\n')
        self._csv.writeheader()

    @abc.abstractmethod
    def __call__(self, *args, **kwargs):
        pass

    @staticmethod
    def _row_from_user(user):
        row = {'primary_id': user['primary_id'],
               'full_name': user['full_name']}
        try:
            contacts = user['contact_info']
            contact = next(chain(contacts['email'], contacts['address']))
            try:
                row['contact'] = contact['email_address']
            except KeyError:
                parts = [contact.get(key) for key in [
                    'line1', 'line2', 'line3', 'line4', 'line5', 'city',
                    'state_province', 'postal_code']]
                parts.append(contact.get('country', {}).get('desc'))
                row['contact'] = ', '.join(p for p in parts if p)
        except (LookupError, StopIteration):
            pass
        return row


class FeesCSV(BaseCSV):
    headers = BaseCSV.headers + ['type', 'status', 'balance', 'creation_time',
                                 'status_time', 'owner', 'item_barcode',
                                 'comment']

    def __call__(self, user, fees):
        base_row = self._row_from_user(user)
        for fee in fees:
            row = {'type': fee.get('type', {}).get('desc'),
                   'status': fee.get('status', {}).get('desc'),
                   'balance': fee.get('balance'),
                   'creation_time': fee.get('creation_time'),
                   'status_time': fee.get('status_time'),
                   'owner': fee.get('owner', {}).get('desc'),
                   'item_barcode': (fee.get('barcode') or {}).get('value'),
                   'comment': fee.get('comment')}
            row.update(base_row)
            self._csv.writerow(row)


class LoansCSV(BaseCSV):
    headers = BaseCSV.headers + ['library', 'circ_desk', 'loan_status',
                                 'loan_date', 'due_date', 'item_barcode',
                                 'call_number']

    def __call__(self, user, loans):
        base_row = self._row_from_user(user)
        for loan in loans:
            row = {'library': loan.get('library', {}).get('desc'),
                   'circ_desk': loan.get('circ_desk', {}).get('desc'),
                   'loan_status': loan.get('loan_status'),
                   'loan_date': loan.get('loan_date'),
                   'due_date': loan.get('due_date'),
                   'item_barcode': loan.get('item_barcode'),
                   'call_number': loan.get('call_number')}
            row.update(base_row)
            self._csv.writerow(row)


async def delete_users(api, user_ids, fees_writer, loans_writer):
    for user_id in user_ids:
        try:
            user, fees, loans = await asyncio.gather(api.get_user(user_id),
                                                     api.get_fees(user_id),
                                                     api.get_loans(user_id))
        except (IOError, BaseAlmaApiError):
            sys.exc_info()
            continue

        can_delete = True
        if fees['total_record_count'] > 0:
            can_delete = False
            fees_writer(user, fees['fee'])
        if loans['total_record_count'] > 0:
            can_delete = False
            loans_writer(user, loans['item_loan'])

        if can_delete:
            ...  # yield api.delete_user(user_id)


def main():
    user_ids = (line.strip() for line in fileinput.input()
                if line and line[0] != '#')

    # TODO: this doesn't like not being created in a coroutine
    api = UserApiClient(Session.create(
        apikey='l7xx10a9b17ea31f46f9b3ec6d5fcf57d713', region='eu'))

    with open('outstanding_fees.csv', 'wt') as fees_file:
        with open('outstanding_loans.csv', 'wt') as loans_file:
            fees_csv = FeesCSV(fees_file)
            loans_csv = LoansCSV(loans_file)
            coroutine = delete_users(api, user_ids, fees_csv, loans_csv)
            asyncio.get_event_loop().run_until_complete(coroutine)


if __name__ == '__main__':
    main()
