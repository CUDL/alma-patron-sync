import asyncio
import random
import sys

from asynctest import MagicMock, patch, sentinel
import attr
import pytest


from almausersync.syncprocessor import AsyncWalProcessorState, JobFactory, Job, AsyncWalProcessorDriver, \
    DEFAULT_ACTIVE_JOB_LIMIT
from almausersync.wal import JsonStreamWalWriter


@pytest.fixture()
def input_queue_limit():
    return 0


@pytest.fixture()
def input_queue(input_queue_limit):
    return asyncio.Queue(input_queue_limit)


@pytest.fixture()
def output_queue_limit():
    return 0


@pytest.fixture()
def output_queue(output_queue_limit):
    return asyncio.Queue(output_queue_limit)


@pytest.fixture()
def job_factory():
    return MagicMock(spec=JobFactory)


@pytest.fixture()
def awp_state_job_limit():
    return DEFAULT_ACTIVE_JOB_LIMIT

@pytest.fixture()
def awp_state(input_queue, output_queue, job_factory, awp_state_job_limit):
    return AsyncWalProcessorState(job_factory=job_factory,
                                  input_queue=input_queue,
                                  output_queue=output_queue,
                                  active_job_limit=awp_state_job_limit)


async def test_awp_state_submit_job(awp_state):
    result = asyncio.Future()
    job = Job(id=0, type='abc', input_data=[1, 2], state={}, result=result)

    awp_state.submit_job(job)

    assert len(awp_state.jobs) == 1


@pytest.fixture()
def create_driver():
    sleep = sys.float_info.epsilon
    async def driver(state, wal_writer):
        await asyncio.sleep(sleep)

        return state.value, 42

    return driver


@attr.s()
class SimpleState:
    value = attr.ib()
    is_settled = True


@pytest.fixture()
def simple_job_factory(create_driver):
    class JobFact(JobFactory):
        def create_state(self, type, data):
            return SimpleState(data)

        def create_driver(self, state, wal_writer):
            """Returns: An awaitable which runs the driver.
            """
            return create_driver(state, wal_writer)

        def create_replay_driver(self, state, wal_reader):
            raise NotImplementedError()
    return JobFact()

@pytest.fixture()
def wal_writer():
    return MagicMock(spec=JsonStreamWalWriter)


@pytest.fixture()
def aio_debug(event_loop):
    was_enabled = event_loop.get_debug()
    event_loop.set_debug(True)
    yield
    event_loop.set_debug(was_enabled)


@pytest.fixture()
def input_feeder(input_queue, delay_before_send_input, delay_between_inputs):
    async def feed_input(input_data):
        if delay_before_send_input:
            print('In: Sleeping before sending input')
            await asyncio.sleep(delay_before_send_input)

        for data in input_data:
            print(f'In: Submitting input: {data}')
            await input_queue.put(('simple', data))
            if delay_between_inputs:
                await asyncio.sleep(delay_between_inputs)
        await input_queue.put(None)
        print(f'In: Submitted all.')
    return feed_input


@pytest.fixture()
def output_consumer(awp_state, output_queue, delay_before_consume_output,
                    delay_between_outputs):
    async def handle_output(results):

        if delay_before_consume_output:
            await asyncio.sleep(delay_before_consume_output)

        while True:
            job = await output_queue.get()
            print(f'Out: Got output: {job!r}')

            if job is None:
                print('Out: Got None output')
                assert awp_state.is_settled
                break
            assert job.is_finished
            results.append(job)

            if delay_between_outputs:
                await asyncio.sleep(delay_between_outputs)

        print('Out: Done.')
    return handle_output


# TODO: test various permutations of queue size restrictions/delays.
@pytest.mark.parametrize('job_factory', [pytest.lazy_fixture('simple_job_factory')])
@pytest.mark.asyncio
@pytest.mark.usefixtures('aio_debug')

@pytest.mark.parametrize('delay_before_send_input', [None, 0.2 / 10])
@pytest.mark.parametrize('delay_before_consume_output', [None, 0.2 / 10])
@pytest.mark.parametrize('delay_before_driver', [None, 0.2 / 10])

@pytest.mark.parametrize('delay_between_inputs', [None, 1 / 200])
@pytest.mark.parametrize('delay_between_outputs', [None, 1 / 200])

@pytest.mark.parametrize('input_queue_limit', [0, 1, 2, 100])
@pytest.mark.parametrize('awp_state_job_limit', [1, 2, 100])
@pytest.mark.parametrize('output_queue_limit', [0, 1, 2, 100])
async def test_integration_1(
    awp_state, input_feeder, output_consumer, wal_writer, job_factory,
    delay_before_driver, input_queue_limit, awp_state_job_limit,
    output_queue_limit):

    input_data = list(range(int(max(input_queue_limit, awp_state_job_limit,
                             output_queue_limit) * 3.5)))
    results = []

    driver = AsyncWalProcessorDriver(state=awp_state, wal_writer=wal_writer)

    async def run_driver():
        if delay_before_driver:
            await asyncio.sleep(delay_before_driver)
        return await driver.run()

    inputter, runner, outputter = await asyncio.gather(
        input_feeder(input_data), run_driver(), output_consumer(results))

    assert all(j.result.done() for j in results)
    results = sorted(results, key=lambda j: j.id)


    assert input_data == [(await j.result)[0] for j in results]
