import asyncio
import time

from unittest.mock import MagicMock, sentinel
import pytest

from almausersync.async_utils import (
    SlidingWindowRateLimiter, BurstingRateLimiter)


@pytest.mark.asyncio
async def test_integration():
    thing = MagicMock()

    rate_limiter = SlidingWindowRateLimiter(10, 1)

    async def task(id):
        async with rate_limiter.limit():
            thing(id)

    start = time.monotonic()
    await asyncio.wait([task(i) for i in range(30)])
    end = time.monotonic()

    elapsed = end - start
    print(elapsed)
    assert elapsed >= 0.3


@pytest.mark.asyncio
async def test_bursting_rl_integration():
    thing = MagicMock()

    rate_limiter = BurstingRateLimiter(100)

    async def task(id):
        async with rate_limiter.limit():
            thing(id)

    start = time.monotonic()
    await asyncio.wait([task(i) for i in range(500)])
    end = time.monotonic()

    elapsed = end - start
    print(elapsed)
    assert elapsed >= 5
