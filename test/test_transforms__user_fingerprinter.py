import pytest

from almausersync.utils import create_id
from almausersync.transforms import UserFingerprinter


@pytest.fixture()
def fingerprinter():
    return UserFingerprinter()


@pytest.fixture()
def fingerprint():
    return create_id()


@pytest.fixture()
def fingerprinted_user(fingerprint):
    return {
        'alma_user': {
            'user_note': [
                {
                    'note_text': 'other-id-1',
                    'created_by': 'https://alma.lib.cam.ac.uk'
                                  '#standalone-user-sync-fingerprint'
                },
                {'some': 'note'},
                {'some': 'note'},
                {
                    'note_text': fingerprint,
                    'created_by': 'https://alma.lib.cam.ac.uk'
                                  '#standalone-user-sync-fingerprint'
                },
                {'some': 'note'},
                {
                    'note_text': 'other-id-2',
                    'created_by': 'https://alma.lib.cam.ac.uk'
                                  '#standalone-user-sync-fingerprint'
                }
            ]
        }
    }


def test_fingerprinter_generates_fingerprint(fingerprinter):
    assert isinstance(fingerprinter.fingerprint_uuid, str)
    assert len(fingerprinter.fingerprint_uuid) == 36


def test_fingerprinter_uses_provided_fingerprint():
    assert UserFingerprinter('my-id').fingerprint_uuid == 'my-id'


def test_untagged_user_doesnt_match_fingerprint(fingerprinter):
    assert not fingerprinter.matches_fingerprint({})


def test_tagged_user_matches_fingerprint(fingerprinted_user, fingerprint):
    fingerprinter = UserFingerprinter(fingerprint)
    assert fingerprinter.matches_fingerprint(fingerprinted_user)


def test_apply_fingerprint_strips_existing_fingerprints(fingerprinted_user):
    assert sum(n.get('created_by') == UserFingerprinter._CREATOR
               for n in fingerprinted_user['alma_user']['user_note']) == 3

    updated_user = UserFingerprinter().apply_fingerprint(fingerprinted_user)

    assert sum(n.get('created_by') == UserFingerprinter._CREATOR
               for n in updated_user['alma_user']['user_note']) == 1


def test_apply_fingerprint(fingerprinter):
    wrapper = {'alma_user': {}}
    tagged = fingerprinter.apply_fingerprint(wrapper)

    assert wrapper is not tagged
    assert wrapper != tagged

    assert not fingerprinter.matches_fingerprint(wrapper)
    assert fingerprinter.matches_fingerprint(tagged)
