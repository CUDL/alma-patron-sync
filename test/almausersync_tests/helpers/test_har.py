import base64

import pytest

from almausersync_tests.helpers.har import *


@pytest.fixture()
def content_b64_text():
    value = 'abcd€#¢'.encode('utf-8')
    return {
        'size': len(value),
        'mimeType': 'text/plain;charset=UTF-8',
        'text': base64.b64encode(value).decode(),
        'encoding': 'base64'
    }


@pytest.fixture()
def content_text():
    value = 'abcd€#¢'
    return {
        'size': len(value.encode()),
        'mimeType': 'text/plain;charset=UTF-8',
        'text': value
    }


@pytest.fixture()
def content_b64_octet_stream():
    value = 'abcd€#¢'.encode('utf-8')
    return {
        'size': len(value),
        'mimeType': 'application/octet-stream',
        'text': base64.b64encode(value).decode(),
        'encoding': 'base64'
    }


@pytest.mark.parametrize('content_js,size,bytes,text,is_binary,is_textual,'
                         'encoding',
[  # noqa: E128
    (
        pytest.lazy_fixture('content_b64_text'),
        10,
        b'abcd\xe2\x82\xac#\xc2\xa2',
        'abcd€#¢',
        False,
        True,
        'base64'
    ),
    (
        pytest.lazy_fixture('content_b64_octet_stream'),
        10,
        b'abcd\xe2\x82\xac#\xc2\xa2',
        None,
        True,
        False,
        'base64'
    ),
    (
        pytest.lazy_fixture('content_text'),
        10,
        b'abcd\xe2\x82\xac#\xc2\xa2',
        'abcd€#¢',
        False,
        True,
        None
    ),
])
def test_content(content_js, size, bytes, text,
                 is_binary, is_textual, encoding):
    content = HarContent.from_json(content_js)

    assert content.size == size
    if bytes is not None:
        assert content.bytes == bytes
    if text is not None:
        assert content.text == text
    assert content.is_binary is is_binary
    assert content.is_textual is is_textual
    assert content.encoding == encoding
