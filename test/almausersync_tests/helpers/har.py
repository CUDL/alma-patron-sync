"""
This module implements a (restricted) .har file parser. Used to represent HTTP
request and response data.

See: https://en.wikipedia.org/wiki/.har
"""
import base64
import cgi
import json

from aiohttp import web
import attr
from dateutil.parser import parse as parsedate
from multidict import CIMultiDict
from yarl import URL


@attr.s(frozen=True)
class HarFile:
    entries = attr.ib(convert=list)

    @classmethod
    def from_file(cls, file):
        return cls.from_json(json.load(file))

    @classmethod
    def from_string(cls, string):
        return cls.from_json(json.loads(string))

    @classmethod
    def from_json(cls, har_js):
        return HarFile(HarEntry.from_json(entry_js)
                       for entry_js in har_js['entries'])

    @property
    def singular_entry(self):
        if len(self.entries) != 1:
            raise ValueError('HAR file doesn\'t have exactly one entry')
        return self.entries[0]


@attr.s(frozen=True)
class HarEntry:
    started = attr.ib()
    time = attr.ib()
    request = attr.ib()
    response = attr.ib()

    @classmethod
    def from_json(cls, entry_js):
        return HarEntry(**{
            'started': (parsedate(entry_js['startedDateTime'])
                        if entry_js['startedDateTime'] else None),
            'time': entry_js['time'],
            'request': HarRequest.from_json(entry_js['request']),
            'response': HarResponse.from_json(entry_js['response'])
        })


def _headers_to_multidict(headers):
    return CIMultiDict((h.name, h) for h in headers)


@attr.s(frozen=True)
class HarRequest:
    method = attr.ib()
    url = attr.ib(convert=URL)
    http_version = attr.ib()
    headers = attr.ib(convert=_headers_to_multidict)
    body = attr.ib()

    @classmethod
    def from_json(cls, req_js):
        return HarRequest(**{
            'method': req_js['method'],
            'url': req_js['url'],
            'http_version': req_js.get('httpVersion'),
            'headers': [
                HarHeader.from_json(hdr_js)
                for hdr_js in req_js.get('headers', [])
            ],
            'body': (HarPostData.from_json(req_js['postData'])
                     if 'postData' in req_js else None)
        })

    @property
    def simple_headers(self):
        return CIMultiDict((k, h.raw_value) for k, h in self.headers.items())


@attr.s(frozen=True)
class HarPostData:
    raw_mime = attr.ib()
    text = attr.ib()

    @property
    def mime_type(self):
        return cgi.parse_header(self.raw_mime or '')[0] or None

    @property
    def charset(self):
        return cgi.parse_header(self.raw_mime or '')[1].get('charset')

    @classmethod
    def from_json(cls, pd_json):
        return HarPostData(**{
            'raw_mime': pd_json['mimeType'],
            'text': pd_json['text']
        })


@attr.s(frozen=True)
class HarResponse:
    status = attr.ib()
    status_text = attr.ib()
    http_version = attr.ib()
    headers = attr.ib(convert=_headers_to_multidict)
    content = attr.ib()

    @classmethod
    def from_json(cls, resp_js):
        return HarResponse(**{
            'status': resp_js['status'],
            'status_text': resp_js['statusText'],
            'http_version': resp_js.get('httpVersion'),
            'headers': [
                HarHeader.from_json(hdr_js)
                for hdr_js in resp_js.get('headers', [])
            ],
            'content': (HarContent.from_json(resp_js['content'])
                        if resp_js.get('content') else None)
        })

    @property
    def simple_headers(self):
        return CIMultiDict((k, h.raw_value) for k, h in self.headers.items())

    def as_aiohttp_response(self):
        args = {
            'status': self.status,
            'reason': self.status_text,
            'headers': self.simple_headers
        }

        if self.content is not None:
            try:
                args.update({'body': self.content.bytes})
            except ValueError:
                args.update({'text': self.content.text})

            if 'Content-Type' not in self.headers:
                args.update({'content_type': self.content.mime_type,
                             'charset': self.content.charset})

        return web.Response(**args)


@attr.s(frozen=True)
class HarHeader:
    name = attr.ib()
    raw_value = attr.ib()

    @property
    def value(self):
        return cgi.parse_header(self.raw_value)[0]

    @property
    def props(self):
        return cgi.parse_header(self.raw_value)[1]

    @classmethod
    def from_json(cls, resp_js):
        return HarHeader(**{
            'name': resp_js['name'],
            'raw_value': resp_js['value']
        })


@attr.s(frozen=True)
class HarContent:
    size = attr.ib()
    raw_text = attr.ib()
    raw_mime = attr.ib()
    encoding = attr.ib()

    @property
    def mime_type(self):
        return cgi.parse_header(self.raw_mime or '')[0] or None

    @property
    def charset(self):
        return cgi.parse_header(self.raw_mime or '')[1].get('charset')

    @property
    def is_binary(self):
        return self.encoding is not None and self.charset is None

    @property
    def is_textual(self):
        return self.charset is not None

    @property
    def text(self):
        if not self.is_textual:
            raise ValueError('Content is not textual')

        if self.encoding is not None:
            return self.bytes.decode(self.charset)
        return self.raw_text

    @property
    def bytes(self):
        if self.encoding is not None:
            if self.encoding == 'base64':
                return base64.b64decode(self.raw_text)
            raise ValueError(f'Unsupported encoding: {self.encoding}')

        if self.charset:
            return self.raw_text.encode(self.charset)

        raise ValueError('Unable to determine original binary representation')

    @classmethod
    def from_json(cls, cont_js):
        return HarContent(**{
            'size': cont_js['size'],
            'raw_text': cont_js['text'],
            'raw_mime': cont_js['mimeType'],
            'encoding': cont_js.get('encoding')
        })
