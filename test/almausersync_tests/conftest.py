from functools import singledispatch
from io import BytesIO, StringIO
from unittest.mock import Mock, MagicMock

import aiofiles
import pytest


@pytest.fixture()
def mock_aio_file():
    return MagicMock()


@pytest.fixture()
def aiofiles_singledispatch_sync_open(monkeypatch):
    monkeypatch.setattr(aiofiles.threadpool, 'sync_open',
                        singledispatch(aiofiles.threadpool.sync_open))


@pytest.fixture()
def aiofiles_support_mock(aiofiles_singledispatch_sync_open):
    aiofiles.threadpool.wrap.register(Mock)(
        lambda *args, **kwargs:
            aiofiles.threadpool.AsyncBufferedIOBase(*args, **kwargs))

    @aiofiles.threadpool.sync_open.register(Mock)
    def _sync_open(mock, *args, **kwargs):
        return mock


@pytest.fixture()
def aiofiles_support_bytesio(aiofiles_singledispatch_sync_open):
    @aiofiles.threadpool.wrap.register(BytesIO)
    def _wrap(*args, **kwargs):
        return aiofiles.threadpool.AsyncBufferedIOBase(*args, **kwargs)

    @aiofiles.threadpool.sync_open.register(BytesIO)
    def _sync_open(bytesio, *args, **kwargs):
        return bytesio


@pytest.fixture()
def aiofiles_support_stringio(aiofiles_singledispatch_sync_open):
    @aiofiles.threadpool.wrap.register(StringIO)
    def _wrap(*args, **kwargs):
        return aiofiles.threadpool.AsyncTextIOWrapper(*args, **kwargs)

    @aiofiles.threadpool.sync_open.register(StringIO)
    def _sync_open(stringio, *args, **kwargs):
        return stringio
