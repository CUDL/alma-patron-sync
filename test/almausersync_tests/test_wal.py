import asyncio
from io import StringIO

import aiofiles
from asynctest.mock import MagicMock, sentinel, call, patch
import pytest

from almausersync.async_utils import anext
from almausersync import wal
from almausersync.wal import WalWriter, BufferingWalWriter


@pytest.mark.usefixtures('aiofiles_support_mock')
@pytest.mark.asyncio
@pytest.mark.parametrize('log_args,expected_write_calls', [
    ((('value',), {}), [call('"value"'), call('\n')]),
    (((), {'obj': 'value'}), [call('"value"'), call('\n')]),
    (((), {'objects': ['value1', 'value2']}), [
        call('"value1"'), call('\n'),
        call('"value2"'), call('\n'),
    ])
])
async def test_log_writes_and_fsyncs_file(log_args, expected_write_calls):
    file = MagicMock()
    file.fileno.return_value = sentinel.fileno

    async with aiofiles.open(file) as f:
        jsonwal = wal.JsonStreamWalWriter(f)

        with patch('os.fsync') as fsync:
            await jsonwal.log(*log_args[0], **log_args[1])

        assert file.write.mock_calls == expected_write_calls
        file.flush.assert_called_once_with()
        fsync.assert_called_once_with(sentinel.fileno)


class TestWalWriter(WalWriter):
    async def log(self, obj=None, objects=None):
        pass

    async def _log(self, objects):
        pass


@pytest.fixture()
def target_writer():
    tw = MagicMock(spec=TestWalWriter)

    # The BufferingWalWriter reuses lists, so we need to copy them to retain
    # the objects passed on each call
    async def copy_log(objects=None):
        assert isinstance(objects, list)
        await tw._log(list(objects))
    tw.log.side_effect = copy_log
    return tw


def _assert_all_buffers_spare(buffering_wal_writer):
    assert len(buffering_wal_writer._spare) == 2
    assert buffering_wal_writer._writing is None
    assert buffering_wal_writer._pending is None


@pytest.mark.parametrize('log_args,logged_objects', [
    ((('foo',), {}), ['foo']),
    (((), {'obj': 'foo'}), ['foo']),
    (((), {'objects': ['foo']}), ['foo']),
    (((), {'objects': ['foo', 'bar']}), ['foo', 'bar'])
])
async def test_buffering_wal_writer_single_log(log_args, logged_objects,
                                               target_writer):
    bww = BufferingWalWriter(wal_writer=target_writer)

    await bww.log(*log_args[0], **log_args[1])

    assert target_writer._log.called_once_with(logged_objects)
    _assert_all_buffers_spare(bww)


async def test_buffering_wal_writer_sequential_logs(target_writer):
    bww = BufferingWalWriter(wal_writer=target_writer)

    await bww.log('foo')
    await bww.log('bar')

    assert target_writer._log.mock_calls == [
        call(['foo']),
        call(['bar'])
    ]
    _assert_all_buffers_spare(bww)


async def test_buffering_wal_writer_blocked_logs_get_grouped(target_writer):
    bww = BufferingWalWriter(wal_writer=target_writer)

    first_log_started = asyncio.Event()
    second_logs_scheduled = asyncio.Event()

    async def on_log():
        first_log_started.set()
        await second_logs_scheduled.wait()

    target_writer._log.side_effect = [on_log(), None]

    async def log_first():
        await bww.log('foo')

    async def log_more():
        await first_log_started.wait()
        logs = [asyncio.ensure_future(log) for log in [bww.log('bar'),
                                                       bww.log('baz'),
                                                       bww.log('boz')]]
        await asyncio.sleep(0)
        second_logs_scheduled.set()
        await asyncio.gather(*logs)

    await asyncio.gather(log_first(), log_more())

    assert target_writer._log.mock_calls == [
        call(['foo']),
        call(['bar', 'baz', 'boz'])
    ]
    _assert_all_buffers_spare(bww)


async def test_buffering_wal_writer_repeated_blocked_logs_get_grouped(
        target_writer):
    bww = BufferingWalWriter(wal_writer=target_writer)

    first_log_started = asyncio.Event()
    second_logs_scheduled = asyncio.Event()
    second_log_started = asyncio.Event()
    third_logs_scheduled = asyncio.Event()

    async def on_log1():
        first_log_started.set()
        await second_logs_scheduled.wait()

    async def on_log2():
        second_log_started.set()
        await third_logs_scheduled.wait()

    target_writer._log.side_effect = [on_log1(), on_log2(), None]

    async def log_first():
        await bww.log('foo')
        assert call(['foo']) in target_writer._log.mock_calls

    async def log_second():
        await first_log_started.wait()
        logs = [asyncio.ensure_future(log) for log in [bww.log('bar'),
                                                       bww.log('baz'),
                                                       bww.log('boz')]]
        await asyncio.sleep(0)
        second_logs_scheduled.set()
        await asyncio.gather(*logs)
        assert call(['bar', 'baz', 'boz']) in target_writer._log.mock_calls

    async def log_third():
        await second_log_started.wait()
        logs = [asyncio.ensure_future(log) for log in [bww.log('abc'),
                                                       bww.log('def'),
                                                       bww.log('123')]]
        await asyncio.sleep(0)
        third_logs_scheduled.set()
        await asyncio.gather(*logs)
        assert call(['abc', 'def', '123']) in target_writer._log.mock_calls

    await asyncio.gather(log_first(), log_second(), log_third())

    assert target_writer._log.mock_calls == [
        call(['foo']),
        call(['bar', 'baz', 'boz']),
        call(['abc', 'def', '123'])
    ]
    _assert_all_buffers_spare(bww)


@pytest.mark.usefixtures('aiofiles_support_stringio')
@pytest.mark.asyncio
async def test_json_stream_wal_reader_is_async_iterator():
    file = StringIO('["a"]\n')

    async with aiofiles.open(file) as f:
        jsonwal = wal.JsonStreamWalReader(f)

        assert await anext(jsonwal) == ['a']


@pytest.mark.usefixtures('aiofiles_support_stringio')
@pytest.mark.asyncio
async def test_json_stream_wal_reader_is_async_iterable():
    file = StringIO('["a"]\n{}\n[1,2,3]\n')

    async with aiofiles.open(file) as f:
        logs = []
        async for l in wal.JsonStreamWalReader(f):
            logs.append(l)
        assert logs == [["a"], {}, [1, 2, 3]]


@pytest.mark.usefixtures('aiofiles_support_stringio')
@pytest.mark.asyncio
async def test_json_stream_wal_reader_ignores_trailing_garbage():
    file = StringIO('["a"]\n{}\n[1,2,3')

    async with aiofiles.open(file) as f:
        logs = []
        async for l in wal.JsonStreamWalReader(f):
            logs.append(l)
        assert logs == [["a"], {}]


@pytest.fixture()
def mock_writer():
    return MagicMock(spec=wal.JsonStreamWalWriter)


@pytest.mark.asyncio
async def test_nested_writer_logs_result_of_applying_wrapper_to_logged_obj(
        mock_writer):
    wrapper = MagicMock()

    writer = wal.NestedJsonStreamWalWriter(wal_writer=mock_writer,
                                           wrapper=wrapper)

    await writer.log(sentinel)

    wrapper.assert_called_once_with(sentinel)
    mock_writer.log.assert_called_once_with(wrapper(sentinel))


@pytest.mark.asyncio
async def test_wrap_with_source_wraps_messages(mock_writer):
    writer = wal.wrap_with_source(mock_writer, sentinel.name)

    await writer.log(sentinel.message)

    mock_writer.log.assert_called_once_with({
        'source': sentinel.name,
        'message': sentinel.message
    })


@pytest.mark.asyncio
async def test_wrap_with_source_wraps_recursively(mock_writer):
    level1 = wal.wrap_with_source(mock_writer, sentinel.name_1)
    level2 = wal.wrap_with_source(level1, sentinel.name_2)

    await level2.log(sentinel.message)

    mock_writer.log.assert_called_once_with({
        'source': sentinel.name_1,
        'message': {
            'source': sentinel.name_2,
            'message': sentinel.message
        }
    })
