from asyncio import Queue
from unittest.mock import MagicMock

import pytest

from almausersync.async_coroutines import async_prewarmed, pump, apply, into_queue, from_queue, partition


@pytest.fixture()
def async_coro():
    async def async_coro(thing):
        while True:
            thing.foo((yield))

    return async_coro


@pytest.mark.asyncio
async def test_async_generator_requires_asending_none_first(async_coro):
    ac = async_coro(MagicMock())
    with pytest.raises(TypeError) as exc_info:
        await ac.asend('blah')

    assert str(exc_info.value) == (
        'can\'t send non-None value to a just-started async generator')


@pytest.mark.asyncio
async def test_async_prewarmed_generator_doesnt_require_asending_none_first(
        async_coro):
    thing = MagicMock()

    ac = await async_prewarmed(async_coro)(thing)
    await ac.asend('blah')

    thing.foo.assert_called_once_with('blah')


@pytest.mark.asyncio
async def test_apply():
    things = []

    applier = await apply(lambda x: x + 10, await to_list(things))
    await applier.asend(1)
    await applier.asend(2)
    await applier.asend(3)
    await applier.aclose()

    assert things == [11, 12, 13]


@async_prewarmed
async def to_list(things):
    while True:
        things.append((yield))


async def drain_queue(queue):
    bucket = []
    while True:
        value = await queue.get()
        bucket.append(value)

        if value is None:
            return bucket


@pytest.mark.asyncio
async def test_from_queue():
    queue = Queue()
    things = [1, 2, 3]

    for t in things:
        queue.put_nowait(t)
    queue.put_nowait(None)

    dest = []
    await from_queue(queue, await to_list(dest))
    assert things == dest


@pytest.mark.asyncio
async def test_into_queue():
    queue = Queue()
    things = [1, 2, 3]
    await pump(things, await into_queue(queue))

    assert await drain_queue(queue) == [1, 2, 3, None]


@pytest.mark.asyncio
async def test_partition():
    things = list(range(6))

    a, b = [], []

    p = await partition(lambda x: x % 2, {
        0: await to_list(a),
        1: await to_list(b)
    })

    await pump(things, p)

    assert a == [0, 2, 4]
    assert b == [1, 3, 5]
