"""
SyncProcessor
    UserSyncOp
        a: DeleteOp
            APIDeleteOp
        b: UpdateOp
            a: APIUpdateOp
            b: APIUpdateOp w/ migrated IDs
               APIUpdateOp w/ final state
"""
import pytest
from asynctest.mock import MagicMock, sentinel, call
from transitions import MachineError

from almausersync.almarest import (UserApiClient, BaseAlmaApiError)
from almausersync.almarest.constants import UserOverride
from almausersync.exceptions import TooManyAttemptsError, \
    ApiOperationFailedError
from almausersync.jobs import (
    UpdateAPIOperation, UpdateAPIOperationDriver, DefaultRetryStrategy,
    DefaultFailureMessageStrategy, UpdateAPIOperationReplayDriver)
from almausersync.states import IllegalStateError
from almausersync.transforms import UserFingerprinter
from almausersync.utils import IdSelectionStrategy
from almausersync.wal import JsonStreamWalWriter


@pytest.fixture()
def user_wrapper(alma_user, original_ids):
    return {
        'alma_user': alma_user,
        'original_ids': original_ids
    }


@pytest.fixture()
def alma_user():
    return MagicMock(spec=dict)


@pytest.fixture()
def original_ids():
    return [
        {'type': 'primary', 'value': 'foo'}
    ]


@pytest.fixture()
def init_op(user_wrapper):
    op = UpdateAPIOperation(user_wrapper)
    assert op.state == 'init'
    return op


@pytest.fixture()
def attempting_op(init_op):
    init_op.initialise_fingerprint_id()
    init_op.start()
    assert init_op.state == 'attempting'
    return init_op


@pytest.fixture()
def succeeded_op(attempting_op):
    attempting_op.finish(sentinel.result)
    assert attempting_op.state == 'succeeded'
    return attempting_op


@pytest.fixture()
def failed_op(attempting_op):
    attempting_op.fail(sentinel.msg)
    assert attempting_op.state == 'failed'
    return attempting_op


@pytest.fixture()
def dirty_op(attempting_op):
    attempting_op.recover()
    assert attempting_op.state == 'dirty'
    return attempting_op


@pytest.fixture()
def wal_writer():
    return MagicMock(spec=JsonStreamWalWriter)


@pytest.fixture()
def userapi():
    return MagicMock(spec=UserApiClient)


@pytest.fixture()
def fingerprinter():
    fp = MagicMock(spec=UserFingerprinter)
    fp.apply_fingerprint.side_effect = lambda user_wrapper: user_wrapper
    return fp


@pytest.fixture()
def fingerprinter_cls(fingerprinter):
    fingerprinter_cls = MagicMock()
    fingerprinter_cls.return_value = fingerprinter
    return fingerprinter_cls


@pytest.fixture(params=['primary', 'other'])
def id_sel_type(request):
    return request.param


@pytest.fixture()
def id_selector(id_sel_type):
    idsel = MagicMock(spec=IdSelectionStrategy)
    idsel.get_id_for_update.return_value = (id_sel_type, sentinel.id_val)
    return idsel


@pytest.fixture(params=[
    [('primary', sentinel.id_a)],
    [('other', sentinel.id_a)],
    [(None, sentinel.id_a)],
    [('primary', sentinel.id_a), ('primary', sentinel.id_b)],
    [('foo', sentinel.id_a), ('foo', sentinel.id_b)],
    [('other', sentinel.id_a), ('primary', sentinel.id_b)],
])
def cleanup_ids(request):
    return request.param


@pytest.fixture()
def cleanup_id_selector(cleanup_ids):
    idsel = MagicMock(spec=IdSelectionStrategy)
    idsel.get_ids_for_update_clean_up.return_value = cleanup_ids
    return idsel


@pytest.fixture()
def retry_strategy():
    return MagicMock(spec=DefaultRetryStrategy)


@pytest.fixture()
def failure_msg_maker():
    return MagicMock(spec=DefaultFailureMessageStrategy)


@pytest.fixture()
def driver(op, wal_writer, userapi, fingerprinter_cls, id_selector,
           retry_strategy, failure_msg_maker):

    return UpdateAPIOperationDriver(state=op, wal_writer=wal_writer,
                                    userapi=userapi,
                                    fingerprinter_cls=fingerprinter_cls,
                                    id_selector=id_selector,
                                    retry_strategy=retry_strategy,
                                    failure_message_strategy=failure_msg_maker)


@pytest.fixture()
def temporary_error():
    return BaseAlmaApiError(is_transitory=True)


@pytest.fixture()
def hard_error():
    return BaseAlmaApiError()


def test_leaving_init_requires_fingerprint_id(init_op):
    assert init_op.state == 'init'
    with pytest.raises(IllegalStateError):
        init_op.start()


def test_leaving_init(init_op):
    init_op.initialise_fingerprint_id()
    init_op.start()


def test_initialise_fingerprint_id_accepts_arg(init_op):
    init_op.initialise_fingerprint_id('foo')
    assert init_op.fingerprint_id == 'foo'


def test_initialise_fingerprint_id_arg_is_optional(init_op):
    assert not init_op.has_fingerprint_id
    init_op.initialise_fingerprint_id()
    assert init_op.has_fingerprint_id


def test_bump_request_count(attempting_op):
    assert attempting_op.request_count == 0
    attempting_op.bump_request_count()
    assert attempting_op.request_count == 1
    attempting_op.bump_request_count()
    assert attempting_op.request_count == 2


@pytest.mark.parametrize('op', [pytest.lazy_fixture('init_op')])
async def test_driver_initialise(op, wal_writer, driver):
    await driver._initialise()

    assert op.state == 'attempting'
    wal_writer.log.assert_called_once_with({
        'barrier': 'before-attempting',
        'fingerprint_id': op.fingerprint_id
    })


@pytest.mark.parametrize('exc, args', [
    (ValueError, (None,)),
    (TypeError, ())  # no exception passed
])
def test_op_failing_requires_exception_argument(attempting_op, exc, args):
    with pytest.raises(exc):
        attempting_op.fail(*args)


def test_op_failing_requires_exception_value(attempting_op):
    with pytest.raises(ValueError):
        attempting_op.fail(None)


def test_op_failing_with_exception(attempting_op):
    exc = RuntimeError('boom')
    attempting_op.fail(exc)

    assert attempting_op.state == 'failed'
    assert attempting_op.failure_message == exc


def test_op_succeeding(attempting_op):
    attempting_op.finish(sentinel)
    assert attempting_op.state == 'succeeded'
    assert attempting_op.updated_user == sentinel


@pytest.mark.parametrize('exc, args', [
    (ValueError, (None,)),
    (TypeError, ())
])
def test_op_succeeding_requires_user_argument(attempting_op, exc, args):
    with pytest.raises(exc):
        attempting_op.finish(*args)


@pytest.mark.parametrize('op', [pytest.lazy_fixture('attempting_op')])
async def test_attempt(driver, op, fingerprinter, user_wrapper, alma_user,
                       id_selector, id_sel_type, userapi, wal_writer):
    userapi.update_user.return_value = sentinel.result
    pid_type = None if id_sel_type == 'primary' else id_sel_type

    await driver._attempt()

    assert op.state == 'succeeded'
    assert op.updated_user == sentinel.result

    assert wal_writer.log.mock_calls == [
        call({'barrier': 'increment-request-count'}),
        call({'barrier': 'succeeded', 'alma_user': sentinel.result})
    ]

    fingerprinter.apply_fingerprint.assert_called_once_with(user_wrapper)

    id_selector.get_id_for_update.assert_called_once_with(user_wrapper)

    userapi.update_user.assert_called_once_with(
        alma_user, previous_id=sentinel.id_val, prev_id_type=pid_type,
        override=UserOverride.all)

    fingerprinter.matches_fingerprint.assert_called_once_with(sentinel.result)


@pytest.mark.parametrize('op', [pytest.lazy_fixture('attempting_op')])
async def test_attempt_handles_alma_api_errors(
        driver, userapi, op, retry_strategy, failure_msg_maker, wal_writer):

    err = BaseAlmaApiError('Boom!')
    userapi.update_user.side_effect = err
    retry_strategy.can_retry.return_value = False, sentinel.err
    failure_msg_maker.message_from_exception.return_value = sentinel.msg

    await driver._attempt()

    retry_strategy.can_retry.assert_called_once_with(op, err)
    failure_msg_maker.message_from_exception.assert_called_once_with(
        sentinel.err)

    assert op.state == 'failed'
    assert op.failure_message == sentinel.msg

    assert wal_writer.log.mock_calls == [
        call({'barrier': 'increment-request-count'}),
        call({'barrier': 'failed', 'failure_message': sentinel.msg})
    ]


@pytest.mark.parametrize('op', [pytest.lazy_fixture('attempting_op')])
async def test_attempt_retries_according_to_retry_strategy(
        driver, userapi, op, retry_strategy, failure_msg_maker):

    err = BaseAlmaApiError('Boom!')
    userapi.update_user.side_effect = err
    retry_strategy.can_retry.return_value = True, None

    await driver._attempt()

    assert op.state == 'dirty'
    failure_msg_maker.message_from_exception.assert_not_called()


def test_default_retry_strategy_requires_positive_retry_limit():
    with pytest.raises(ValueError):
        DefaultRetryStrategy(0)


def test_default_retry_strategy_has_default_retry_limit():
    rs = DefaultRetryStrategy()
    assert rs.max_attempts == 10


def test_default_retry_strategy_allows_temporary_errors_up_to_limit(
        attempting_op, temporary_error):

    rs = DefaultRetryStrategy(3)

    for x in range(3):
        can_retry, err = rs.can_retry(attempting_op, temporary_error)
        attempting_op.bump_request_count()
        assert can_retry is True
        assert err is None

    can_retry, err = rs.can_retry(attempting_op, temporary_error)
    assert can_retry is False
    assert isinstance(err, TooManyAttemptsError)
    assert err.__cause__ is temporary_error


def test_default_retry_strategy_wont_retry_hard_errors(init_op, hard_error):
    rs = DefaultRetryStrategy(3)

    can_retry, err = rs.can_retry(init_op, hard_error)
    assert can_retry is False
    assert err is hard_error


@pytest.fixture()
def exc_with_tb():
    try:
        raise ValueError('weeee!')
    except ValueError as e:
        return e


def test_default_failure_message_strategy_formats_exc_as_string_with_tb(
        exc_with_tb):

    fms = DefaultFailureMessageStrategy()
    msg = fms.message_from_exception(exc_with_tb)

    assert 'weeee!' in msg
    assert 'exc_with_tb' in msg


def test_dirty_op_can_become_dirty(dirty_op):
    dirty_op.recover()
    assert dirty_op.state == 'dirty'


def test_dirty_op_can_be_too_dirty_to_clean(dirty_op):
    dirty_op.fail(':(')
    assert dirty_op.state == 'failed'


def test_dirty_op_can_become_clean_by_retrying(dirty_op):
    dirty_op.retry()
    assert dirty_op.state == 'attempting'


def test_dirty_op_can_become_clean_by_finishing(dirty_op):
    dirty_op.finish({})
    assert dirty_op.state == 'succeeded'


@pytest.mark.parametrize('op', [pytest.lazy_fixture('dirty_op')])
@pytest.mark.parametrize('id_selector',
                         [pytest.lazy_fixture('cleanup_id_selector')])
@pytest.mark.parametrize('fingerprint_matches', [
    (False, True), (False, False),
    # These two don't occur in practice, but nice to be robust in handling them
    (True, False), (True, True)
])
async def test_successful_clean_up(
        driver, op, fingerprinter, fingerprint_matches, user_wrapper,
        id_selector, cleanup_ids, userapi, wal_writer):

    fingerprinter.matches_fingerprint.side_effect = fingerprint_matches
    userapi.get_user.return_value = sentinel.result

    await driver._clean_up()

    iter_count = len(cleanup_ids)

    if any(fingerprint_matches[:iter_count]):
        assert op.state == 'succeeded'
        assert op.updated_user == sentinel.result

        assert (call({'barrier': 'succeeded', 'alma_user': sentinel.result}
                     ) == wal_writer.log.mock_calls[-1])
    else:
        assert op.state == 'attempting'

    id_selector.get_ids_for_update_clean_up.assert_called_once_with(
        user_wrapper)

    assert len(userapi.get_user.mock_calls) == iter_count

    userapi.get_user.assert_has_calls([
        call(v, id_type=(None if t == 'primary' else t))
        for (t, v) in cleanup_ids[:iter_count]], any_order=True)

    wal_writer.log.assert_has_calls(
        [call({'barrier': 'increment-request-count'})] * iter_count)

    fingerprinter.matches_fingerprint.assert_any_call(sentinel.result)


@pytest.mark.parametrize('op', [pytest.lazy_fixture('dirty_op')])
@pytest.mark.parametrize('id_selector',
                         [pytest.lazy_fixture('cleanup_id_selector')])
async def test_clean_up_handles_alma_api_errors(
        driver, userapi, op, retry_strategy, failure_msg_maker,
        id_selector, cleanup_ids):

    err = BaseAlmaApiError('Boom!')
    userapi.get_user.side_effect = err
    retry_strategy.can_retry.return_value = False, sentinel.err
    failure_msg_maker.message_from_exception.return_value = sentinel.msg

    await driver._clean_up()

    retry_strategy.can_retry.assert_called_once_with(op, err)
    failure_msg_maker.message_from_exception.assert_called_once_with(
        sentinel.err)

    assert op.state == 'failed'
    assert op.failure_message == sentinel.msg


@pytest.mark.parametrize('op', [pytest.lazy_fixture('dirty_op')])
@pytest.mark.parametrize('id_selector',
                         [pytest.lazy_fixture('cleanup_id_selector')])
async def test_clean_up_retries_according_to_retry_strategy(
        driver, userapi, op, retry_strategy, failure_msg_maker,
        id_selector, cleanup_ids):

    err = BaseAlmaApiError('Boom!')
    userapi.get_user.side_effect = err
    retry_strategy.can_retry.return_value = True, None

    await driver._clean_up()

    assert op.state == 'dirty'
    failure_msg_maker.message_from_exception.assert_not_called()


async def test_run():
    driver = MagicMock(spec=UpdateAPIOperationDriver)
    driver._run_one.return_value = sentinel

    assert (await UpdateAPIOperationDriver.run(driver)) == sentinel
    assert driver.mock_calls == [call._run_one()]


@pytest.mark.parametrize('state, method', [
    ('init', '_initialise'),
    ('attempting', '_attempt'),
    ('dirty', '_clean_up')
])
async def test_run_one(state, method):
    driver = MagicMock(spec=UpdateAPIOperationDriver)
    driver._state = MagicMock()
    driver._state.state = state

    await UpdateAPIOperationDriver._run_one(driver)
    getattr(driver, method).assert_called_once_with()


async def test_run_one_succeeded():
    driver = MagicMock(spec=UpdateAPIOperationDriver)
    driver._state = MagicMock()
    driver._state.state = 'succeeded'
    driver._state.updated_user = sentinel.user

    assert await UpdateAPIOperationDriver._run_one(driver) == sentinel.user


async def test_run_one_failed():
    driver = MagicMock(spec=UpdateAPIOperationDriver)
    driver._state = MagicMock()
    driver._state.state = 'failed'
    driver._state.failure_message = sentinel.failure_msg

    with pytest.raises(ApiOperationFailedError) as excinfo:
        await UpdateAPIOperationDriver._run_one(driver)

    assert excinfo.value.args == ('Failed to update user', sentinel.failure_msg)


@pytest.fixture()
def wal_reader_messages():
    return []


@pytest.fixture()
def wal_reader(wal_reader_messages):
    async def async_gen_messages():
        for message in wal_reader_messages:
            yield message
    return async_gen_messages()


@pytest.fixture()
def replay_driver(op, wal_reader):
    return UpdateAPIOperationReplayDriver(op, wal_reader)


@pytest.mark.parametrize('op', [pytest.lazy_fixture('init_op')])
def test_replay_driver_handles_before_attempting_barrier(
        op, replay_driver):

    replay_driver._handle_before_attempting({'fingerprint_id': 'blah'})
    assert op.fingerprint_id == 'blah'
    assert op.state == 'dirty'


@pytest.mark.parametrize('op', [
    pytest.lazy_fixture('attempting_op'),
    pytest.lazy_fixture('dirty_op'),
    pytest.lazy_fixture('failed_op'),
    pytest.lazy_fixture('succeeded_op')
])
def test_replay_driver_rejects_before_attempting_barrier_in_incorrect_state(
        op, replay_driver):

    with pytest.raises((IllegalStateError, MachineError)):
        replay_driver._handle_before_attempting({'fingerprint_id': 'blah'})


@pytest.mark.parametrize('op', [
    pytest.lazy_fixture('attempting_op'),  # Note this never occurs in practice
    pytest.lazy_fixture('dirty_op')
])
def test_replay_driver_handles_increment_barrier(
        op, replay_driver):

    initial_state = op.state
    assert op.request_count == 0

    replay_driver._handle_increment_request_count({})

    assert op.state == initial_state
    assert op.request_count == 1


@pytest.mark.parametrize('op', [
    pytest.lazy_fixture('init_op'),
    pytest.lazy_fixture('failed_op'),
    pytest.lazy_fixture('succeeded_op')
])
def test_replay_driver_rejects_increment_barrier_in_incorrect_state(
        op, replay_driver):

    with pytest.raises((IllegalStateError, MachineError)):
        replay_driver._handle_increment_request_count({})


@pytest.mark.parametrize('op', [
    pytest.lazy_fixture('attempting_op'),
    pytest.lazy_fixture('dirty_op')
])
def test_replay_driver_handles_failed_barrier(
        op, replay_driver):

    replay_driver._handle_failed({'failure_message': 'blah'})
    assert op.failure_message == 'blah'
    assert op.state == 'failed'


@pytest.mark.parametrize('op', [
    pytest.lazy_fixture('init_op'),
    pytest.lazy_fixture('failed_op'),
    pytest.lazy_fixture('succeeded_op')
])
def test_replay_driver_rejects_failed_barrier_in_incorrect_state(
        op, replay_driver):

    with pytest.raises((IllegalStateError, MachineError)):
        replay_driver._handle_failed({'failure_message': 'blah'})


@pytest.mark.parametrize('op', [
    pytest.lazy_fixture('attempting_op'),
    pytest.lazy_fixture('dirty_op')
])
def test_replay_driver_handles_succeeded_barrier(
        op, replay_driver):

    user = {'foo': 'bar'}
    replay_driver._handle_succeeded({'alma_user': user})
    assert op.updated_user == user
    assert op.state == 'succeeded'


@pytest.mark.parametrize('op', [
    pytest.lazy_fixture('init_op'),
    pytest.lazy_fixture('failed_op'),
    pytest.lazy_fixture('succeeded_op')
])
def test_replay_driver_rejects_failed_barrier_in_incorrect_state_2(
        op, replay_driver):

    with pytest.raises((IllegalStateError, MachineError)):
        replay_driver._handle_succeeded({'alma_user': {'foo': 'bar'}})


@pytest.mark.parametrize('wal_reader_messages, op, final_state, req_count, '
                         'fingerprint_id, err_msg, user_obj',
[  # noqa: E128
    ([], pytest.lazy_fixture('init_op'), 'init', 0, None, None, None),
    ([{'barrier': 'before-attempting', 'fingerprint_id': '123'}
      ], pytest.lazy_fixture('init_op'), 'dirty', 0, '123', None, None),
    ([{'barrier': 'before-attempting', 'fingerprint_id': '456'},
      {'barrier': 'increment-request-count'},
      {'barrier': 'increment-request-count'}
      ], pytest.lazy_fixture('init_op'), 'dirty', 2, '456', None, None),
    ([{'barrier': 'before-attempting', 'fingerprint_id': '456'},
      {'barrier': 'increment-request-count'},
      {'barrier': 'increment-request-count'},
      {'barrier': 'succeeded', 'alma_user': {'abc': 'def'}},
      ], pytest.lazy_fixture('init_op'), 'succeeded', 2, '456', None,
     {'abc': 'def'}),
    ([{'barrier': 'before-attempting', 'fingerprint_id': '456'},
      {'barrier': 'increment-request-count'},
      {'barrier': 'increment-request-count'},
      {'barrier': 'failed', 'failure_message': 'oh dear'},
      ], pytest.lazy_fixture('init_op'), 'failed', 2, '456', 'oh dear',
     None)
])
async def test_replay_driver_run(op, replay_driver, wal_reader, final_state,
                                 req_count, fingerprint_id, err_msg, user_obj):

    await replay_driver.run()

    assert op.state == final_state
    assert req_count == op.request_count
    assert fingerprint_id == op.fingerprint_id
    assert user_obj == op.updated_user
    assert err_msg == op.failure_message
