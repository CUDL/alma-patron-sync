from unittest.mock import sentinel

import pytest
from transitions import Machine

from almausersync.states import requirestate, IllegalStateError


@pytest.fixture()
def model():
    class TestModel:
        def __init__(self):
            self.machine = Machine(model=self, states='a b c'.split(), initial='a')

        @requirestate('a')
        def only_in_a(self):
            return sentinel

        @requirestate('a', 'b')
        def only_in_a_or_b(self):
            return sentinel
    return TestModel()


@pytest.mark.parametrize('state, action', [
    ('a', 'only_in_a'),
    ('a', 'only_in_a_or_b'),
    ('b', 'only_in_a_or_b')
])
def test_requirestate_allows_calls_in_matching_state(model, state, action):
    model.trigger(f'to_{state}')
    assert model.state == state
    getattr(model, action)()


@pytest.mark.parametrize('state, action', [
    ('b', 'only_in_a'),
    ('c', 'only_in_a_or_b')
])
def test_requirestate_raises_illegalstateerror_if_state_is_wrong(model, state,
                                                                 action):
    model.trigger(f'to_{state}')
    assert model.state == state

    with pytest.raises(IllegalStateError):
        getattr(model, action)()
