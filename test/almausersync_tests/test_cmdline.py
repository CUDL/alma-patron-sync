import aiofiles
import pytest

from almausersync.cmdline import estimate_record_count


@pytest.mark.asyncio
async def test_estimate_record_count(datadir):
    file = await aiofiles.open(datadir / 'multiline.txt')

    assert await estimate_record_count(file) == 4
