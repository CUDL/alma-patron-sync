from io import StringIO

import aiofiles
import pytest

from almausersync.async_utils import anext
from almausersync.jsonstreams import (
    dump_incremental_async, load_async, FinalLineTruncatedJsonStreamError,
    JsonInvalidJsonStreamError)


@pytest.fixture()
def json_stream():
    return '''\
{}\n\
{"abc":"def"}\n\
[]\n'''


@pytest.fixture()
def json_stream_objects():
    return [
        {},
        {'abc': 'def'},
        []
    ]


@pytest.mark.usefixtures('aiofiles_support_stringio')
@pytest.mark.asyncio
async def test_load_async(json_stream_objects, json_stream):
    sio = StringIO(json_stream)

    async with aiofiles.open(sio) as f:
        objects = []
        async for obj in load_async(f):
            objects.append(obj)

        assert objects == json_stream_objects


@pytest.mark.usefixtures('aiofiles_support_stringio')
@pytest.mark.asyncio
async def test_load_async_rejects_lines_containing_invalid_json():
    input = StringIO('{}\n}\n')

    async with aiofiles.open(input) as f:
        items = load_async(f)
        assert await anext(items) == {}

        with pytest.raises(JsonInvalidJsonStreamError) as exc_info:
            await anext(items)

        assert str(exc_info.value) == 'line doesn\'t contain valid JSON'


@pytest.mark.usefixtures('aiofiles_support_stringio')
@pytest.mark.asyncio
async def test_load_async_rejects_no_newline_at_end_of_stream():
    input = StringIO('{}\n{}')

    async with aiofiles.open(input) as f:
        items = load_async(f)
        assert await anext(items) == {}

        with pytest.raises(FinalLineTruncatedJsonStreamError) as exc_info:
            await anext(items)

        assert str(exc_info.value) == 'line doesn\t end with \\n'


@pytest.mark.usefixtures('aiofiles_support_stringio')
@pytest.mark.asyncio
async def test_dump_incremental_async(json_stream_objects, json_stream):
    sio = StringIO()

    async with aiofiles.open(sio) as f:
        dumper = await dump_incremental_async(f)

        for o in json_stream_objects:
            await dumper.asend(o)

        assert sio.getvalue() == json_stream
