import pytest
from yarl import URL

from almausersync.almarest import AlmaApiConfig, Session, UserApiClient

__all__ = ('api_key', 'api_base_url', 'api_config', 'alma_session',
           'user_client')


@pytest.fixture()
def api_key():
    return 'notarealkeynotarealkeynotarealkeynot'


@pytest.fixture()
def api_base_url(har_entry_client):
    server = har_entry_client.server
    return URL.build(scheme=server.scheme, host=server.host, port=server.port)


@pytest.fixture()
def api_config(api_key, api_base_url):
    return AlmaApiConfig(api_base_url, api_key)


@pytest.fixture()
def alma_session(api_config, har_entry_client):
    return Session(api_config, har_entry_client.session)


@pytest.fixture()
def user_client(alma_session):
    return UserApiClient(alma_session)
