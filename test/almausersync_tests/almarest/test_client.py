# coding=utf-8
from unittest.mock import MagicMock

import pytest
from aiohttp import web

from almausersync.almarest import (
    AlmaApiConfig, AlmaApiAuth, ApiResponseErrorHandler, TemporaryAlmaApiError,
    HardAlmaApiError)
from almausersync.almarest import constants as const


@pytest.mark.parametrize('key', [
    'abc^',
    'foo=bar'
])
def test_alma_api_auth_rejects_invalid_apikey(key):
    with pytest.raises(ValueError):
        AlmaApiAuth(key)


def test_alma_api_auth_applies_key_to_request():
    req = MagicMock()
    key = 'hkgcy7dpuo6mdn0pa5omlemtyw32oelpixng'
    auth = AlmaApiAuth('hkgcy7dpuo6mdn0pa5omlemtyw32oelpixng')

    auth(req)

    assert req.__setitem__.called_once_with('Authorization', f'apikey {key}')


over_per_day_threshold_xml = '''\
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<web_service_result xmlns="http://com/exlibris/urm/general/xmlbeans">
  <errorsExist>true</errorsExist>
  <errorList>
    <error>
      <errorCode>DAILY_THRESHOLD</errorCode>
      <errorMessage>Daily API Request Threshold has been reached. For details see: https://developers.exlibrisgroup.com/alma/apis#threshold</errorMessage>
      <reason></reason>
    </error>
  </errorList>
</web_service_result>
'''.encode('utf-8')

over_per_second_threshold_xml = '''\
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<web_service_result xmlns="http://com/exlibris/urm/general/xmlbeans">
  <errorsExist>true</errorsExist>
  <errorList>
    <error>
      <errorCode>PER_SECOND_THRESHOLD</errorCode>
      <errorMessage>HTTP requests are more than allowed per second. See https://goo.gl/2x1c1M </errorMessage>
    </error>
  </errorList>
</web_service_result>
'''.encode('utf-8')


@pytest.fixture()
def threshold_response(xml_bytes, content_type):
    return web.Response(
        status=429, body=xml_bytes,
        content_type=content_type, charset='UTF-8')


@pytest.mark.asyncio
@pytest.mark.parametrize('server_response', [
    pytest.lazy_fixture('threshold_response')
])
@pytest.mark.parametrize('content_type', [
    'application/xml',
    'text/plain'
])
@pytest.mark.parametrize('xml_bytes,code', [
    (over_per_day_threshold_xml, const.ERR_THRESHOLD_DAILY),
    (over_per_second_threshold_xml, const.ERR_THRESHOLD_QPS)
])
@pytest.mark.usefixtures('threshold_response')
async def test_error_handler_detects_threshold_response(
        code, client_response):

    with pytest.raises(TemporaryAlmaApiError) as excinfo:
        await ApiResponseErrorHandler()(client_response)

    assert excinfo.value.error_code == code.code


@pytest.mark.asyncio
@pytest.mark.parametrize('har_file,code', [
    ('user_post_err_401851_id_already_in_use.har', const.ErrorCode('401851'))
])
async def test_error_handler_handles_json_response(code, har_entry_response):
    with pytest.raises(HardAlmaApiError) as excinfo:
        await ApiResponseErrorHandler()(har_entry_response)

    assert excinfo.value.error_code == code.code


def test_api_config_from_params_rejects_region_and_baseurl_together():
    with pytest.raises(ValueError) as excinfo:
        AlmaApiConfig.from_params(
            apikey='abc', region='cn', base_url='https://example.com')

    assert str(excinfo.value) == 'both region and base_url were specified'


def test_api_config_from_params_rejects_relative_base_url():
    with pytest.raises(ValueError) as excinfo:
        AlmaApiConfig('./foo', 'apikey')

    assert str(excinfo.value) == 'base_url was not absolute: ./foo'


@pytest.mark.parametrize('opts, apikey_value, base_url_value', [
    ({'region': 'ca', 'apikey': 'keyvalue1'}, 'keyvalue1', 'https://api-ca.hosted.exlibrisgroup.com/'),
    ({'region': 'canada', 'apikey': 'keyvalue2'}, 'keyvalue2', 'https://api-ca.hosted.exlibrisgroup.com/'),
    ({'base_url': 'https://example.com/', 'apikey': 'keyvalue3'}, 'keyvalue3', 'https://example.com/')
])
def test_api_config_from_params(opts, apikey_value, base_url_value):
    config = AlmaApiConfig.from_params(**opts)

    assert config.apikey == apikey_value
    assert str(config.base_url) == base_url_value
