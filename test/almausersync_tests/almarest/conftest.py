import asyncio

from aiohttp import web
import attr
import gzip
import json
import pytest
from pytest_lazyfixture import is_lazy_fixture

from almausersync_tests.helpers.har import HarFile
from almausersync_tests.almarest.client_fixtures import *  # noqa: F401


# pytest-asyncio uses event_loop, pytest-aiohttp uses loop - override to ensure
# they both use the same loop.
@pytest.fixture()
def loop(event_loop):
    return event_loop


@pytest.fixture()
def server_response_method():
    return 'GET'


@pytest.fixture()
def server_response_path():
    return '/'


@pytest.fixture()
async def client_response(request, server_response, server_response_method,
                          server_response_path, test_client):
    """This fixture produces an aiohttp.ClientResponse reflecting the value of
    the `server_response` fixture. The `server_response` fixture needs
    overriding in a test module or test function via parametrize.
    """
    async def handler(req):
        if is_lazy_fixture(server_response):
            return request.getfixturevalue(server_response.name)
        return server_response
    app = web.Application()
    app.router.add_route(server_response_method, server_response_path, handler)
    client = await test_client(app)
    return await client.request(server_response_method, server_response_path)


@pytest.fixture()
def server_response():
    """Placeholder to fail fast if someone forgets to specify the
    server_response fixture when using client_response.
    """
    raise NotImplementedError(
        'No server_response fixture defined in test module.')


@pytest.fixture()
async def har_entry_app(request, har_entry, har_app_request,
                        har_app_response,
                        # These two are loaded for their side-effects (but
                        # mark.usefixtures doesn't seem to work on fixtures)
                        validate_har_app_request, validate_har_app_response):

    if is_lazy_fixture(har_entry):
        har_entry = request.getfixturevalue(har_entry.name)

    async def handler(req):
        # Have to read bytes in the handler (to cache them), otherwise large
        # bodies can't be accessed later via the har_app_request future.
        await req.read()

        har_app_request.set_result(req)

        try:
            # Store the HTTP response in a Future so that we can access it
            # elsewhere. This allows us to handle exceptions (as otherwise the
            # pytest doesnt see them as the web server handles them).
            resp = har_entry.response.as_aiohttp_response()
            har_app_response.set_result(resp)
            return resp
        except Exception as e:
            har_app_response.set_exception(e)
            raise

    har_req = har_entry.request
    app = web.Application()
    app.router.add_route(har_req.method, har_req.url.path, handler)
    return app


@pytest.fixture()
async def har_app_response(loop):
    return loop.create_future()


@pytest.fixture()
async def har_app_request(loop):
    """
    An asyncio.Future which resolves to the request that the har_entry_app
    receives from the har_entry_client.
    """
    return loop.create_future()


def _get_comparator(mime_type):
    async def compare(har_req, req):
        if mime_type == 'application/json':
            assert har_req.body.mime_type == mime_type
            # assert req.can_read_body
            assert req.content_type == mime_type
            har_req_json = json.loads(har_req.body.text)
            req_json = await req.json()
            assert har_req_json == req_json
        else:
            pytest.fail(f'No comparator implemented for {mime_type}')
    return compare


@pytest.fixture()
def validate_har_app_request(har_entry, har_app_request, har_validation):
    """
    Register a validator which asserts that the request received by the har_app
    """
    async def validate_har_request():
        req = await har_app_request

        har_req = har_entry.request
        assert req.method == har_req.method
        assert req.path_qs == har_req.url.path_qs
        for header in har_req.headers.values():
            assert header.name in req.headers
            assert header.raw_value in req.headers.getall(header.name)

        if har_req.body:
            await _get_comparator(har_req.body.mime_type)(har_req, req)

    har_validation.add_validator(validate_har_request())


@pytest.fixture()
def validate_har_app_response(har_app_response, har_validation):
    async def validate_har_app_response():
        # All this does is throw the exception set on the har_app_response
        # future if generating it failed (otherwise the exception is caught
        # by the test server and pytest never sees it).
        await har_app_response

    har_validation.add_validator(validate_har_app_response())


@pytest.fixture()
async def har_validation():
    @attr.s
    class Validator:
        validators = attr.ib(default=attr.Factory(list))
        _started = attr.ib(default=False)

        def add_validator(self, validator):
            if self._started:
                raise RuntimeError(
                    'Can\'t add validator after validation has begun')
            self.validators.append(validator)

        async def validate(self):
            self._started = True

            return await asyncio.gather(*self.validators)
    validator = Validator()
    yield validator

    await validator.validate()


@pytest.fixture()
async def har_entry_client(har_entry_app, test_client):
    return await test_client(har_entry_app)


@pytest.fixture()
async def har_entry_response(har_entry, har_entry_client):
    har_req = har_entry.request
    return await har_entry_client.request(har_req.method, har_req.url.path)


@pytest.fixture()
def har_entry(datadir, har_file):
    if har_file.endswith('.gz'):
        text = gzip.decompress((datadir / har_file).read_bytes()).decode()
    else:
        text = (datadir / har_file).read_text()

    return HarFile.from_string(text).singular_entry
