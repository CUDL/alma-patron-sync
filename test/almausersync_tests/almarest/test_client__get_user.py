import pytest

from almausersync.almarest import HardAlmaApiError
from almausersync.almarest import constants


@pytest.mark.parametrize('har_file', ['success.har'])
async def test_successful_request(user_client):
    user = await user_client.get_user('hwtb2_test@cam.ac.uk')

    assert user['primary_id'] == 'hwtb2_test@cam.ac.uk'


@pytest.mark.parametrize('har_file, err_code, user_id, id_type', [
    ('err_401861__id_doesnt_exist.har',
     constants.ErrorCodes.USER_ID_DOESNT_EXIST.value,
     'notauser',
     None),
    ('err_401862__id_type_doesnt_exist.har',
     constants.ErrorCodes.ID_TYPE_DOESNT_EXIST.value,
     'hwtb2@cam.ac.uk',
     '01'),
    ('err_401890__id_doesnt_exist_with_type.har',
     constants.ErrorCodes.USER_ID_DOESNT_EXIST_WITH_TYPE.value,
     'hwtb2@cam.ac.uk',
     'FACEBOOK'),
    ('err_ROUTING_ERROR__alma_down_for_maintenance.har',
     constants.ErrorCodes.ROUTING_ERROR.value,
     '3263342420003606',
     None)
])
async def test_unsuccessful_request(user_client, err_code, user_id, id_type):
    with pytest.raises(HardAlmaApiError) as excinfo:
        await user_client.get_user(user_id, id_type=id_type)

    assert excinfo.value.error_code == err_code
