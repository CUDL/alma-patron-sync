import pytest

from almausersync.almarest import HardAlmaApiError
from almausersync.almarest import constants


@pytest.mark.parametrize('har_file', ['ok__user_deleted.har'])
async def test_successful_request(user_client):
    assert await user_client.delete_user('someuser') is None


@pytest.mark.parametrize('har_file, err_code, user_id, id_type', [
    ('err_401861__user_id_doesnt_exist.har',
     constants.ErrorCodes.USER_ID_DOESNT_EXIST.value,
     '55477BAB-8D9E-4E8B-8C44-9A8043F0CB9D',
     None),
    ('err_401890__user_id_doesnt_have_type.har',
     constants.ErrorCodes.USER_ID_DOESNT_EXIST_WITH_TYPE.value,
     'someuser',
     'GOOGLE'),

])
async def test_unsuccessful_request(user_client, err_code, user_id, id_type):
    with pytest.raises(HardAlmaApiError) as excinfo:
        await user_client.delete_user(user_id, id_type=id_type)

    assert excinfo.value.error_code == err_code
