import json

import pytest

from almausersync.almarest import HardAlmaApiError
from almausersync.almarest import constants


@pytest.fixture()
def har_request_json(har_entry):
    assert har_entry.request.body.mime_type == 'application/json'
    return json.loads(har_entry.request.body.text)


@pytest.fixture()
def har_response_json(har_entry):
    assert har_entry.response.content.mime_type == 'application/json'
    return json.loads(har_entry.response.content.text)


@pytest.mark.parametrize('har_file', ['ok__minimal_user.har'])
async def test_successful_request(user_client, har_request_json,
                                  har_response_json):

    user = await user_client.create_user(har_request_json)

    assert har_response_json == user


@pytest.mark.parametrize('har_file, err_code, err_cls', [
    ('err_401664__missing_required_field.har',
     constants.ErrorCodes.MISSING_REQUIRED_FIELD.value,
     HardAlmaApiError),
    ('err_401854__missing_external_id.har',
     constants.ErrorCodes.MISSING_EXTERNAL_USER_FIELD.value,
     HardAlmaApiError),
    ('err_401851__id_already_in_use.har',
     constants.ErrorCodes.USER_ID_ALREADY_EXISTS.value,
     HardAlmaApiError)
])
async def test_unsuccessful_request(user_client, err_code, err_cls,
                                    har_request_json):
    with pytest.raises(err_cls) as excinfo:
        await user_client.create_user(har_request_json)

    assert excinfo.value.error_code == err_code
