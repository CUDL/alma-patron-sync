import json

import pytest

from almausersync.almarest import HardAlmaApiError, TemporaryAlmaApiError
from almausersync.almarest import constants


@pytest.fixture()
def har_request_json(har_entry):
    assert har_entry.request.body.mime_type == 'application/json'
    return json.loads(har_entry.request.body.text)


@pytest.fixture()
def har_response_json(har_entry):
    assert har_entry.response.content.mime_type == 'application/json'
    return json.loads(har_entry.response.content.text)


@pytest.mark.parametrize('har_file', ['ok__disable_cam_patron_role.har'])
async def test_update_user(user_client, har_request_json, har_response_json):
    user = await user_client.update_user(har_request_json, override=())

    assert user == har_response_json


@pytest.mark.parametrize('har_file, overrides', [
    ('ok__update_no_overrides.har', ()),
    ('ok__update_with_overrides.har', (constants.UserOverride.USER_TITLE,
                                       constants.UserOverride.USER_GROUP))
])
async def test_update_user_with_overrides(user_client, overrides,
                                          har_request_json, har_response_json):

    user = await user_client.update_user(har_request_json,
                                         override=overrides)
    assert user == har_response_json


@pytest.mark.parametrize('har_file', [
    'err_4018994__move_primary_id_to_secondary.har',
    'err_4018994__move_primary_id_to_inactive_secondary.har'
])
async def test_one_does_not_simply_move_an_id(user_client, har_request_json):

    with pytest.raises(HardAlmaApiError) as excinfo:
        await user_client.update_user(har_request_json,
                                      previous_id='3263342420003606',
                                      override=())

    # Note that the ID value is *not* actually duplicated, but Alma thinks it
    # is. The ID was the primary ID and gets moved to secondary.
    assert (excinfo.value.error_code ==
            constants.ErrorCodes.ID_DUPLICATED_IN_REQUEST.value)


@pytest.mark.parametrize('har_file, err_code, err_cls, prev_id, prev_id_type',
[  # noqa: E128
    ('err_401890__id_doesnt_have_type.har',
     constants.ErrorCodes.USER_ID_DOESNT_EXIST_WITH_TYPE.value,
     HardAlmaApiError,
     '3263342420003606', 'GOOGLE')

])
async def test_unsuccessful_request_with_id_override(
        user_client, err_code, err_cls, prev_id, prev_id_type,
        har_request_json):

    with pytest.raises(err_cls) as excinfo:
        await user_client.update_user(har_request_json, previous_id=prev_id,
                                      prev_id_type=prev_id_type, override=())

    assert excinfo.value.error_code == err_code


@pytest.mark.parametrize('har_file, err_code, err_cls', [
    ('err__routing_error_on_normal_req.har',
     constants.ErrorCodes.ROUTING_ERROR.value,
     HardAlmaApiError),
    # The API erroneously responds with 429 for requests > 256KiB
    ('err_PER_SECOND_THRESHOLD__spurious.har.gz',
     constants.ErrorCodes.ERR_THRESHOLD_QPS.value,
     TemporaryAlmaApiError),

])
async def test_unsuccessful_request(user_client, err_code, err_cls,
                                    har_request_json):
    with pytest.raises(err_cls) as excinfo:
        await user_client.update_user(har_request_json, override=())

    assert excinfo.value.error_code == err_code
