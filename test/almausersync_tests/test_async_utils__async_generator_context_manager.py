from unittest.mock import MagicMock, sentinel
import pytest

from almausersync.async_utils import AsyncGeneratorContextManager


@pytest.fixture()
def mock():
    return MagicMock()


@pytest.fixture()
def generator_context_manager(mock):
    async def context_manager():
        mock.enter()
        yield sentinel
        mock.exit()

    return AsyncGeneratorContextManager(context_manager())


@pytest.mark.asyncio
async def test_enter_runs_generator_up_to_yield(
        mock, generator_context_manager):

    mock.enter.assert_not_called()
    mock.exit.assert_not_called()

    async with generator_context_manager as value:
        assert value is sentinel

        mock.enter.assert_called_once_with()
        mock.exit.assert_not_called()

    mock.enter.assert_called_once_with()
    mock.exit.assert_called_once_with()
