import pytest

from almausersync.exceptions import InvalidUserWrapper, InvalidUserError
from almausersync.utils import (get, get_original_ids, IdSelectionStrategy,
                                filter_ids, get_new_ids, validate_ids)


@pytest.fixture()
def sample_map():
    return {
        1: 'abc'
    }


@pytest.mark.parametrize('args, kwargs, val', [
    ((1,), dict(), 'abc',),
    ((1,), dict(type=None), 'abc'),
    ((1,), dict(type=str), 'abc'),
    ((1,), dict(type=int), None),
    ((1,), dict(type=int, default=False), False),
    ((1, False, int), dict(), False),

    ((0,), dict(), None),
    ((0,), dict(default=1), 1),
    ((0,), dict(type=None), None),
    ((0,), dict(type=str), None),
    ((0,), dict(type=str, default=1), 1),
    ((0, 1, str), dict(), 1),
])
def test_get(sample_map, args, kwargs, val):
    assert get(sample_map, *args, **kwargs) == val


@pytest.fixture()
def user_1_id():
    return {
        'primary_id': '__new__'
    }


@pytest.fixture()
def user_several_ids():
    return {
        'primary_id': '__new__',
        'user_identifier': [
            {
                'value': '123',
                'id_type': {'value': '01'},
                'status': 'ACTIVE'
            },
            {
                'value': '456',
                'id_type': {'value': '02'},
                'status': 'INACTIVE'
            },
            {
                'value': 'hi',
                'id_type': {'value': 'FOOBAR'},
                'status': 'ACTIVE'
            }
        ]
    }


@pytest.fixture()
def wrapped_user_several_ids(user_several_ids):
    return {
        'alma_user': user_several_ids
    }


@pytest.fixture()
def user_wrapper(user_1_id):
    return {
        'alma_user': user_1_id,
        'original_ids': [
            {'type': 'primary', 'value': 'abc', 'is_active': True},
            {'type': 'primary', 'value': 'def'},
            {'type': 'foo', 'value': 'ghi', 'is_active': True},
            {'type': 'bar', 'value': '123', 'is_active': False}
        ]
    }


def test_get_original_ids(user_wrapper):
    assert get_original_ids(user_wrapper) == [
        ('primary', 'abc', True),
        ('primary', 'def', True),
        ('foo', 'ghi', True),
        ('bar', '123', False)
    ]


@pytest.mark.parametrize('user', [
    pytest.lazy_fixture('user_several_ids'),
    pytest.lazy_fixture('wrapped_user_several_ids')
])
def test_get_new_ids(user):
    assert get_new_ids(user) == [
        ('primary', '__new__', True),
        ('01', '123', True),
        ('02', '456', False),
        ('FOOBAR', 'hi', True)
    ]


@pytest.fixture()
def user_wrapper_with_no_orig_primary(user_1_id):
    return {
        'alma_user': user_1_id,
        'original_ids': [
            {'type': 'foo', 'value': 'ghi', 'is_active': True},
            {'type': 'bar', 'value': '123', 'is_active': False}
        ]
    }


@pytest.fixture()
def id_selector():
    return IdSelectionStrategy()


@pytest.mark.parametrize('uw, id_type, id_value', [
    (pytest.lazy_fixture('user_wrapper'), 'primary', 'abc'),
    (pytest.lazy_fixture('user_wrapper_with_no_orig_primary'), 'foo', 'ghi')
])
def test_id_selection_for_update(id_selector, uw, id_type, id_value):
    assert id_selector.get_id_for_update(uw) == (id_type, id_value)


@pytest.mark.parametrize('wrapper', [
    {},
    {'original_ids': []},
    {'original_ids': [
        {'type': 'bar', 'value': '123', 'is_active': False}
    ]}
])
def test_update_id_selection_fails_when_wrapper_has_no_orig_ids(
        id_selector, wrapper, user_1_id):
    # Ensure the wrapped user has an ID
    wrapper['alma_user'] = user_1_id

    with pytest.raises(InvalidUserWrapper) as excinfo:
        id_selector.get_id_for_update(wrapper)

    assert 'user_wrapper has no active ' in str(excinfo.value)


@pytest.mark.parametrize('ids, active, result', [
    ([], True, []),
    ([], False, []),
    ([('a', 'b', True), ('x', 'y', False)], True, [('a', 'b')]),
    ([('a', 'b', True), ('x', 'y', False)], False, [('x', 'y')]),
])
def test_filter_ids(ids, active, result):
    assert filter_ids(ids, active=active) == result


@pytest.mark.parametrize('user_wrapper, msg', [
    ({'alma_user': {'primary_id': '123'},
      'original_ids': []},
     'Old user state has no active IDs'),
    ({'alma_user': {},
      'original_ids': [{'type': 'primary', 'value': '123',
                        'is_active': True}]},
     'New user state has no active IDs')
])
def test_get_ids_for_update_clean_up_rejects_users_without_ids(
        id_selector, user_wrapper, msg):

    with pytest.raises(ValueError) as excinfo:
        id_selector.get_ids_for_update_clean_up(user_wrapper)

    assert msg == str(excinfo.value)


@pytest.fixture()
def ids_for_clean_up_user__matching_primary_ids():
    return {
        'alma_user': {'primary_id': 'foo'},
        'original_ids': [
            {'type': 'primary', 'value': 'foo', 'is_active': True}]}


@pytest.fixture()
def ids_for_clean_up_user__distinct_primary_ids():
    return {
        'alma_user': {'primary_id': 'abc'},
        'original_ids': [
            {'type': 'primary', 'value': '123', 'is_active': True}]}


@pytest.fixture()
def ids_for_clean_up_user__matching_new_secondary_and_old_primary():
    return {
        'alma_user': {
            'primary_id': '__new__',
            'user_identifier': [
                {'value': 'foo', 'id_type': {'value': '01'},
                 'status': 'ACTIVE'}]},
        'original_ids': [
            {'type': 'primary', 'value': 'foo', 'is_active': True}]}


@pytest.mark.parametrize('user_wrapper, ids', [
    (pytest.lazy_fixture('ids_for_clean_up_user__matching_primary_ids'),
     [('primary', 'foo')]),
    (pytest.lazy_fixture('ids_for_clean_up_user__distinct_primary_ids'),
     [('primary', '123'), ('primary', 'abc')]),
    (pytest.lazy_fixture(
        'ids_for_clean_up_user__matching_new_secondary_and_old_primary'),
     [(None, 'foo')])
])
def test_get_ids_for_update_clean_up(id_selector, user_wrapper, ids):
    assert ids == id_selector.get_ids_for_update_clean_up(user_wrapper)


def _user_with_ids(*ids, primary_id='foo'):
    if ids is not OMITTED:
        ids = list(ids)

    return {
        k: v for k, v in [('primary_id', primary_id),
                          ('user_identifier', ids)]
        if v is not OMITTED
    }


OMITTED = object()


def _id_type(value='BARCODE', desc=OMITTED):
    return {
        k: v for k, v in [('value', value), ('desc', desc)] if v is not OMITTED
    }


def _additional_id(value='abc', note=OMITTED, status=OMITTED,
                   id_type=_id_type(), segment_type=OMITTED):
    return {
        k: v for k, v in [
            ('value', value),
            ('note', note),
            ('status', status),
            ('id_type', id_type),
            ('segment_type', segment_type),
            ('status', status),
        ]
        if v is not OMITTED
    }


@pytest.mark.parametrize('user, err_snippets', [
    ({'primary_id': ''}, ['User\'s primary_id is invalid',
                          'id has no value']),
    ({'primary_id': None}, ['User\'s primary_id is invalid',
                            'NoneType is not str']),
    ({}, ['User\'s primary_id is invalid', 'NoneType is not str']),
    ({'primary_id': 'a' * 256}, [
        'User\'s primary_id is invalid',
        'id was longer than the limit of 255 characters: 256']),

    (_user_with_ids(_additional_id(value=123)),
     ['Invalid value', 'int is not str']),
    (_user_with_ids(_additional_id(value=OMITTED)),
     ['Invalid value', 'NoneType is not str']),
    (_user_with_ids(_additional_id(value='')),
     ['Invalid value', 'id has no value']),
    (_user_with_ids(_additional_id(value='a' * 256)),
     ['Invalid value',
      'id was longer than the limit of 255 characters: 256']),

    (_user_with_ids(_additional_id(note=123)),
     ['Invalid note', 'int is not str']),
    (_user_with_ids(_additional_id(note='a' * 2001)),
     ['Invalid note', 'Value longer than 2000']),

    (_user_with_ids(_additional_id(status=123)),
     'Unexpected status: 123'),
    (_user_with_ids(_additional_id(status='a')),
     'Unexpected status: \'a\''),

    (_user_with_ids(_additional_id(segment_type=123)),
     'Unexpected segment_type: 123'),
    (_user_with_ids(_additional_id(segment_type='FOO')),
     'Unexpected segment_type: \'FOO\''),

    (_user_with_ids(_additional_id(id_type=None)),
     'id_type is not an object'),

    (_user_with_ids(_additional_id(id_type=_id_type(desc=123))),
     ['Invalid id_type', 'int is not str']),
    (_user_with_ids(_additional_id(id_type=_id_type(desc='a' * 4001))),
     ['Invalid id_type', 'desc was longer than 4000: 4001']),

    (_user_with_ids(_additional_id(id_type={'foo': 'bar', 'value': '00'})),
     'id_type contains unexpected keys: {\'foo\'}'),
    (_user_with_ids(_additional_id(id_type=_id_type(value=None))),
     ['Invalid id_type', 'NoneType is not str']),
    (_user_with_ids(_additional_id(id_type=_id_type(value=''))),
     ['Invalid id_type', 'Unexpected type code: \'\'']),
    (_user_with_ids(_additional_id(id_type=_id_type(value='0123'))),
     ['Invalid id_type', 'Unexpected type code: \'0123\'']),

    # additional keys
    (_user_with_ids(
        {'foo': 'bar',
         'value': 'foo',
         'id_type': {'value': '00'}}),
     ['object contains unexpected keys: {\'foo\'}']),

    # duplicates
    (_user_with_ids(
        _additional_id(value='bar'),
        _additional_id(value='bar')),
     ['User has multiple occurrences of IDs', "'bar'"]),
    (_user_with_ids(
        _additional_id(value='bar', status='INACTIVE'),
        _additional_id(value='bar', status='INACTIVE')),
     ['User has multiple occurrences of IDs', "'bar'"]),
    (_user_with_ids(
        _additional_id(value='bar', status='ACTIVE'),
        _additional_id(value='bar', status='INACTIVE')),
     ['User has multiple occurrences of IDs', "'bar'"]),
    (_user_with_ids(
        _additional_id(value='bar'),
        _additional_id(value='bar', status='INACTIVE')),
     ['User has multiple occurrences of IDs', "'bar'"]),

    (_user_with_ids(
        _additional_id(value='bar'),
        primary_id='bar'),
     ['User has multiple occurrences of IDs', "'bar'"]),

    (_user_with_ids(
        _additional_id(value='bar', status='INACTIVE'),
        primary_id='Bar'),
     ['User has case-insensitive collision between primary and additional ID '
      'values', "'bar'"])
])
def test_validate_ids_rejects_users_with_invalid_ids(user, err_snippets):
    with pytest.raises(InvalidUserError) as excinfo:
        validate_ids(user)

    snippets = ([err_snippets]
                if isinstance(err_snippets, str) else err_snippets)

    for snippet in snippets:
        assert snippet in str(excinfo.value) or (
            snippet in str(excinfo.value.__cause__))


@pytest.mark.parametrize('user', [
    _user_with_ids(),
    _user_with_ids(
        _additional_id()),
    _user_with_ids(
        _additional_id(value='bar'),
        _additional_id(value='baz')),
    {'alma_user': _user_with_ids()}
])
def test_validate_ids_accepts_users_with_valid_ids(user):
    validate_ids(user)
