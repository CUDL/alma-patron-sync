import copy

import pytest

from almausersync.transforms import IdMigrationTransform


@pytest.fixture(params=['ACTIVE', 'INACTIVE'])
def id_migration__primary_to_secondary(request):
    status = request.param
    assert status in ['ACTIVE', 'INACTIVE']
    return {
        'alma_user': {
            'primary_id': '123',
            'user_identifier': [
                {
                    'value': 'abc',
                    'status': status,
                    'id_type': {
                        'value': '02'
                    }
                }
            ]
        },
        'original_ids': [
            {'type': 'primary', 'value': 'abc', 'is_active': True}
        ]
    }


@pytest.fixture(params=['ACTIVE', 'INACTIVE'])
def id_migration__primary_to_secondary_insensitive(request):
    status = request.param
    assert status in ['ACTIVE', 'INACTIVE']
    return {
        'alma_user': {
            'primary_id': '123',
            'user_identifier': [
                {
                    'value': 'ABC',
                    'status': status,
                    'id_type': {
                        'value': '02'
                    }
                }
            ]
        },
        'original_ids': [
            {'type': 'primary', 'value': 'abc', 'is_active': True}
        ]
    }


@pytest.fixture()
def id_migration__active_secondary_to_primary():
    return {
        'alma_user': {
            'primary_id': '123',
            'user_identifier': [
                {
                    'value': 'abc',
                    'status': 'ACTIVE',
                    'id_type': {
                        'value': '02'
                    }
                }
            ]
        },
        'original_ids': [
            {'type': '02', 'value': '123', 'is_active': True}
        ]
    }


@pytest.fixture()
def id_migration__inactive_secondary_to_primary():
    return {
        'alma_user': {
            'primary_id': '123',
            'user_identifier': [
                {
                    'value': 'abc',
                    'id_type': {
                        'value': '02'
                    }
                }
            ]
        },
        'original_ids': [
            {'type': '02', 'value': '123', 'is_active': False}
        ]
    }


@pytest.fixture(params=[
    ('ACTIVE', 'ACTIVE'),
    ('ACTIVE', 'INACTIVE'),
    ('INACTIVE', 'INACTIVE'),
    ('INACTIVE', 'ACTIVE')
])
def id_migration__secondary_to_secondary(request):
    old, new = request.param
    assert all(status in ('ACTIVE', 'INACTIVE') for status in (old, new))
    return {
        'alma_user': {
            'primary_id': '123',
            'user_identifier': [
                {
                    'value': 'abc',
                    'status': new,
                    'id_type': {
                        'value': '02'
                    }
                }
            ]
        },
        'original_ids': [
            {'type': '03', 'value': 'abc', 'is_active': old == 'ACTIVE'}
        ]
    }


@pytest.fixture()
def id_migration__no_desired_ids():
    return {
        'alma_user': {
            'user_identifier': []
        },
        'original_ids': [
            {'type': '03', 'value': 'abc', 'is_active': True}
        ]
    }


@pytest.mark.parametrize('user_wrapper, requires_migration', [
    (pytest.lazy_fixture('id_migration__primary_to_secondary'),
     True),
    (pytest.lazy_fixture('id_migration__primary_to_secondary_insensitive'),
     True),
    (pytest.lazy_fixture('id_migration__active_secondary_to_primary'),
     True),
    (pytest.lazy_fixture('id_migration__inactive_secondary_to_primary'),
     False),
    (pytest.lazy_fixture('id_migration__secondary_to_secondary'),
     False),
    (pytest.lazy_fixture('id_migration__no_desired_ids'),
     True)
])
def test_requires_id_migration(user_wrapper, requires_migration):
    assert (IdMigrationTransform.requires_id_migration(user_wrapper)
            is requires_migration)


def test_id_migration_transform_generates_id():
    idtx = IdMigrationTransform({})

    assert isinstance(idtx.temporary_id, str)
    assert len(idtx.temporary_id) == 36


def test_id_migration_transform_generates_distinct_ids():
    ids = set(IdMigrationTransform({}).temporary_id for _ in range(10))

    assert len(ids) == 10


def test_id_migration_transform_id_can_be_specified():
    idtx = IdMigrationTransform({}, 'myid')

    assert idtx.temporary_id == 'myid'


def test_id_migration_transform_user_wrapper():
    user_wrapper = {}
    idtx = IdMigrationTransform(user_wrapper)

    assert idtx.user_wrapper is user_wrapper


@pytest.mark.parametrize('user_wrapper', [
    pytest.lazy_fixture('id_migration__primary_to_secondary'),
    pytest.lazy_fixture('id_migration__active_secondary_to_primary'),
    pytest.lazy_fixture('id_migration__inactive_secondary_to_primary'),
    pytest.lazy_fixture('id_migration__secondary_to_secondary')
])
def test_id_migration_transform_input_user_is_not_modified(user_wrapper):
    orig = copy.deepcopy(user_wrapper)

    tx = IdMigrationTransform(user_wrapper)
    assert tx.get_intermediate_state() != orig
    assert orig == tx.user_wrapper

    tx.get_final_state()
    assert tx.get_final_state() != orig
    assert orig == tx.user_wrapper


@pytest.mark.parametrize('user_wrapper', [
    pytest.lazy_fixture('id_migration__primary_to_secondary'),
    pytest.lazy_fixture('id_migration__active_secondary_to_primary'),
    pytest.lazy_fixture('id_migration__inactive_secondary_to_primary'),
    pytest.lazy_fixture('id_migration__secondary_to_secondary')
])
def test_id_migration_transform_intermediate_state(user_wrapper):
    idtx = IdMigrationTransform(user_wrapper)

    inter = idtx.get_intermediate_state()

    assert inter['alma_user']['primary_id'] == idtx.temporary_id
    assert len(inter['alma_user']['user_identifier']) == 0

    # A user id-conflicting migration is transformed into two migrations which
    # achieve the same thing, but don't conflict.
    assert not IdMigrationTransform.requires_id_migration(inter)


@pytest.mark.xfail
@pytest.mark.parametrize('user_wrapper', [
    pytest.lazy_fixture('id_migration__no_desired_ids')
])
def test_id_migration_transform_intermediate_state_no_desired_ids(
        user_wrapper):

    # Currently throws NotImplementedError
    IdMigrationTransform(user_wrapper).get_intermediate_state()


@pytest.mark.parametrize('user_wrapper', [
    pytest.lazy_fixture('id_migration__primary_to_secondary'),
    pytest.lazy_fixture('id_migration__active_secondary_to_primary'),
    pytest.lazy_fixture('id_migration__inactive_secondary_to_primary'),
    pytest.lazy_fixture('id_migration__secondary_to_secondary')
])
def test_id_migration_transform_final_state(user_wrapper):
    orig_user = copy.deepcopy(user_wrapper['alma_user'])

    idtx = IdMigrationTransform(user_wrapper)

    final = idtx.get_final_state()

    assert final['alma_user'] == orig_user
    # ID required to find the user after the intermediate migration
    assert final['original_ids'] == [
        {'type': 'primary', 'value': idtx.temporary_id, 'is_active': True}
    ]

    # A user id-conflicting migration is transformed into two migrations which
    # achieve the same thing, but don't conflict.
    assert not IdMigrationTransform.requires_id_migration(final)
