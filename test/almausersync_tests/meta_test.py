from io import StringIO, BytesIO
from unittest.mock import MagicMock

import aiofiles
import pytest


@pytest.mark.usefixtures('aiofiles_support_stringio')
@pytest.mark.asyncio
async def test_monkeypatching_aio_stringio():
    sio = StringIO('abcd')
    async with aiofiles.open(sio) as f:
        msg = await f.read()
        await f.write('123')

        assert msg == 'abcd'
        assert sio.getvalue() == 'abcd123'


@pytest.mark.usefixtures('aiofiles_support_bytesio')
@pytest.mark.asyncio
async def test_monkeypatching_aio_bytesio():
    bio = BytesIO(b'abcd')
    async with aiofiles.open(bio) as f:
        msg = await f.read()
        await f.write(b'123')

        assert msg == b'abcd'
        assert bio.getvalue() == b'abcd123'


@pytest.mark.usefixtures('aiofiles_support_mock')
@pytest.mark.asyncio
async def test_monkeypatching_aio_mock():
    mock_file = MagicMock()
    mock_file.read.return_value = 'abcd'

    async with aiofiles.open(mock_file) as f:
        msg = await f.read()
        await f.write('123')

        assert msg == 'abcd'
        mock_file.read.assert_called_once_with()
        mock_file.write.assert_called_once_with('123')
