from unittest.mock import sentinel, patch

import pytest

from almausersync.async_utils import anext, default_loop_or


@pytest.mark.asyncio  # default can be None:
@pytest.mark.parametrize('default_val', [sentinel.default, None])
async def test_anext_returns_default_instead_of_raising(default_val):
    async def empty_async_iter():
        if False:
            yield None

    assert await anext(empty_async_iter(), default_val) is default_val


@pytest.mark.asyncio
async def test_anext_raises_when_iter_is_empty_and_no_default_provided():
    async def empty_async_iter():
        if False:
            yield None

    with pytest.raises(StopAsyncIteration):
        assert await anext(empty_async_iter())


@pytest.mark.asyncio
async def test_anext_returns_next_value():
    values = list(range(3, 10))

    async def async_iter():
        for value in values:
            yield value

    aiter = async_iter()

    for value in values:
        assert await anext(aiter) == value


@pytest.mark.parametrize('args,kwargs', [
    ([], {}),
    ([None], {}),
    ([], {'loop': None}),
])
@patch('asyncio.get_event_loop')
def test_get_loop_returns_default_event_loop_when_given_none(
        get_event_loop, args, kwargs):

    get_event_loop.return_value = sentinel

    assert default_loop_or(*args, **kwargs) is sentinel

    get_event_loop.assert_called_once_with()


@pytest.mark.parametrize('args,kwargs', [
    ([sentinel], {}),
    ([], {'loop': sentinel}),
])
@patch('asyncio.get_event_loop')
def test_get_loop_returns_argument_if_not_none(get_event_loop, args, kwargs):
    assert default_loop_or(*args, **kwargs) is sentinel

    get_event_loop.assert_not_called()
