from asynctest.mock import MagicMock, patch, sentinel

import pytest

from almausersync.almarest import UserApiClient
from almausersync.exceptions import ApiOperationFailedError, UserUpdateFailedError, IdMigrationFailedError, \
    InvalidUserError
from almausersync.jobs import DefaultRetryStrategy, UpdateAPIOperationDriver
from almausersync.operations.update_user import UpdateUserOperation, UpdateUserOperationDriver
from almausersync.transforms import IdMigrationTransform
from almausersync.utils import create_id
from almausersync.wal import JsonStreamWalWriter


@pytest.fixture()
def current_primary_id():
    return '__orig_id__'


@pytest.fixture()
def final_primary_id():
    return '__final_id__'


@pytest.fixture()
def user_wrapper(current_primary_id, final_primary_id):
    return {
        'alma_user': {
            'primary_id': final_primary_id
        },
        'original_ids': [
            {'type': 'primary', 'value': current_primary_id, 'is_active': True}
        ]
    }


@pytest.fixture()
def init_op(user_wrapper, id_migration_tx_cls):
    op = UpdateUserOperation(user_wrapper,
                             id_migration_tx_cls=id_migration_tx_cls)
    assert op.state == 'init'
    return op


@pytest.fixture()
def one_shot_op(init_op, id_migration_tx_cls):
    id_migration_tx_cls.requires_id_migration.return_value = False
    assert not init_op.requires_id_migration()
    init_op.start()
    assert init_op.state == 'one_shot'
    return init_op


@pytest.fixture()
def id_migrate_intermediate_op(init_op):
    assert init_op.requires_id_migration()
    init_op.init_id_tx_transform()
    init_op.start()
    return init_op


@pytest.fixture()
def id_migrate_final_op(id_migrate_intermediate_op):
    id_migrate_intermediate_op.finish(sentinel.user)
    return id_migrate_intermediate_op


@pytest.fixture()
def failed_op(one_shot):
    one_shot.start_id_migration()
    return one_shot


@pytest.fixture()
def succeeded_op(one_shot):
    one_shot.finish()
    return one_shot


@pytest.fixture()
def id_tx_s1(user_wrapper):
    return user_wrapper


@pytest.fixture()
def id_tx_s2(user_wrapper):
    return user_wrapper


@pytest.fixture()
def temporary_id():
    return create_id()


@pytest.fixture()
def id_migration_tx(id_tx_s1, id_tx_s2, temporary_id):
    mock = MagicMock(spec=IdMigrationTransform)
    mock.get_intermediate_state.return_value = id_tx_s1
    mock.get_final_state.return_value = id_tx_s2
    mock.temporary_id = temporary_id
    return mock


@pytest.fixture()
def id_migration_tx_cls(id_migration_tx):
    mock = MagicMock(spec=IdMigrationTransform)
    mock.return_value = id_migration_tx
    mock.requires_id_migration.return_value = True

    return mock


def test_op_has_user_wrapper(user_wrapper):
    op = UpdateUserOperation(user_wrapper)

    assert op.user_wrapper is user_wrapper


@pytest.fixture()
def user_wrapper_invalid_ids():
    return {
        'alma_user': {
            'primary_id': 'abc',
            'user_identifier': [
                {'value': 'abc', 'id_type': {'value': '00'}}
            ]
        }
    }


def test_op_rejects_users_with_invalid_ids(user_wrapper_invalid_ids):
    with pytest.raises(ValueError) as excinfo:
        UpdateUserOperation(user_wrapper=user_wrapper_invalid_ids)

    cause = excinfo.value.__cause__
    assert isinstance(cause, InvalidUserError)
    assert 'User has multiple occurrences of IDs' in str(cause)


@pytest.mark.parametrize('requires_id_migration', [True, False])
def test_op_init_start(init_op, requires_id_migration, id_migration_tx_cls):
    id_migration_tx_cls.requires_id_migration.return_value = \
        requires_id_migration

    if requires_id_migration:
        init_op.init_id_tx_transform()

    init_op.start()

    if requires_id_migration:
        assert init_op.state == 'id_migrate_intermediate'
    else:
        assert init_op.state == 'one_shot'


@pytest.mark.parametrize('op, exc_type', [
    (pytest.lazy_fixture('one_shot_op'), UserUpdateFailedError),
    (pytest.lazy_fixture('id_migrate_intermediate_op'), UserUpdateFailedError),
    (pytest.lazy_fixture('id_migrate_final_op'), IdMigrationFailedError)
])
def test_op_fail(op, exc_type):
    cause = ValueError('boom')
    op.fail(cause)

    assert op.state == 'failed'
    assert op.failure_exception.__cause__ == cause
    assert isinstance(op.failure_exception, exc_type)


@pytest.mark.parametrize('op', [
    pytest.lazy_fixture('one_shot_op'),
    pytest.lazy_fixture('id_migrate_final_op')
])
def test_op_finish(op):
    op.finish(sentinel)

    assert op.state == 'succeeded'
    assert op.updated_user == sentinel


@pytest.fixture()
def wal_writer():
    return MagicMock(spec=JsonStreamWalWriter)


@pytest.fixture()
def userapi():
    return MagicMock(spec=UserApiClient)


@pytest.fixture()
def retry_strategy():
    return MagicMock(spec=DefaultRetryStrategy)


@pytest.fixture()
def update_api_op_driver_cls(update_api_op_driver):
    mock = MagicMock()
    mock.return_value = update_api_op_driver
    return mock


@pytest.fixture()
def update_api_op_driver():
    return MagicMock(spec=UpdateAPIOperationDriver)


@pytest.fixture()
def driver(op, wal_writer, userapi, retry_strategy, update_api_op_driver_cls):
    return UpdateUserOperationDriver(
        state=op, wal_writer=wal_writer, userapi=userapi,
        retry_strategy=retry_strategy,
        update_api_op_driver_cls=update_api_op_driver_cls)


@pytest.mark.parametrize('op', [pytest.lazy_fixture('init_op')])
@pytest.mark.parametrize('requires_id_migration', [True, False])
async def test_driver_init(
        op, requires_id_migration, driver, id_migration_tx_cls, wal_writer,
        temporary_id, current_primary_id, final_primary_id):

    id_migration_tx_cls.requires_id_migration.return_value = \
        requires_id_migration

    assert op.tx_temporary_id is None

    await driver._run_once()

    if requires_id_migration:
        assert op.tx_temporary_id is temporary_id

        wal_writer.log.assert_called_once_with({
            'barrier': 'before-id-migration',
            'current_primary_id': current_primary_id,
            'final_primary_id': final_primary_id,
            'temporary_id': temporary_id
        })

        assert op.state == 'id_migrate_intermediate'
    else:
        assert op.tx_temporary_id is None
        assert op.state == 'one_shot'


@pytest.mark.parametrize('op, requires_id_migration, success_state', [
    (pytest.lazy_fixture('id_migrate_intermediate_op'), True, 'id_migrate_final'),
    (pytest.lazy_fixture('id_migrate_final_op'), True, 'succeeded'),
    (pytest.lazy_fixture('one_shot_op'), False, 'succeeded')
])
@pytest.mark.onlythis
async def test_perform_api_update_success(
        op, requires_id_migration, driver, wal_writer, userapi,
        update_api_op_driver_cls, update_api_op_driver, success_state):

    api_state = MagicMock(wraps=op.api_update_op)
    op.api_update_op = api_state

    def move_api_op_to_outcome_state():
        op.api_update_op.state = success_state
    update_api_op_driver.run.side_effect = move_api_op_to_outcome_state

    with patch('almausersync.operations.update_user.'
               'wrap_with_source') as wrap_with_source:
        wrap_with_source.return_value = sentinel.wal
        await driver._run_once()

    # We share the same state names, and expect to succeed
    assert op.state == success_state

    update_api_op_driver_cls.assert_called_once_with(
        state=api_state, wal_writer=sentinel.wal, userapi=userapi)


@pytest.mark.parametrize('op, requires_id_migration, exc_type', [
    (pytest.lazy_fixture('id_migrate_intermediate_op'), True, UserUpdateFailedError),
    (pytest.lazy_fixture('id_migrate_final_op'), True, IdMigrationFailedError),
    (pytest.lazy_fixture('one_shot_op'), False, UserUpdateFailedError)
])
@pytest.mark.onlythis
async def test_perform_api_update_failure(
        op, requires_id_migration, driver, wal_writer, userapi,
        update_api_op_driver_cls, update_api_op_driver, exc_type):

    api_state = MagicMock(wraps=op.api_update_op)
    op.api_update_op = api_state

    def move_api_op_to_outcome_state():
        op.api_update_op.state = 'failed'
        raise ApiOperationFailedError('boom')
    update_api_op_driver.run.side_effect = move_api_op_to_outcome_state

    with patch('almausersync.operations.update_user.'
               'wrap_with_source') as wrap_with_source:
        wrap_with_source.return_value = sentinel.wal
        with pytest.raises(exc_type):
            await driver._run_once()

    # We share the same state names, and expect to succeed
    assert op.state == 'failed'

    update_api_op_driver_cls.assert_called_once_with(
        state=api_state, wal_writer=sentinel.wal, userapi=userapi)
