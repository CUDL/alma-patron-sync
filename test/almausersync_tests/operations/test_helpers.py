import pytest

from almausersync.operations.helpers import validate_wal_message_dict


def test_validate_wal_message_dict_accepts_dict():
    validate_wal_message_dict({})


@pytest.mark.parametrize('message', [42, 'hi', []])
def test_validate_wal_message_dict_rejects_non_dict(message):
    with pytest.raises(ValueError):
        validate_wal_message_dict(message)
